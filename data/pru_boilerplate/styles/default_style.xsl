<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
xmlns:mmax="org.eml.MMAX2.discourse.MMAX2DiscourseLoader"
xmlns:ne="www.eml.org/NameSpaces/ne"
xmlns:neposfilter="www.eml.org/NameSpaces/neposfilter"
xmlns:entity="www.eml.org/NameSpaces/entity"
xmlns:sentence="www.eml.org/NameSpaces/sentence">
<xsl:output method="text" indent="no" omit-xml-declaration="yes"/>
<xsl:strip-space elements="*"/>

<xsl:template match="words">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="word">
    <xsl:value-of select="mmax:registerDiscourseElement(@id)"/>
    <xsl:if test="substring(text(),1,1)!='*'"> <!-- Consider for space insertion only if word is not empty -->
        <xsl:choose>
            <xsl:when test=" 
                    text()='.' or
                    text()='!' or
                    text()='?' or
                    text()=',' or
                    text()=';'
                    "> <!-- Do not insert space if word is some EOS-like marker -->
            </xsl:when>
            <xsl:otherwise> <!-- In all other cases, insert spaces in between words -->
                <xsl:text> </xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:if>

    <xsl:apply-templates select="mmax:getStartedMarkables(@id)" mode="opening"/>
    <xsl:value-of select="mmax:setDiscourseElementStart()"/>
    <!-- Display word text only if it is not empty -->
    <xsl:if test="substring(text(),1,1)!='*'">
        <xsl:apply-templates/>
    </xsl:if>
    <xsl:value-of select="mmax:setDiscourseElementEnd()"/>
    <xsl:apply-templates select="mmax:getEndedMarkables(@id)" mode="closing"/>
</xsl:template>

<xsl:template match="sentence:markable" mode="opening">
    <!-- <xsl:text>	</xsl:text> -->
</xsl:template>

<xsl:template match="sentence:markable" mode="closing">
    <xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="entity:markable" mode="opening">
    <!-- <xsl:text>	</xsl:text> -->
</xsl:template>

<xsl:template match="entity:markable" mode="closing">
    <!-- <xsl:text>	</xsl:text> -->
</xsl:template>

<xsl:template match="neposfilter:markable" mode="opening">
    <!-- <xsl:text>	</xsl:text> -->
</xsl:template>

<xsl:template match="neposfilter:markable" mode="closing">
    <!-- <xsl:text>	</xsl:text> -->
</xsl:template>


<xsl:template match="ne:markable" mode="opening">
    <xsl:value-of select="mmax:startBold()"/>
    <xsl:value-of select="mmax:addLeftMarkableHandle(@mmax_level, @id, '[')"/>
    <xsl:value-of select="mmax:endBold()"/>
</xsl:template>

<!--
<xsl:template match="ne:markable" mode="closing">
    <xsl:value-of select="mmax:startBold()"/>
    <xsl:value-of select="mmax:addRightMarkableHandle(@mmax_level, @id, ']')"/>
    <xsl:value-of select="mmax:endBold()"/>
</xsl:template>
-->

<xsl:template match="ne:markable" mode="closing">
    <xsl:value-of select="mmax:addRightMarkableHandle(@mmax_level, @id, string-length(@ne_type)+1,1)"/>
    <xsl:value-of select="mmax:startBold()"/>
    <xsl:text>]</xsl:text>
    <xsl:value-of select="mmax:endBold()"/>
    <xsl:value-of select="mmax:startSubscript()"/>
    <xsl:value-of select="@ne_type"/>
    <xsl:value-of select="mmax:endSubscript()"/>
</xsl:template>

</xsl:stylesheet>
