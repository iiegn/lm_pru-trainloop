# txp2conll -- make a .conll file from a TextPro file with
# eos, pos, lemma information
sub coarse_pos($) {
    my ($pos)=@_;
    $pos=~/^V/ and return "VERB";
    $pos=~/^R/ and return "ART";
    $pos=~/^E/ and return "PREP";
    $pos=~/^S/ and return "NOUN";
    $pos=~/^A/ and return "ADJ";
    $pos=~/^D/ and return "ADJ";
    # 'che' is PRON 337x and CONJ 123x;
    # a better heuristic might help here
    $pos=~/^CCHE/ and return "PRON";
    $pos=~/^C/ and return "CONJ";
    $pos=~/^XP/ and return "PUNCT";
    $pos=~/^B/ and return "ADV";
    $pos=~/^P/ and return "PRON";
    $pos=~/^YA/ and return "NOUN";
    $pos=~/^YF/ and return "NOUN";
    $pos=~/^N/ and return "NUM";
    return "UNK";
}

sub correct_lemma($$$) {
    my ($pos,$lemma,$word)=@_;
    $lemma=~s/full_stop/\#\\./;
    $lemma=~s/,/\#\\,/;
    $lemma=~s/semicolon/\#\\;/;
    $lemma=~s/\/det$//;
    $lemma=~s/indet/un/;
    $lemma=~s/det/il/;
    $lemma=~s/rifl/si/;
    $lemma=lc($lemma);
    return $lemma;
}

sub correct_pos($$) {
    my ($word,$pos)=@_;
    if ($pos eq 'C') {
	if ($word=~/^(?:ed?|ma|n�|oppure|.*s�|mentre|per�|o|anzich�|nonch�)$/i) {
	    return 'C_coo';
	} elsif ($word=~/^(?:appena|cos�|dopo|se|.*ch�)$/i) {
	    return 'C_sub';
	} elsif ($word=~/^(?:anche|infatti)/) {
	    # TextPro mistags these as C, but they should be B
	    return 'B';
	}
    } elsif ($pos =~ /^[PD]/ and $word =~ /^qual[ei]?$/i) {
	$pos .= '-Q';
    } elsif ($pos eq 'YA') {
	$pos = 'SPN';
    } elsif ($pos eq 'YF') {
	$pos = 'SN';
    }
    return $pos;
}

$wnum=1;
while (<>)
{
    next if /(^\#)|(^$)/;
    split;
    $pos=correct_pos($_[0],$_[2]);
    $cpos=coarse_pos($pos);
    $lemma=correct_lemma($_[2],$_[3],$_[0]);
    print "$wnum\t$_[0]\t$lemma\t$pos\t$cpos\t_\n";
    $wnum++;
    if ($_[1] eq '<eos>') {
	print "\n";
	$wnum=1;
    }
}
