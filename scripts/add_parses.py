import sys
import re
import getopt
try:
    import xml.etree.cElementTree as etree
except ImportError:
    import cElementTree as etree
import morphit_pos
import ita_heads
from pynlp.mmax_tools import MMAXDiscourse, export_fname, all_docs
import pytree.tree as tree
import pytree.deps as deps
import pytree.export as export
import pytree.export2mrg as export2mrg

opts,args=getopt.getopt(sys.argv[1:],'su',
                        ['sentence','unit'])
want_sentence=False
want_unit=False
for opt,val in opts:
    if opt in ['-s','--sentence']:
        want_sentence=True
    if opt in ['-u','--unit']:
        want_unit=True
    else:
        print >>sys.stderr,"Unrecognized option: %s"%(opt,)
        sys.exit(1)

anno_dir=args[0]

headFinder=deps.SimpleDepExtractor(ita_heads.mod_hr_table)

def detect_passive(t):
    for n in t.terminals:
        if n.edge_label=='PASSIVE':
            head=n.parent.head
            head.passive=True

def write_stuff(trees,doc):
    aa=[]
    for sent_no,t in enumerate(trees):
        t.sent_no=sent_no
        pos0=t.start_word
        headFinder(t)
        detect_passive(t)
        top_node=tree.NontermNode('Start')
        top_node.children=t.roots
        for n in t.terminals:
            if n.word=='(':
                n.word='-LRB-'
            elif n.word==')':
                n.word='-RRB-'
        # do_attach_punct reattaches fragments that
        # would otherwise break projectivity
        # (Note that this is not sufficient for the entwined
        # tangles that TUT sometimes produces)
        export2mrg.do_attach_punct(t)
        aa.append(('sentence',None,{'orderid':str(sent_no+1)},
                   pos0,pos0+len(t.terminals)))
        aa.append(('parse',None,{'tag':top_node.to_full([])},
                   pos0,pos0+len(t.terminals)))
        for i,n in enumerate(t.terminals):
            aa.append(('pos',None,{'tag':n.cat.lower()},
                       pos0+i,pos0+i+1))
            aa.append(('lemma',None,
                       {'tag':morphit_pos.get_lemma(n.word,n.cat)},
                       pos0+i,pos0+i+1))
        for n in t.roots:
            add_nps(n,pos0,aa,t,doc)
    doc.write_markables(aa)

def is_definite(node):
    for n in node.children:
        if n.cat[0]=='R' and n.word[0].lower()!='u':
            return True
    if node.parent and node.parent.cat=='PX':
        for n1 in node.parent.children:
            if n1.cat=='ES':
                return True
    return False

postmod_cats=['E','ES','EP','XPW','XPS',
              'CCHE','CCHI','C','PS-Q','PN-Q','PP-Q']

def make_chunk(t,node,heads,attrs):
    start=node.start
    chunk_end=heads[-1].end
    while chunk_end<node.end:
        if t.terminals[chunk_end].cat in postmod_cats:
            break
        chunk_end+=1
    pos_l=[]
    for n in t.terminals[start:chunk_end]:
        pos_l.append(n.cat.lower())
    attrs['pos']=' '.join(pos_l)
    attrs['min_ids']=(t.start_word+start,t.start_word+chunk_end)

def add_pos_attr(t,node,attrs):
    pos_l=[]
    for n in t.terminals[node.start:node.end]:
        pos_l.append(n.cat.lower())
    attrs['pos']=' '.join(pos_l)

def get_np_attrs(node,attrs,t,doc):
    is_conjunction=False
    heads=[]
    head_pos=None
    head_morph=None
    has_numP=False
    first_conjunct=None
    for n in node.children:
        if n.edge_label=='KONJ':
            is_conjunction=True
            if first_conjunct is None:
                first_conjunct=n
        elif n.edge_label=='HD':
            head_pos=n.cat
            head_morph=n.morph
            heads.append(n)
        if n.cat=='NUM-P':
            has_numP=True
    if head_pos is None and len(node.children)==1:
        heads.append(node.children[0])
        head_pos=node.children[0].cat
        head_morph='**'
    if is_conjunction:
        attrs['markable_type']='np.coord'
        if is_definite(first_conjunct):
            attrs['mention_type']='nom.def'
        else:
            attrs['mention_type']='nom.indef'            
        attrs['mention_type']
        add_pos_attr(t,node,attrs)
    elif head_pos is not None:
        attrs['head_pos']=doc.make_span(t.start_word+heads[0].start,
                                        t.start_word+heads[-1].end)
        make_chunk(t,node,heads,attrs)
        if head_pos=='SPN':
            attrs['mention_type']='nam'
            attrs['num']='sing'
        elif head_pos[0] in 'DASP':
            if head_pos[1]=='P':
                attrs['num']='plur'
            elif head_pos[1] in 'S':
                attrs['num']='sing'
            if head_pos[0]=='P':
                if len(head_morph)==3:
                    attrs['mention_type']='pro.per'+head_morph[2]
                    if head_morph[1]=='m':
                        attrs['gend']='masc'
                    elif head_morph[1]=='f':
                        attrs['gend']='fem'
            else:
                if len(head_morph)==2:
                    if head_morph[1]=='m':
                        attrs['gend']='masc'
                    elif head_morph[1]=='f':
                        attrs['gend']='fem'
                if is_definite(node):
                    attrs['mention_type']='nom.def'
                else:
                    attrs['mention_type']='nom.indef'
    elif has_numP:
        if is_definite(node):
            attrs['mention_type']='nom.def'
        else:
            attrs['mention_type']='nom.indef'
        attrs['sem_type']='numeric'
        add_pos_attr(t,node,attrs)
    else:
        if is_definite(node):
            attrs['mention_type']='nom.def'
        else:
            attrs['mention_type']='nom.indef'
        attrs['sem_type']='abstract'
        print >>sys.stderr,"No head for %s"%(node.to_penn(),)
        add_pos_attr(t,node,attrs)
    n1=node
    while n1.edge_label=='KONJ':
        n1=n1.parent
    grole=n1.edge_label
    srole=n1.edge_label
    if n1.parent:
        head=n1.parent.head
        head_lemma=morphit_pos.get_lemma(head.word,head.cat)
    else:
        head=None
        head_lemma='NULL'
    if head and head.cat[0]=='E':
        grole=srole='PP_'+head_lemma
        head=n1.head
        if head:
            head_lemma=morphit_pos.get_lemma(head.word,head.cat)
        else:
            head_lemma='NULL'
    if grole=='SUBJ':
        if getattr(head,'passive',False):
            srole='DOBJ'
        else:
            for n2 in n1.parent.children:
                if n2.edge_label=='PRED':
                    srole='pred'
                    head=n2.head
                    head_lemma=morphit_pos.get_lemma(head.word,head.cat)
    attrs['srole']='%s:%s:%s'%(grole,srole,head_lemma)

clitic_re=re.compile('([mtcv]i|l[ioae]|gli|si|ne)$')
clitic_map={'mi':'1s', 'ti':'2s', 'lo':'3sm', 'la':'3sf',
            'ci':'1p', 'vi':'2s', 'le':'3', 'gli':'3',
            'si':'R', 'ne':'3'}
possessive_re=re.compile('(mi|tu|su|nostr|vostr)(?:[aoei]|oi|ei)$',re.I)
possessive_map={'mi':'1s', 'tu':'2s', 'su':'3s',
            'nostr':'1p', 'vostr':'2s', 'lor':'3'}
num_map={'s':'sing','p':'plur'}
gend_map={'f':'fem','m':'masc'}
def classify_clitic(w,attrs):
    m=clitic_re.search(w)
    if m:
        clitic=m.group(1)
        if clitic in clitic_map:
            entry=clitic_map[clitic]
        else:
            entry='3'
            print >>sys.stderr, "No clitic_map entry for %s"%(w,)
        attrs['head_word']=clitic
    else:
        entry='3'
        print >>sys.stderr, "No clitic_map entry for %s"%(w,)
    if entry=='R':
        attrs['mention_type']='pro.refl'
        attrs['pos']='PN'
    else:
        attrs['mention_type']='pro.per'+entry[0]
        if len(entry)>=2:
            attrs['num']=num_map[entry[1]]
            attrs['pos']='P'+entry[1].upper()
        else:
            attrs['pos']='PN'
        if len(entry)>=3:
            attrs['gend']=gend_map[entry[2]]

def classify_zero(vfin,attrs):
    if vfin.morph=='--':
        print >>sys.stderr,"CFY_ZERO: no morph for %s"%(vfin.to_full([]),)
        attrs['pos']='PN'
    else:
        attrs['num']=num_map[vfin.morph[1]]
        attrs['mention_type']='pro.per'+vfin.morph[0]
        attrs['pos']='PN'

def find_subject_in_coordination(node):
    first_conj=None
    for n in node.children:
        if n.edge_label=='KONJ':
            first_conj=n
            break
    assert first_conj
    has_subj=False
    for n in first_conj.children:
        if n.edge_label=='SUBJ':
            has_subj=True
            break
    return has_subj

def classify_possessive(wpart,attrs):
    if wpart in possessive_map:
            entry=possessive_map[wpart]
    else:
        entry='3'
        print >>sys.stderr, "No possessive_map entry for %s"%(wpart,)
    if entry=='R':
        attrs['mention_type']='pro.refl'
    else:
        attrs['mention_type']='pro.per'+entry[0]
        if len(entry)>=2:
            attrs['num']=num_map[entry[1]]
        if len(entry)>=3:
            attrs['gend']=gend_map[entry[2]]

unwanted_tokens=re.compile('(?:[,:;\\.`\'"]+|(?:ne|a|su|de|da|co)(?:l|ll[a\']|gli|i))$')
def frob_boundaries(doc,p0,p1):
    while unwanted_tokens.match(doc.tokens[p0]) and p0+1<p1:
        p0+=1
    while unwanted_tokens.match(doc.tokens[p1-1]) and p0+1<p1:
        p1-=1
    return (p0,p1)    

def add_nps(node,pos0,aa,t,doc):
    if node.cat=='NP':
        attrs={'markable_type':'np',
               'mention_type':'any',
               'gend':'any',
               'num':'any',
               'sem_type':'any',
               'sentenceid':t.sent_no}
        get_np_attrs(node,attrs,t,doc)
        (p0,p1)=frob_boundaries(doc,pos0+node.start,pos0+node.end)
        aa.append(('markable',None,attrs,p0,p1))
    elif node.cat.endswith('+E'):
        attrs={'markable_type':'v.clitic',
               'mention_type':'pro.per3',
               'gend':'any',
               'num':'any',
               'sem_type':'any',
               'sentenceid':t.sent_no}
        # todo: classfy clitic
        classify_clitic(node.word,attrs)
        aa.append(('markable',None,attrs,pos0+node.start,pos0+node.end))
    elif node.cat == 'VXI':
        # if it's a finite verb chunk, see if there is a subject
        has_subj=False
        for n in node.parent.children:
            if n.edge_label=='SUBJ':
                has_subj=True
        if not has_subj:
            n_parent=node.parent
            # if it's part of a conjunction, then
            # it might be a VP conjunction and we don't
            # really need the subject
            if (n_parent.edge_label=='KONJ' and
                n_parent is not n_parent.parent.children[0]):
                # Kepa says that in cases like
                # "e' andato in citta' e ha mangiato un panino"
                # we should have two zero subjects
                # so in these cases, we need to look at the conjunction
                # to distinguish it from cases of VP coordination
                # that do have a subject for the coordinated VP
                has_subj=find_subject_in_coordination(n_parent.parent)
        if not has_subj:
            attrs={'markable_type':'v.zero',
                   'mention_type':'pro.per3',
                   'gend':'any',
                   'num':'any',
                   'sem_type':'any',
                   'sentenceid':t.sent_no}
            vfin=None
            for n in node.children:
                if n.cat.startswith('VI'):
                    vfin=n
                    break
            classify_zero(vfin,attrs)
            aa.append(('markable',None,attrs,pos0+vfin.start,pos0+vfin.end))
    if want_unit:
        if node.cat in ['S','S-REL','VP']:
            if node.cat in ['S','S-REL']:
                is_finite='finite-yes'
            else:
                is_finite='finite-no'
            if node.parent is None:
                utype='main'
            elif node.cat=='S-REL' or node.edge_label=='RELCL':
                utype='relative'
            elif node.edge_label in ['PN','PREP-ARG','CONJ-ARG']:
                utype='argument'
            else:
                utype='adjunct'
            aa.append(('unit',None,{'verbed':'verbed-yes',
                                    'finite':is_finite,
                                    'utype':utype},
                       pos0+node.start,pos0+node.end))
        elif ((node.parent is None or node.parent.cat=='Start') and
              not node.cat.startswith('XP')):
            aa.append(('unit',None,{'verbed':'verbed-no',
                                    'finite':'no-finite',
                                    'utype':'main'},
                       pos0+node.start,pos0+node.end))            
    if node.isTerminal():
        m=possessive_re.match(node.word)
        if m:
            attrs={'markable_type':'poss',
                   'mention_type':'pro.per3',
                   'gend':'any',
                   'num':'any',
                   'pos':node.cat,
                   'sem_type':'any',
                   'sentenceid':t.sent_no}
            classify_possessive(m.group(1).lower(),attrs)
            aa.append(('markable',None,attrs,pos0+node.start,pos0+node.end))
    for n in node.children:
        add_nps(n,pos0,aa,t,doc)

def find_different(words,tokens):
    n1=0
    while words[n1]==tokens[n1]:
        n1+=1
    n2=1
    while words[-n2]==tokens[-n2]:
        n2+=1
    n2a=len(words)-n2
    n2b=len(tokens)-n2
    return ((n1,n2a),words[n1:n2a+1],tokens[n1:n2b+1])

if __name__=='__main__':
    dirname=args[0]
    if len(args)<2:
        docs=all_docs(dirname)
    else:
        docs=args[1:]
    for docid in docs:
        print >>sys.stderr,"%s (add_parses)"%(docid,)
        doc=MMAXDiscourse(anno_dir,docid)
        trees=list(export.read_trees(file(export_fname(anno_dir,docid))))
        words=[]
        for t in trees:
            t.start_word=len(words)
            words += [n.word for n in t.terminals]
        assert len(words)==len(doc.tokens), find_different(words,doc.tokens)
        assert words==doc.tokens, find_different(words,doc.tokens)
        write_stuff(trees,doc)

