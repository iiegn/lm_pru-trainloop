#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import sys
import re
import pytree.tree as tree
import pytree.export as export
from tut_reader import read_trees, make_terminal, Token
import tut_xform
import morphit_pos

uppercase_classes=['SPN']
nomorph_pos=['XPS','XPW','XPB','XPO']
def add_morphtags(t):
    for n in t.terminals:
        if n.cat in nomorph_pos:
            continue
        if n.cat in uppercase_classes:
            word=n.word
        else:
            word=n.word.lower()
        n.morphs=[m for m in morphit_pos.possible_ana(word)
                  if m[0]==n.cat]
        if len(n.morphs)==0:
            print >>sys.stderr,"no morphs for word: %s"%(n.word)
        elif len(n.morphs)>1:
            print >>sys.stderr,"ambig morph for word %s: %s"%(n.word,n.morphs)
        if n.morphs:
            n.morph=n.morphs[0][2]
        else:
            n.morph='--'

proj_label={
    'PREP':'PX',
    'PREPART':'PX',
    'NOUN':'NP',
    'PRON':'NP',
    'ADV':'ADVP',
    'ADJ':'ADJP',
    'ART':'ARTP',
    'VERB':'VP'
    }

def make_tree(tokens):
    terminals=[]
    nodes=[]
    nodes_by_id={}
    for tok in tokens:
        nterm=make_projection(tok)
        if '.' not in tok.id:
            term=make_terminal(tok)
            term.parent_id=tok.id
            nodes.append(term)
            terminals.append(term)
        nterm.parent_id=tok.parent_id
        nodes_by_id[tok.id]=nterm
        nodes.append(nterm)
    roots=[]
    for n in nodes:
        if n.parent_id=='0':
            n.parent=None
            roots.append(n)
        else:
            n.parent=nodes_by_id[n.parent_id]
            n.parent.children.append(n)
    t=tree.Tree()
    t.terminals=terminals
    t.roots=roots
    return t


def make_projection(tok):
    if tok.coarse_pos=='PRON' and tok.attrs[2]=='LOC':
        lbl='ADVP'
    elif tok.coarse_pos in proj_label:
        lbl=proj_label[tok.coarse_pos]
    else:
        lbl=tok.coarse_pos+'-P'
    if tok.parent_id=='0':
        elbl='--'
    else:
        if 'RMOD+RELCL' in tok.edge_label:
            elbl='RELCL'
        elif 'RMOD' in tok.edge_label:
            elbl='RMOD'
        elif 'PREDCOMPL' in tok.edge_label:
            elbl='PRED'
        elif tok.edge_label=='VERB-SUBJ':
            elbl='SUBJ'
        elif tok.edge_label in ['VERB-OBJ','VERB-OBJ*LOCUT']:
            elbl='DOBJ'
        elif tok.edge_label=='PREP-ARG':
            elbl='PN'
        else:
            elbl=tok.edge_label
    a=tree.NontermNode(lbl,elbl)
    return a

default_attrs={
    'PS':['PERS','ALLVAL','SING','3'],
    'PP':['PERS','ALLVAL','PLUR','3'],
    'PN':['PERS','ALLVAL','ALLVAL','3'],
    'PS-Q':['RELAT','ALLVAL','SING','3'],
    'PP-Q':['RELAT','ALLVAL','PLUR','3'],
    'PN-Q':['RELAT','ALLVAL','ALLVAL','3'],
    'CCHE':['RELAT','ALLVAL','ALLVAL','3'],
    }
def read_conll(f):
    toks=[]
    sent_no=1
    for l in f:
        line=l.strip().split()
        if line and line != ['']:
            if line[7] in ['TENSE','PASSIVE','PROGRESSIVE']:
                line[7]='AUX+'+line[7]
            elif line[7].startswith('CONTIN-'):
                line[7]='CONTIN+'+line[7][7:]
            elif line[7].startswith('OPEN-'):
                line[7]='OPEN+'+line[7][5:]
            elif line[7].startswith('CLOSE-'):
                line[7]='CLOSE+'+line[7][6:]

            tok=Token(word=line[1],id=line[0],lemma=line[2],
                      cat=line[3],coarse_pos=line[4],
                      parent_id=line[6], edge_label=line[7])
            tok.attrs=[line[2],line[4]]+default_attrs.get(line[3],[])
            toks.append(tok)
        else:
            if toks:
                yield (sent_no,toks)
                toks=[]
                sent_no +=1

def sanitize_tokens(tokens):
    for tok in tokens:
        if (tok.edge_label.startswith('AUX+') and
            'Y' not in tok.cat):
            tok.cat += 'Y'

for (sent_no,tokens) in read_conll(file(sys.argv[1])):
    sanitize_tokens(tokens)
    t=make_tree(tokens)
    tut_xform.correct_const(t)
    t.node_table={}
    t.renumber_ids(t.roots)
    add_morphtags(t)
    print '#BOS %d 0 0 0'%(sent_no,)
    export.write_sentence(t,sys.stdout)
    print "#EOS %d"%(sent_no,)

