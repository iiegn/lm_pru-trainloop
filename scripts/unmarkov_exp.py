import sys
import pytree.export as export
import pytree.mrg2export as mrg2export

f_exp=sys.stdout
sentnum=1
for t in export.read_trees(sys.stdin):
    mrg2export.unmarkovize_proc(t)
    mrg2export.undecorate_proc(t)
    t.node_table={}
    t.renumber_ids()
    f_exp.write('#BOS %d 0 0 0\n'%(sentnum,))
    export.write_sentence(t,f_exp)
    f_exp.write('#EOS %d\n'%(sentnum,))
    sentnum+=1

