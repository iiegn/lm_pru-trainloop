#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

# extracts tokens and sentence boundaries from an export file
# and creates an MMAX file, adding a paragraph for each sentence.
import sys
import pytree.tree as tree
import pytree.export as export
import os
import os.path
import glob
from getopt import getopt
from pynlp.mmax_tools import *

sentences=[]

opts,args=getopt(sys.argv[1:],'etf',['retokenize'])
mode='exp'
retokenize=False
force=False
for o,a in opts:
    if o=='-e':
        mode='exp'
    elif o=='-t':
        mode='tok'
    elif o=='-f':
        force=True
    elif o=='--retokenize':
        retokenize=True

if mode=='exp':
    for t in export.read_trees(file(args[0])):
        sent=[n.word for n in t.terminals]
        sentences.append(sent)
elif mode=='tok':
    words=[]
    for l in file(args[0]):
        line=l.strip().split()
        if l==['']:
            if words:
                sentences.append(words)
                words=[]
        else:
            words.append(line[0])

paragraphs=[]
tokens=[]
pos=0
for sent in sentences:
    for w in sent:
        tokens.append(w)
    newpos=pos+len(sent)
    paragraphs.append(('sentence',None,{},pos,newpos))
    pos=newpos

doc_id=os.path.basename(args[0])
if '.' in doc_id:
    doc_id=doc_id[:doc_id.index('.')]
if (os.path.exists(words_fname(args[1],doc_id)) and
    not retokenize):
    if force:
        print >>sys.stderr, "MMAX file exists - removing everything"
        os.unlink(words_fname(args[1],doc_id))
        for mfile in glob.glob(markable_fname(args[1],doc_id,'*')):
            os.unlink(mfile)
        os.unlink(os.path.join(dirname,'%s.mmax'%(base,)))
    else:
        print >>sys.stderr, "MMAX file exists - won't overwrite it"
        sys.exit(1)
if retokenize and not os.path.exists(words_fname(args[1],doc_id)):
    print >>sys.stderr, "retokenize: can't find MMAX data."
    sys.exit(1)
if retokenize:
    doc=MMAXDiscourse(args[1],doc_id)
    doc.retokenize(tokens)
    doc.write_markables(paragraphs)
else:
    write_dotmmax(args[1],doc_id)
    write_basedata(words_fname(args[1],doc_id),tokens)
    write_markables(args[1],doc_id,paragraphs)
