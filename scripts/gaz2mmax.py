#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

dsc = """
extracts tokens, sentence, nerType, entity, pos/lemma boundaries from a TextPro
file; also keep '#comment lines' and all other columns.

all columns (and other content) will end up as MMax Levels - but the content we
don't care about will be pickled and b64encoded.
"""

epl = """
(this is used by other scripts, e.g.:)
.../LM_Coref/bin/convert_gaz_data.sh \
        DIR_WITH_TXP_FILES/*.txt.txp.gaz MMAX_OUT.mmax/
"""

import argparse
import base64
import glob
import logging
import os, os.path
import pickle
import sys

from pynlp.mmax_tools import (markable_fname, write_basedata, 
        write_dotmmax, words_fname, write_markables)

import gaz2mmax_overhaul

parser = argparse.ArgumentParser(description=dsc,epilog=epl)
parser.add_argument("-f", "--force", action="store_true",
        dest="force", default=False,
        help="overwrite destination files")
parser.add_argument("--auto_citations", action="store_true",
        dest="auto_citations", default=False,
        help="try addind pubauthor, -year markables")
parser.add_argument("--nertype_sanity_check", action="store_true",
        dest="nertype_sanity_check", default=False,
        help="check that at least one pos_tag within nerType range is 'spn'")
parser.add_argument("-v", dest="log_level",
        action="store_const", const=logging.INFO,
        help="be informative while processing")
parser.add_argument("-vv", dest="log_level",
        action="store_const", const=logging.DEBUG,
        help="be very informative while processing (DEBUG)")
parser.add_argument("txp_file", type=file)
parser.add_argument("outdir")
args = parser.parse_args()

# init logging
log_level = args.log_level
log = logging.getLogger(sys.argv[0])
log = logging.basicConfig(format='%(name)s:%(message)s', level=log_level)

tokens = []     # word forms, sequencial list of tokens
nerTypeCol = [] # ('ne',None,nerType,startTok,endTok)
nerTypePosFilter = []
entityCol = [] 
poslemCol = [] 
otherCols = []  # keep the non-nerTypes, to be able to reconstruct the 'txp' file
                # the content will be a list, pickled and, finally, b64encoded
sentences = []  # ('sentence',None,{},startTok,endTok)
comments = []   # keep the 'comments' to be able to reconstruct the 'txp' file

tmp_nerTypes = {} # tmp var for the currently processed ranges
tmp_comments = [] # tmp var for the currently processed range

while True:
    line = args.txp_file.readline()
    
    # it's a comment (or at least, s.th. we only need to store)
    if line.startswith("#"):
        tmp_comments.append(line.strip()[2:])

    # empty line or EOF: i.e. (usually) a sentence has been finished
    elif line == "\n" or line == "":
        # ...but might have been an emtpy line at the beginning
        if len(tokens) > 0:
            starttok = 0
            endtok = len(tokens)

            # to be able to get the end of the last range, there needs to be
            # already a value to look-up.
            if len(sentences) > 0:
                starttok = sentences[len(sentences)-1][4]

            #...or might have been an emtpy line at the end.
            if starttok != endtok:
                comments.append(('comments', None, 
                    {"value":base64.b64encode(pickle.dumps(tmp_comments))}, 
                    starttok, endtok))
                sentences.append(('sentence', None, {}, starttok, endtok))

        # empty out the tmp var - this should be safe, i.e. 'unsaved' content
        # should never be destroyed.
        tmp_comments = []
        
        # EOF - we're done
        if line == "": break

    # split tab-delimited fields and process them
    else:
        fields = line.strip().split('\t')
        token = fields[0]
        pos = fields[3]
        lemma = fields[4]
        entity = fields[5]
        others = fields[1:3]
        nertypestrs = [x.strip() for x in fields[6].split('/')]

        # check: are we processing an 'onging' nerType, or a new one
        for nertypestr in nertypestrs:
            if nertypestr in tmp_nerTypes:
                tmp_nerTypes[nertypestr] = (
                        [tmp_nerTypes[nertypestr][0], len(tokens)+1])
            elif nertypestr != "O":
                tmp_nerTypes[nertypestr] = [len(tokens), len(tokens)+1]

        spos = len(tokens)
        epos = len(tokens)+1
        tokens.append(token)
        otherCols.append(('other_cols', None, 
            {"value":base64.b64encode(pickle.dumps(others))}, spos, epos))
        poslemCol.append(('poslem', None, 
            {"pos_tag":pos.lower(), "lemma":base64.b64encode(
                pickle.dumps(lemma))}, spos, epos))
        if pos.lower() == "spn":
            nerTypePosFilter.append(('neposfilter', None, 
                {"pos_tag":pos.lower()}, spos, epos))
        if entity != "O":
            entityCol.append(('entity', None, {"value":entity}, spos, epos))

doc_id = os.path.basename(args.txp_file.name)
if '.' in doc_id:
    doc_id = doc_id[:doc_id.index('.')]

# overhauling
gaz2mmax_overhaul.chk_ner_range_for_spn_pos(
        tmp_nerTypes, doc_id, poslemCol, nerTypeCol, args.nertype_sanity_check)
if args.auto_citations:
    gaz2mmax_overhaul.add_citations(sentences, tokens, nerTypeCol)

if (os.path.exists(words_fname(args.outdir, doc_id))):
    if args.force:
        print >> sys.stderr, "MMAX file exists - removing everything"
        os.unlink(words_fname(args.outdir, doc_id))
        for mfile in glob.glob(markable_fname(args.outdir, doc_id, '*')):
            os.unlink(mfile)
        os.unlink(os.path.join(args.outdir,'%s.mmax'%(doc_id,)))
    else:
        print >> sys.stderr, "MMAX file exists - won't overwrite it"
        sys.exit(1)

write_dotmmax(args.outdir, doc_id)
write_basedata(words_fname(args.outdir, doc_id), tokens)
write_markables(args.outdir, doc_id, sentences)
write_markables(args.outdir, doc_id, nerTypeCol)
write_markables(args.outdir, doc_id, nerTypePosFilter)
write_markables(args.outdir, doc_id, entityCol)
write_markables(args.outdir, doc_id, poslemCol)
write_markables(args.outdir, doc_id, otherCols)
write_markables(args.outdir, doc_id, comments)
