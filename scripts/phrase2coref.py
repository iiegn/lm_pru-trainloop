#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import sys
import re
import getopt
from glob import glob
import os.path
from mmax_tools import MMAXDiscourse, links2sets, all_docs

unwanted_tokens=re.compile('(?:[,:;\\.`\'"]+|(?:ne|a|su|de|co)(?:l|ll[a\']|gli|i))$')
postmod_tokens=re.compile('(?:[,:;\\.]+|(?:ne|a|su|de|co)(?:l|ll[a\']|gli|i)|che|chi|qual[ei]|da|di|a|su|con|in)$')
defart_re=re.compile('(?:il|gli|i|la|le)$',re.I)
postmod_cats=['E','ES','EP','XPW','XPS',
              'CCHE','CCHI','C','PS-Q','PN-Q','PP-Q']
def frob_boundaries(doc,p0,p1):
    while unwanted_tokens.match(doc.tokens[p0]) and p0+1<p1:
        p0+=1
    while unwanted_tokens.match(doc.tokens[p1-1]) and p1>p0+1:
        p1-=1
    return (p0,p1)    

def min_ids(doc,p0,p1):
    chunk_end=p0+1
    if defart_re.match(doc.tokens[p0]) and chunk_end<p1:
        # to care for relative NPs, e.g. "i quali"
        chunk_end+=1
    while chunk_end<p1:
        if postmod_tokens.match(doc.tokens[chunk_end]):
            print "break before: %s"%(doc.tokens[chunk_end],)
            break
        chunk_end+=1
    if p0==chunk_end:
        chunk_end=p1
    return (p0,chunk_end)

def make_coref(phrases,doc):
    new_markables=[]
    for alvl,m_id,attrs,start_pos,end_pos in phrases:
        new_attrs={}
        if 'antecedente_singolo' in attrs:
            new_attrs['COREF']=attrs['antecedente_singolo']
        mtype=attrs['markable_type']
        if mtype=='nominale':
            new_attrs['markable_type']='np'
        elif mtype=='verbale':
            subtype=attrs['verbal_type']
            if subtype=='soggetto_vuoto':
                new_attrs['markable_type']='v.zero'
            elif subtype=='clitico':
                new_attrs['markable_type']='v.clitic'
            else:
                print >>sys.stderr,"Markable %s has unknown verbal_type '%s'"%(m_id,subtype)
        else:
            print >>sys.stderr,"Markable %s has unknown markable_type '%s'"%(m_id,mtype)
        (newstart,newend)=frob_boundaries(doc,start_pos,end_pos)
        (min_start,min_end)=min_ids(doc,newstart,newend)
        new_attrs['min_ids']=doc.make_span(min_start,min_end)
        try:
            new_attrs['sem_class']=attrs['categoria']
        except KeyError:
            pass
        new_markables.append(('coref',m_id,new_attrs,newstart,newend))
    return new_markables

def do_convert(dirname,basename):
    doc=MMAXDiscourse(dirname,basename)
    phrases=doc.read_markables('phrase')
    proto_coref=make_coref(phrases,doc)
    links2sets(proto_coref,keep_singletons=True)
    proto_coref.sort(key=lambda x: x[3])
    #print proto_coref
    doc.write_markables(proto_coref)

if __name__=='__main__':
    dirname=sys.argv[1]
    if len(sys.argv)<3:
        docs=all_docs(dirname)
    else:
        docs=sys.argv[2:]
    for docid in docs:
        do_convert(dirname,docid)
