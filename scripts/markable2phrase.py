import sys
import re
import getopt
import morphit_pos
from pynlp.mmax_tools import MMAXDiscourse, export_fname
import pytree.tree as tree
import pytree.export as export
import pytree.export2mrg as export2mrg

prepart_re=re.compile('(?:ne|a|su|de|da|co)(?:l|ll[a\']|gli|i)$',re.I)

def markables2phrases(markables,doc):
    result=[]
    for alvl,m_id,attrs,start_pos,end_pos in markables:
        new_attrs={}
        mtype=attrs['markable_type']
        if mtype.startswith('v.'):
            new_attrs['markable_type']='verbale'
            if mtype.startswith('v.clitic'):
                new_attrs['verbal_type']='clitico'
            elif mtype=='v.zero':
                new_attrs['verbal_type']='soggetto_vuoto'
            else:
                print >>sys.stderr, "Unknown verbal markable_type: %s"%(mtype,)
        else:
            new_attrs['markable_type']='nominale'
            if mtype=='np':
                # add prepart to the span of NP markables
                if start_pos>0 and prepart_re.match(doc.tokens[start_pos-1]):
                    start_pos-=1
        if attrs['gend']=='any':
            new_attrs['genere']='unmarked'
        else:
            new_attrs['genere']=attrs['gend']
        if attrs['num']=='any':
            new_attrs['numero']='unmarked'
        else:
            new_attrs['numero']=attrs['num']
        mention_type=attrs['mention_type']
        if mention_type.startswith('pro.per'):
            new_attrs['persona']=mention_type[4:]
        else:
            new_attrs['persona']='per3'
        new_attrs['riferimento']='unmarked'
        result.append(('phrase',None,new_attrs,start_pos,end_pos))
    #print result
    return result

anno_dir=sys.argv[1]
for docid in sys.argv[2:]:
    print >>sys.stderr,"%s (markable2phrase)"%(docid,)
    doc=MMAXDiscourse(anno_dir,docid)
    markables=doc.read_markables('markable')
    phrases=markables2phrases(markables,doc)
    doc.write_markables(phrases)
