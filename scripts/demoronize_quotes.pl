#!/usr/bin/perl
# -*- encoding: utf8 -*-

# reduces the *huge* variety of different quotes that UTF-8 offers
# to something we are actually prepared to handle.

while(<>)
{
    s/“/"/g;
    s/”/"/g;
    s/″/"/g;
    s/˝/"/g;
    s/’/'/g;
    s/‘/'/g;
    s/–/-/g;
    s/—/-/g;
    s/…/ /g;
    print;
}
