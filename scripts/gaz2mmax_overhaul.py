#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
overhaul-ing of gaz2mmax results - filtering, augmenting, deleting,...
"""
DESCRIPTION = __doc__

import logging
from pyparsing import (Literal, Suppress, Combine, OneOrMore, Word, alphas,
        ZeroOrMore, oneOf, Empty, Group, Optional, nums, ParseException,
        getTokensEndLoc)
import re
import string

log = logging.getLogger(__name__.split('_')[-1])

def get_clean_nertype_tup(strng):
    """splits UID-after-the-"#" at the end and gets rid of
       'found' at the beginning then, returns (nerType, UID).
    """
    if strng.startswith("found"):
        return strng[5:].rsplit('#')
    else:
        return strng.rsplit('#')

def chk_ner_range_for_spn_pos(tmp_nerTypes, doc_id, poslemCol, nerTypeCol, check):
    for nertypestr in tmp_nerTypes:
        nertype, neid = get_clean_nertype_tup(nertypestr)
    
        # check: at least one pos_tag within nerType range is 'spn' OR
        # manually added tag
        if (("spn" in ([poslem[2]["pos_tag"] 
            for poslem in 
            poslemCol[tmp_nerTypes[nertypestr][0]:tmp_nerTypes[nertypestr][1]]])) or
            (not check)):
    
            nerTypeCol.append(('ne', None, 
                {"ne_type":nertype,"ne_id":neid},tmp_nerTypes[nertypestr][0],
                tmp_nerTypes[nertypestr][1]))
        else:
            log.debug("discarding: %s" %(neid.lower()))

    
def add_citations(sentences, tokens, nerTypeCol):

    #class tokid_str(str):
    #    def __new__(cls, str_val, tokid_val, *args, **kwargs):
    #        newobj = super(tokid_str, cls).__new__(cls, str_val, *args, **kwargs)
    #        newobj.tid = tokid_val
    #        return newobj

    def citation_grammar():
        """Return the pyparsing grammar for citations.
        """
        # helper function to capture the char location of a match
        #loc_none = Empty().setParseAction(lambda s,locn,toks: locn)
        def loc(s,locn,toks):
            return((locn, getTokensEndLoc(), toks[0]))

        # structuring chars - important for parsing but not in the result
        comma = Suppress(Literal(","))
        colon = Suppress(Literal(":"))
        semicolon = Suppress(Literal(";"))
        open_br = Suppress(Literal("("))
        close_br = Suppress(Literal(")"))

        cit_prefix = Literal("see") | Literal("e.g.") | Literal("c.f.")
        cit_start = Suppress(open_br + Optional(cit_prefix))
        cit_end = Suppress(close_br)
        etal = Literal("et alii") | Literal("et al.")
        etal = etal.suppress()
        year_text = Literal("in questo volume") | Literal("C. d . S.")
        year_text = year_text.suppress()

        # Author
        author = Combine(OneOrMore(~year_text + Word(alphas+"-")) + 
                ZeroOrMore(Word("' ") + ~(etal|year_text) + Word(alphas+"-")))
        author = author.setParseAction(loc)
        author_sep = Suppress(oneOf("& ,") + Empty())
        #
        authors = Group(author + 
                ZeroOrMore(author_sep + author) + 
                Optional(etal))
        authors = authors.setResultsName("authors")

        # Year(s)
        year_digits = Word(nums, exact=4)
        year_digits = year_digits.setResultsName("year") 
        year_digits = year_digits.setParseAction(loc)
        year_idx = Suppress(Word(alphas, exact=1))
        year = (year_digits + Optional(year_idx)) | year_text

        # Page(s) and Figure
        page = Word(nums)
        pages = Word(nums) + Optional(Word("-,") + Word(nums))
        pages = Suppress(pages)
        figure = Literal("fig") + Word(".") +  OneOrMore(Word(nums+alphas+",.- "))
        figure = Suppress(figure)

        citation = Group(authors + Optional(comma) + 
                year + Optional(colon + 
                    ( (pages + comma + figure) | pages | figure ) ))
        citations = Group(citation + Optional(semicolon + citation))
        citations = citations.setResultsName("citations") 
        return (cit_start + citations + cit_end)

    cite_grammar = citation_grammar()

    authors = []
    years = []
    # process all sentece tuples of token ids (start,end)...
    for starttok_id, endtok_id in [(sent[3],sent[4]) for sent in sentences]:
        # ...and for all tokens within a sentence find
        # ranges of opening and closing brackets 
        rngs = []
        for tokid in range(starttok_id, endtok_id):
            token = tokens[tokid]

            if token == '(':
                rngs.append(tokid)
            elif token == ')' and len(rngs) > 0:
                rng_spos = rngs.pop()
                rng_epos = tokid

                # in case there has been a 'matching' opening bracket check
                # whether we have a citation in between
                if (rng_spos + 1) < (rng_epos):

                    # make sure encoding issues don't mess up length
                    # calculations and string matching;
                    # then, build the mapping from the current string, i.e.
                    # a sub-part of the sentence enclosed in (), to the token
                    # ids from the sentence.
                    posbl_cite_str = " ".join([tokens[tok].
                        decode('utf-8', 'ignore')
                        for tok in range(rng_spos,rng_epos+1)])
                    posbl_cite_str_lens = [len(tokens[tok].
                        decode('utf-8', 'ignore')) + 1 
                        for tok in range(rng_spos,rng_epos+1)]
                    pos = [0]  # the first token starts at pos:0
                    for ln in posbl_cite_str_lens[:-1]:
                        pos.append(pos[-1] + ln)
                    pos_mappings = zip(pos, range(rng_spos,rng_epos+1))
                    
                    # parse the current sub-string and check for citations
                    try:
                        res = cite_grammar.parseString(posbl_cite_str)
                        log.info("success: %s" %posbl_cite_str)

                        # process each found citation:
                        #  - extract authors, and (poss.) a corresponding year
                        #  - map positions back to token ids from the sentence
                        for c_num, citation in enumerate(res.citations):
                            y = None
                            for a_num, author in enumerate(citation.authors):
                                y = citation.year
                                st_tok_id = en_tok_id = 0
                                log.debug("c%d, a%d: '%s'" 
                                        %(c_num, a_num, author))
                                for cite_str_pos, tok_id in pos_mappings:
                                    if cite_str_pos == author[0]: 
                                        st_tok_id = tok_id
                                    if cite_str_pos < author[1]:
                                        en_tok_id = tok_id
                                authors.append((st_tok_id,en_tok_id))
                                log.debug("start_tok_id:%d, end_tok_id:%d" 
                                        %(st_tok_id, en_tok_id))
                            if y: 
                                for year_str_pos, tok_id in pos_mappings:
                                    if year_str_pos == y:
                                        years.append(tok_id)
                                        log.debug("tok_id:%d" %tok_id)
                                #print "c%d, y: '%s'" %(c_num, y)
                    # the grammar doesn't match the string => no citation
                    except ParseException as e:
                        log.info("fail: %s - %s" %(posbl_cite_str, e))
                        pass

    # use the newly built author, year lists, check for overlap with old
    # nerType ranges (and if so delete the old one), and finally, add the
    # new ones.
    # (check, delete)
    for id, (_ne, _none, nerdict, start_tokid, end_tokid) in enumerate(
            nerTypeCol):

        # print nerdict, start_tokid, end_tokid
        if nerdict["ne_type"] == "Pubauthor":
            for stid, etid in authors:
                if ((stid >= start_tokid and stid <= end_tokid) or
                        etid >= start_tokid and etid <= end_tokid):
                    log.info("*" + " ".join(
                        [tokens[i] for i in range(stid, etid + 1)]))
            log.info("%s, %d, %d" %(nerdict, start_tokid, end_tokid))
            nerTypeCol.pop(id)
        if nerdict["ne_type"] == "Pubyear":
            for tid in years:
                if ((tid >= start_tokid and tid <= end_tokid)):
                    log.info("*" + tokens[tid])
            log.info("%s, %d, %d" %(nerdict, start_tokid, end_tokid))
            nerTypeCol.pop(id)
    # (add)
    for stid, etid in authors:
        nerTypeCol.append(('ne', None, 
            {"ne_type":"Pubauthor","ne_id":""},stid,
            etid+1))
    for tid in years:
        nerTypeCol.append(('ne', None, 
            {"ne_type":"Pubyear","ne_id":""},tid,
            tid+1))
