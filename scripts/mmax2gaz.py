#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

dsc = """
converts a gaz2mmax.py converted MMAX file back to TextPro format.
"""

import argparse
import base64
import logging
import os, os.path
import pickle
import sys
import uuid
from pynlp.mmax_tools import read_basedata, read_markables, words_fname

parser = argparse.ArgumentParser(description=dsc)
#parser.add_argument("-v", dest="log_level",
#        action="store_const", const=logging.INFO,
#        help="be informative while processing")
#parser.add_argument("-vv", dest="log_level",
#        action="store_const", const=logging.DEBUG,
#        help="be very informative while processing (DEBUG)")
parser.set_defaults(log_level=None)
parser.add_argument("mmax_file", type=file)
args = parser.parse_args()

# init logging
log_level = args.log_level
log = logging.getLogger(sys.argv[0])
log = logging.basicConfig(format='%(name)s:%(message)s', level=log_level)

tokens = []     # word forms, sequencial list of tokens
nerTypeCol = [] # ('ne',None,nerType,startTok,endTok)
nerTypePosFilter = []
entityCol = [] 
poslemCol = [] 
otherCols = []  # keep the non-nerTypes to be able to reconstruct the 'txp' file
                # the content will be a list, pickled and, finally, b64encoded
sentences = []  # ('sentence',None,{},startTok,endTok)
comments = []   # keep the 'comments' to be able to reconstruct the 'txp' file

mmax_fname = args.mmax_file.name;
doc_dir = os.path.dirname(mmax_fname)
doc_id = os.path.basename(mmax_fname)
if '.' in doc_id:
    doc_id = doc_id[:doc_id.index('.')]

try:
    tokens, token_ids, ids = read_basedata(words_fname(doc_dir, doc_id))
    sentences =  read_markables(doc_dir, doc_id, 'sentence', token_ids)
    # these can be empty in case no nerType had been regocnized!
    try:
        nerTypeCol = read_markables(doc_dir, doc_id, 'ne', token_ids)
        nerTypePosFilter = read_markables(doc_dir, doc_id, 
                'neposfilter', token_ids)
    except IOError:
        pass
    # this one might be empty in case TextPro 'fails' on the file
    try:
        entityCol = read_markables(doc_dir, doc_id, 'entity', token_ids)
    except IOError:
        pass
    poslemCol = read_markables(doc_dir, doc_id, 'poslem', token_ids)
    otherCols = read_markables(doc_dir, doc_id, 'other_cols', token_ids)
    comments = read_markables(doc_dir, doc_id, 'comments', token_ids)
except:
    print >> sys.stderr, "MMAX file could not be read."
    raise
    # sys.exit(1)

dict_tokid2nerType = {}
for _my_ne_id, nerType in enumerate(nerTypeCol):
    _my_uuid = str(uuid.uuid1())
    for tokid in range(nerType[3], nerType[4]):
        ne_id = "#" + nerType[2].get("ne_id", "".
                join([doc_id.lower(), "-", _my_uuid]))
        dict_tokid2nerType[tokid] = (dict_tokid2nerType.get(tokid, []) +
                [[nerType[2]["ne_type"],ne_id]])

dict_tokid2entity = {}
for entity in entityCol:
    for tokid in range(entity[3], entity[4]):

        value = entity[2].get("value")
        if not value:
            value = "O"
        dict_tokid2entity[tokid] = (dict_tokid2entity.get(tokid, "") + 
                value)

for sid in range(len(sentences)):
    starttok = sentences[sid][3]
    endtok = sentences[sid][4]

    # print the 'comments' (TextPro meta data)
    for line in pickle.loads(base64.b64decode(comments[sid][2]["value"])):
        print "#", line

    # print the tab-delim lines
    for tokid in range(starttok, endtok):
        line = ''
        # the token
        line += tokens[tokid]
        # the 'other' columns
        for s in pickle.loads(base64.b64decode(
            otherCols[tokid][2]["value"])):
            line += '\t'+s
        # the pos and lem
        line += '\t'+poslemCol[tokid][2]["pos_tag"].upper()
        line += '\t'+pickle.loads(base64.b64decode(
            poslemCol[tokid][2]["lemma"]))
        # the entity column
        if tokid in dict_tokid2entity:
            line += '\t'+dict_tokid2entity[tokid]
        else:
            line += '\tO'
        # the nerType column
        if tokid in dict_tokid2nerType:
            for neid, nerType in enumerate(sorted(dict_tokid2nerType[tokid])):
                delim = '\t'
                if neid > 0:
                    delim = '/'
                line += delim+(nerType[0].
                        capitalize()+nerType[1].lower())
        else:
            line += '\tO'
        print line
    # empty line between sentences
    print
