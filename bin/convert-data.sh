#!/bin/bash

# this will give an absolute path to the parent dir of the current script
export LM_COREF=${LM_COREF:-$(readlink -f $(dirname ${0})/..)}

export TEXTPRO=${TEXTPRO:-$LM_COREF/TextPro}
TOOLS_DIR=$LM_COREF/scripts
export PYTHONPATH=${PYTHONPATH:-$LM_COREF/python-libs}

[ "$#" -eq 2 ] || {
    echo "convert-data.sh - takes a bunch of .exp files and creates"
    echo "the MMAX2 files"
    echo "Usage: convert-data.sh <ParseDir> <MMAXDir>" ; exit 1
}

mkdir -p "$2"
tar -C "$2" -xzf "$LM_COREF/data/mmax_boilerplate.tgz" || exit $?

for file in $(find "$1" -maxdepth 1 -name '*.exp')
do
    echo "INFO: $0 for $file."
    fb=$(readlink -f $(dirname "$file")/$(basename "$file" .exp))
    DOCID=$(basename "$fb")

    cp "$file" "$2/parsing/${DOCID}.exp" \
    || { echo "ERR: $0 for $file - cp failed." >&2; continue; }

    python "$TOOLS_DIR/exp2mmax.py" "$file" "$2" \
    || { echo "ERR: $0 for $file - exp2mmax.py failed." >&2; continue; }
    
    python "$TOOLS_DIR/add_parses.py" -u "$2" "$DOCID" \
    || { echo "ERR: $0 for $file - add_parses.py failed." >&2; continue; }
    
    python "$TOOLS_DIR/markable2phrase.py" "$2" "$DOCID" \
    || { echo "ERR: $0 for $file - markable2phrase.py failed." >&2; continue; }
done
