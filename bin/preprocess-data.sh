#!/bin/bash

# this will give an absolute path to the parent dir of the current script
export LM_COREF=${LM_COREF:-$(readlink -f $(dirname ${0})/..)}

export TEXTPRO=${TEXTPRO:-$LM_COREF/TextPro}
TOOLS_DIR=$LM_COREF/scripts

[ "$#" -eq 1 ] || {
    echo "Usage: preprocess-data.sh <Directory>" ; exit 1
}

set -o pipefail

for file in $(find "$1" -maxdepth 1 -name '*.txt')
do
    fb=$(readlink -f $(dirname "$file")/$(basename "$file" .txt))
    [ -f "${fb}.conll-in" ] && { echo "INFO: $0 for $file - output exists, skipping."; continue; }
    echo "INFO: $0 for $file."

    # guess encoding based on what file(1) says
    if file "${fb}.txt" | grep 'ISO-8859' > /dev/null
    then
        cat "${fb}.txt" > "${fb}.iso" || { echo "ERR: $0 for $file - cat failed." >&2; rm -v "${fb}.iso"; continue; } 
    else
        perl $TOOLS_DIR/demoronize_quotes.pl "${fb}.txt" \
        | iconv -s -f UTF-8 -t ISO-8859-15//TRANSLIT \
        > "${fb}.iso" \
        || { echo "ERR: $0 for file - demoronize_quotes.pl | iconv failed." >&2; rm -v "${fb}.iso"; continue; }
    fi
    perl "$TEXTPRO/textpro.pl" -y -l ita -c token+sentence+pos+lemma "${fb}.iso" \
    || { echo "ERR: $0 for $file - textpro failed." >&2; rm -v "${fb}.iso.txp"; continue; } 

    perl "$TOOLS_DIR/txp2conll.pl" "${fb}.iso.txp" \
    > "${fb}.conll-in" \
    || { echo "ERR: $0 for $file - txp2conll.pl failed." >&2; rm -v "${fb}.conll-in"; continue; }
    
done
