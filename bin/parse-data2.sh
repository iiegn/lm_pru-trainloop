#!/bin/bash
HERE=$(dirname $(which $0))
export LM_COREF=${LM_COREF:-${HERE/bin}}
export TEXTPRO=${TEXTPRO:-$LM_COREF/TextPro1.4}
export PYTHONPATH=$LM_COREF/python-libs
MALT=$LM_COREF/malt-1.2
TOOLS_DIR=$LM_COREF/scripts

[ "$#" -eq 1 ] || {
    echo "Usage: parse-data.sh <Directory>" ; exit 1
}

for i in $1/*.token; do
    CUR_DIR=`pwd`
echo $CUR_DIR;
echo $1;
    ( cd $MALT ;
	java -Xmx300m -jar malt.jar -c tut-all.mco -i $CUR_DIR/${i/.token}.conll-in -ic iso-8859-15 -o $CUR_DIR/${i}.conll-utf ) || exit $?
#    iconv -f UTF-8 -t ISO-8859-1 ${i/.token}.conll-utf > ${i/.token}.conll \
#	|| exit $?
#    rm ${i/.txt}.conll-utf
#    python $TOOLS_DIR/conll2export.py ${i/.txt}.conll | \
#	python $TOOLS_DIR/unmarkov_exp.py > ${i/.txt}.exp \
#	|| exit $?
done
