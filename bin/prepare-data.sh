#!/bin/bash
HERE=$(dirname $(which $0))
export LM_COREF=${LM_COREF:-${HERE/bin}}
export TEXTPRO=${TEXTPRO:-$LM_COREF/TextPro1.4}
MALT=$LM_COREF/malt-1.2
TOOLS_DIR=$LM_COREF/scripts
PYTHONPATH=$LM_COREF/python-libs

[ "$#" -eq 1 ] || {
    echo "Usage: preprocess-data.sh <Directory>" ; exit 1
}

#for i in $1/*tab; do
#cut -s -f 1 $i > ${i}.token || exit $?
#done

for i in $1/*.token
do
    ACTUAL_DIR=`pwd`

    # guess encoding based on what file(1) says
    #    if file $i | grep 'ISO-8859' > /dev/null ; then
    #	perl $TOOLS_DIR/add_sentbreak.pl $i > ${i/.token}.iso || exit $?
    #    else
    #	perl $TOOLS_DIR/demoronize_quotes.pl $i | \
    #	    iconv -f UTF-8 -t ISO-8859-15 | \
    #	    perl $TOOLS_DIR/add_sentbreak.pl > ${i/.token}.iso || exit $?
    #    fi
    #    perl $TEXTPRO/textpro -l italian ${i/.token} || exit $?
    perl $TEXTPRO/textpro -l ita -c sentence+token+pos+lemma -d token ${i} \
    || exit $?

    perl /home/kepa.rodriguez/LM_Coref/scripts/txp2conll.pl ${i}.txp > ${i}.conll-in \
    || exit $?

    perl -ne 'if ($_) { split; $_[5]='_'; print join("\t",@_),"\n"; } else
    { print "\n"; }' ${i}.conll-in > ${i}.new.conll-in


    (cd $MALT;
    java -Xmx300m -jar malt.jar -v off -cl off -c tut-all.mco -i $ACTUAL_DIR/${i}.conll-in -ic iso-8859-15 -o  $ACTUAL_DIR/${i}.conll-utf) \
    || exit $?
done
