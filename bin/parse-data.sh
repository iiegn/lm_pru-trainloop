#!/bin/bash

# this will give an absolute path to the parent dir of the current script
export LM_COREF=${LM_COREF:-$(readlink -f $(dirname ${0})/..)}

export TEXTPRO=${TEXTPRO:-$LM_COREF/TextPro}
export PYTHONPATH=${PYTHONPATH:-$LM_COREF/python-libs}
MALT=$LM_COREF/malt
TOOLS_DIR=$LM_COREF/scripts

[ "$#" -eq 1 ] || {
    echo "Usage: parse-data.sh <Directory>" ; exit 1
}

set -o pipefail

for file in $(find "$1" -maxdepth 1 -name '*.conll-in')
do
    fb=$(readlink -f $(dirname "$file")/$(basename "$file" .conll-in))
    [ -f "${fb}.exp" ] && { echo "INFO: $0 for $file - output exists, skipping."; continue; }
    echo "INFO: $0 for $file."
    
    java -Xmx300m -jar "$MALT/malt.jar" \
    -c tut-all.mco \
    -i "${fb}.conll-in" \
    -ic iso-8859-15 \
    -w "$MALT" \
    -o "${fb}.conll.utf8" \
    -v fatal \
    || { echo "ERR: $0 for $file - malt.jar failed." >&2; rm -v "${fb}.conll.utf8"; continue; }

    iconv -f UTF-8 -t ISO-8859-1 "${fb}.conll.utf8" \
    > "${fb}.conll" \
    && rm "${fb}.conll.utf8" \
    || { echo "ERR: $0 for $file - iconf failed." >&2; rm -v "${fb}.conll"; continue; } 
    
    python "${TOOLS_DIR}/conll2export.py" "${fb}.conll" \
    | python "${TOOLS_DIR}/unmarkov_exp.py" > "${fb}.exp" \
    || { echo "ERR: $0 for $file - conll2export.py | unmarkov_exp.py failed." >&2; rm -v "${fb}.exp"; continue; }

done
