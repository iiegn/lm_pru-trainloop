#!/bin/bash

# this will give an absolute path to the parent dir of the current script
export LM_COREF=${LM_COREF:-$(readlink -f $(dirname ${0})/..)}

export TEXTPRO=${TEXTPRO:-$LM_COREF/TextPro}
TOOLS_DIR=$LM_COREF/scripts
export PYTHONPATH=${PYTHONPATH:-$LM_COREF/python-libs}

[ "$#" -eq 2 ] || {
    echo "$(dirname ${0}): takes a bunch of .txp files and creates"
    echo "the MMAX2 files for the nerTypes column"
    echo "Usage: $(dirname ${0}): <ParseDir> <MMAXDir>" ; exit 1
}

mkdir -p "$2"
tar -C "$2" -xzf "$LM_COREF/data/mymmax_boilerplate.tgz" || exit $?

for file in $(find "$1" -maxdepth 1 -name '*.txt.txp.gaz')
do
    echo "INFO: $0 for $file."
    fb=$(readlink -f $(dirname "$file")/$(basename "$file" .exp))
    DOCID=$(basename "$fb")

    python "$TOOLS_DIR/gaz2mmax.py" "$file" "$2" "${AUTO_CITATIONS}" \
    || { echo "ERR: $0 for $file - gaz2mmax.py failed." >&2; continue; }
done
