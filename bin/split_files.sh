#!/bin/bash

## split_files: splits a directory full of MMAX files into a form
## where each file is in its own directory

for i in $1/*.mmax ; do
    BASENAME=$(basename $i)
    DOCID=${BASENAME/.mmax}
    mkdir -p $2/$DOCID
    rm -rf $2/$DOCID/*
    for subdir in Basedata markables parsing ; do
	mkdir -p $2/$DOCID/$subdir
	cp $1/$subdir/$DOCID* $2/$DOCID/$subdir
    done
    cp $i $2/$DOCID
    cp -r $1/CommonFiles $2/$DOCID/
done