#!/bin/bash

# this will give an absolute path to the parent dir of the current script
# (in case a script should also be source-able:
# ${BASH_SOURCE[0]} instead of ${0})
export LM_COREF=${LM_COREF:-$(readlink -f $(dirname ${0})/..)}

[ "$#" -eq 2 ] || {
    echo "lm_convert.sh - takes text files, parses them and creates"
    echo "the MMAX2 files"
    echo "Usage: lm_convert.sh <TextDir> <MMAXDir>" ; exit 1
}

"${LM_COREF}/bin/preprocess-data.sh" "$1"
"${LM_COREF}/bin/parse-data.sh" "$1"
"${LM_COREF}/bin/convert-data.sh" "$1" "$2"
