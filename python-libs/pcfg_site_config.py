import os
import os.path

try:
    pynlp_dir=os.environ['PYNLP']
except KeyError:
    pynlp_dir='pynlp'
try:
    pynlp_grammar=os.environ['PYNLP_GRAMMAR']
except KeyError:
    pynlp_grammar='sclass4'
grammar_dir=os.path.join(pynlp_dir,'grammar',pynlp_grammar)
smor_dir=os.path.join(pynlp_dir,'data/smor/')
names_dir=os.path.join(pynlp_dir,'data/names/')
cdg_mor_dir=os.path.join(pynlp_dir,'data/cdg-mor/')
cdg_dir=os.path.join(pynlp_dir,'data/cdg/')
matrix_dir=os.path.join(pynlp_dir,'data/matrix')
