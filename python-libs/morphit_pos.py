#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import sys
import sfst
import re
import os
import os.path

try:
    data_dir=os.path.join(os.environ['LM_COREF'],'data')
except KeyError:
    print >>sys.stderr,"$LM_COREF is not set - cannot find the data files :-("
    sys.exit(1)
morphit_ca=sfst.load_compact(os.path.join(data_dir,'morphit_pippi.ca'))

noun_re=re.compile('<NOUN><([MF])><([sp])>$')

any_verb_re=re.compile('<(VER|AUX|MOD)>')
vind_re=re.compile('<(VER|AUX|MOD)><(ind|sub|cond)><(pres|impf|past|fut)><([123])><([sp])>$')
vinf_re=re.compile('<(VER|AUX|MOD)><inf><pres>(<(?:si|la|lo|le|li|mi|vi|ci|ne|sene|gli)>|)$')
vpp_re=re.compile('<(VER|AUX|MOD)><part><past><([sp])><([mf])>$')
vprg_re=re.compile('<(VER|AUX|MOD)><ger><pres>(<(?:si|la|lo|le|li|mi|vi|ci|ne|sene|gli)>|)$')
vimp_re=re.compile('<(VER|AUX|MOD)><impr><pres><([123])><([sp])>(<(?:si|la|lo|le|li|mi|vi|ci|ne|sene|gli)>|)$')

adj_re=re.compile('<ADJ><(pos|comp|sup)><([mf])><([sp])>$')

art_re=re.compile('<ART><([MF])><([sp])>')
det_re=re.compile('<DET><(INDEF|DEMO|POSS|WH)><([mf])><([sp])>$')
prepart_re=re.compile('<ARTPRE><([MF])><([sp])>$')

pron_re=re.compile('<PRO><(POSS|DEMO|INDEF|WH)><([MF])><([SP])>')
ppron_re=re.compile('<PRO><PERS>(<CLI>)?<([123])><([MF])><([SP])>')


aux_part={'VER':'', 'MOD':'', 'AUX':'Y'}

pred_verbs=['essere','stare','diventare']
modal_verbs=['dovere','potere','volere']

prepart_map2={'ne':'in','de':'di'}

prepart_ending_re=re.compile('([lg](?:l[aioe\'])?|i)$')

# heuristics for word forms
number_re=re.compile('[0-9](?:[0-9\.\,]*[0-9])?$')

def map_prepart(w):
    w=w.lower()
    if prepart_ending_re.search(w):
        w1=prepart_ending_re.sub('',w)
        if w1 in prepart_map2:
            return prepart_map2[w1]
        else:
            return w1
    else:
        return w


trans_verbs=set()
for line in file(os.path.join(data_dir,'transverb.txt')):
    l=line.strip().split()
    trans_verbs.add(l[0])

def classify_verb(lemma,aux=False):
    if lemma in pred_verbs or lemma=='venire' and aux:
        return 'pred'
    elif lemma in modal_verbs:
        return 'mod'
    elif lemma=='avere':
        return 'ave'
    elif lemma in trans_verbs:
        return 'trans'
    else:
        return 'intr'

def add_cl(s):
    if s:
        return '+E'
    else:
        return ''

temp_map={'pres':'s','past':'t','impf':'i','fut':'f'}

def verb_analysis(a):
    m=vind_re.search(a)
    if m:
        vclass='_'+classify_verb(a[:m.start()],m.group(1)=='AUX')
        return ('VI'+aux_part[m.group(1)]+vclass,
                a[:m.start()],
                m.group(4)+m.group(5)+m.group(2)[0]+temp_map[m.group(3)])
    m=vinf_re.search(a)
    if m:
        vclass='_'+classify_verb(a[:m.start()],m.group(1)=='AUX')
        return ('VF'+aux_part[m.group(1)]+add_cl(m.group(2))+vclass,
                a[:m.start()],
                '--')
    m=vpp_re.search(a)
    if m:
        vclass='_'+classify_verb(a[:m.start()],m.group(1)=='AUX')
        return ('V%sP%s%s'%(m.group(2).upper(),aux_part[m.group(1)],vclass),
                a[:m.start()],
                m.group(2)+m.group(3))
    m=vprg_re.search(a)
    if m:
        vclass='_'+classify_verb(a[:m.start()],m.group(1)=='AUX')
        return('VG'+aux_part[m.group(1)]+add_cl(m.group(2))+vclass,
               a[:m.start()],
               '--')
    m=vimp_re.search(a)
    if m:
        vclass='_'+classify_verb(a[:m.start()],m.group(1)=='AUX')
        return('VM'+aux_part[m.group(1)]+add_cl(m.group(4))+vclass,
               a[:m.start()],
               m.group(2)+m.group(3))
    return ('???'+a,a[:a.index('<')],'--')

# TODO: incorporate verb clitics into analysis
# e.g. 

def possible_ana(word):
    if word=='che':
        return [('CCHE','che','--')]
    elif word=='chi':
        return [('CCHI','chi','--')]
    elif number_re.match(word):
        return [('N',word,'--')]
    ass=morphit_ca.analyze_string(word)
    pos=[]
    for a in ass:
        if '<NOUN>' in a:
            m=noun_re.search(a)
            assert m,a
            wcat='S'+m.group(2).upper()
            for ending in ['t�','ing']:
                if word.endswith(ending):
                    wcat='SN'
            pos.append((wcat,
                        a[:m.start()],
                        m.group(2)+m.group(1).lower()))
        elif '<CAU>' in a or '<part><pres>' in a:
            # we don't distinguish between full-verb and causal use here...
            # and we don't distinguish part-pres from other adjectives
            continue
        elif any_verb_re.search(a):
            pos.append(verb_analysis(a))
        elif '<ADJ>' in a:
            m=adj_re.search(a)
            assert m,a
            pos.append(('A'+m.group(3).upper(),
                        a[:m.start()],
                        m.group(3)+m.group(2)))
        elif '<ADV>' in a:
            pos.append(('B',a[:a.index('<ADV>')],'--'))
        elif '<PRE>' in a:
            pos.append(('E',a[:a.index('<PRE>')],'--'))
        elif '<ARTPRE>' in a:
            m=prepart_re.search(a)
            assert m,a
            pos.append(('E'+m.group(2).upper(),
                        map_prepart(word),
                        m.group(2)+m.group(1).lower()))
        elif '<CON>' in a:
            n=a.index('<CON>')
            pos.append(('C_coo',a[:n],'--'))
            pos.append(('C_sub',a[:n],'--'))
        elif '<NUM>' in a:
            pos.append(('N',a[:a.index('<')],'--'))
        elif '<PRO>' in a:
            if '<COM>' in a:
                # TagPro says PS here, so...
                pos.append(('PS',a[:a.index('<')],'**'))
            elif '<PERS>' in a:
                m=ppron_re.search(a)
                assert m,a
                pos.append(('P'+m.group(4),a[:m.start()],
                            m.group(4).lower()+m.group(3).lower()
                            +m.group(2).lower()))
            else:
                m=pron_re.search(a)
                assert m,a
                pos.append(('P'+m.group(3),
                            a[:m.start()],
                            m.group(3).lower()+m.group(2).lower()
                            +'3'))
        elif '<ART>' in a:
            m=art_re.search(a)
            assert m,a
            pos.append(('R'+m.group(2).upper(),
                        a[:m.start()],
                        m.group(2)+m.group(1).lower()))
        elif '<DET>' in a:
            m=det_re.search(a)
            assert m,a
            pos.append(('D'+m.group(3).upper(),
                        a[:m.start()],
                        m.group(3)+m.group(2).lower()))
        else:
            pos.append(('???'+a,a[:a.index('<')],'--'))
    return pos

def possible_pos(word):
    pos_lst=[]
    for pos,lemma,morph in possible_ana(word):
        pos_lst.append(pos)
    return set(pos_lst)

upper_cat=['SPN']
def get_lemma(word,pos,morph=None):
    cand=[]
    for pos_a,lemma,morph_a in possible_ana(word):
        score=0
        if pos_a==pos:
            score += 2
            if morph and morph==morph_a:
                score +=2
        elif pos_a[0]==pos[0]:
            score +=1
        cand.append((score,lemma))
    if cand:
        cand.sort()
        return cand[-1][1]
    else:
        if pos not in upper_cat:
            return word.lower()
        else:
            return word
    
