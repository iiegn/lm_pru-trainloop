import re

markov_re=re.compile('<.*')
deco_re=re.compile('_.*')
def coarsify_func(sym):
    add_at=False
    if markov_re.search(sym):
        add_at=True
        sym=markov_re.sub('',sym)
    sym=deco_re.sub('',sym)
    if add_at:
        return '@'+sym
    else:
        return sym

nominalCats=frozenset(['NP'])
nominalPOS=frozenset(['SS','SP','SN'])
vx_cats=frozenset(['VXI','VXG','VXF','VXP'])
main_verb_cats=frozenset(['VI','VG','VF','VSP','VPP'])
compl_cat=frozenset(['CCHE','CADV','C_sub'])

hr_table=[(['PX','PX-REL'],[(['E','ES','EP'],'l')]),
    ('NP',[(None,'HD','r'),(['NUM-P','ADJP'],'l')]),
    ('ADJP',[(None,'HD','r')]),
    ('ADVP',[(None,'HD','r')]),
    (vx_cats,[(None,'HD','r'),(main_verb_cats,'r')]),
    (['S','S-REL'],[(vx_cats,'l')]),
    ('SBAR',[(None,'HD','r'),(compl_cat,'r')]),
    ('CONJ-P',[(None,'HD','r'),(compl_cat,'r')]),
    (['@NP','@ADJP','@ADVP','@PX','@S'],
     [(None,'KONJ','l'),(None,'M-REST','l')])]

mod_hr_table=[(['PX','PX-REL'],[(['E','ES','EP'],'l')]),
              ('NP',[(None,'HD','r'),(['NUM-P','ADJP'],'l')]),
              ('NUM-P',[(None,'HD','r'),('N','l')]),
              ('ADJP',[(None,'HD','r')]),
              ('ADVP',[(None,'HD','r')]),
              (vx_cats,[(None,'HD','r'),(main_verb_cats,'r')]),
              (['S','S-REL'],[(vx_cats,'l')]),
              ('SBAR',[(None,'HD','r'),(compl_cat,'r')]),
              ('CONJ-P',[(None,'HD','r'),(compl_cat,'r')]),
              (None,[(None,'HD','r'),(None,'KONJ','l'),(None,'l')])]
