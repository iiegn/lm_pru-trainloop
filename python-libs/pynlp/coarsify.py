from collections import defaultdict
from itertools import izip
import os.path

def coarsify_rules(coarsify_func, f_in, f_out):
    freq=defaultdict(int)
    for l in f_in:
        line=l.strip().split()
        freq[tuple([coarsify_func(x) for x in line[1:]])]+=int(line[0])
    f_in.close()
    all_rules=[(n,xs) for (xs,n) in freq.iteritems()]
    all_rules.sort(reverse=True)
    for n,xs in all_rules:
        print >>f_out, "%s\t%s"%(n,' '.join(xs))
    f_out.close()

def coarsify_lexicon(coarsify_func, f_in, f_out):
    freq=defaultdict(int)
    for l in f_in:
        line=l.strip().split()
        freq.clear()
        for k,n in izip(line[1::2],line[2::2]):
            freq[coarsify_func(k)]+=int(n)
        vals=freq.items()
        vals.sort()
        print >>f_out, '%s\t%s'%(line[0],
                                 ' '.join(['%s %s'%(k,v) for (k,v) in vals]))
    f_in.close()
    f_out.close()

def coarsify_oc(coarsify_func, f_in, f_out):
    freq=defaultdict(int)
    for l in f_in:
        line=l.strip().split()
        freq[coarsify_func(line[0])]+=int(line[1])
    for k,n in freq.iteritems():
        print >>f_out,'%s\t%s'%(k,n)

def coarsify_rdt(coarsify_func, f_in, f_out):
    freq=defaultdict(float)
    for l in f_in:
        if l[0]=='I':
            f_out.write(l)
            continue
        line=l.strip().split()
        freq.clear()
        for k,n in izip(line[1::2],line[2::2]):
            freq[coarsify_func(k)]+=float(n)
        vals=freq.items()
        vals.sort()
        print >>f_out, '%s\t%s'%(line[0],
                                 ' '.join(['%s %s'%(k,v) for (k,v) in vals]))
    f_in.close()
    f_out.close()

def coarsify_grammar(grammar_dir,coarsify_func,prefix='C1_'):
    coarsify_rules(coarsify_func,
                   file(os.path.join(grammar_dir,'rulesC.txt')),
                   file(os.path.join(grammar_dir,prefix+'rulesC.txt'),'w'))
    coarsify_lexicon(coarsify_func,
                     file(os.path.join(grammar_dir,'lexicon.unsorted')),
                     file(os.path.join(grammar_dir,prefix+'lexicon.unsorted'),'w'))
    coarsify_oc(coarsify_func,
                file(os.path.join(grammar_dir,'lexicon.oc')),
                file(os.path.join(grammar_dir,prefix+'lexicon.oc'),'w'))
    coarsify_oc(coarsify_func,
                file(os.path.join(grammar_dir,'lexicon.OC')),
                file(os.path.join(grammar_dir,prefix+'lexicon.OC'),'w'))
    coarsify_rdt(coarsify_func,
                file(os.path.join(grammar_dir,'lexicon.rdt')),
                file(os.path.join(grammar_dir,prefix+'lexicon.rdt'),'w'))
