import re

def mklist(direction,cats):
    return [(x,direction) for x in cats.split()+[None]]

collins_table=[
    (['ADJP'],mklist('l','NNS QP NN $ ADVP JJ VBN VBG ADJP JJR NP JJS DT FW RBR RBS SBAR RB')),
    (['ADVP'],[(['RBR','RB','RBS'],'l'),
               ('FW','l'),
               ('ADVP','l'),('TO','l'),('CD','l'),
               (['JJR','JJ'],'r'),('IN','r'),('NP','r'),
               ('JJS','r'),('NN','r'),(None,'r')]),
    (['CONJP'],[('CC','l'),(['RB','IN'],'r')]),
    (['FRAG'],[(None,'r')]),
    (['INTJ'],[(None,'l')]),
    (['LST'],[(['LS',':'],'r')]),
    (['PRN'],[(None,'l')]),
    (['PRT'],[('RP','r'),(None,'r')]),
    (['QP'],mklist('l','$ IN NNS NN JJ RB DT CD NCD QP JJR JJS')),
    (['RRC'],mklist('r','VP NP ADVP ADJP PP')),
    (['S'],mklist('l','TO IN VP S SBAR ADJP UCP NP')),
    (['SBAR'],mklist('l', 'WHNP WHPP WHADVP WHADJP IN DT S SQ SINV SBAR FRAG')),
    (['SBARQ'],mklist('l', 'SQ S SINV SBARQ FRAG')),
    (['SINV'],mklist('l', 'VBZ VBD VBP VB MD VP S SINV ADJP NP')),
    (['SQ'],mklist('l', 'VBZ VBD VBP VB MD VP SQ')),
    (['UCP'],[(None,'r')]),
    (['VP'],mklist('l', 'TO VBD VBN MD VBZ VB VBG VBP VP ADJP NN NNS NP')),
    (['WHADJP'],mklist('l','CC WRB JJ ADJP')),
    (['WHADVP'],[('CC','r'),('WRB','r'),(None,'r')]),
    (['WHNP'],mklist('l', 'WDT WP WP$ WHADJP WHPP WHNP')),
    (['WHPP'],[(['IN','TO'],'r'),('FW','r')]),
    ([None],[(None,'r')])]

semhead_table=[
    (['ADJP'],
     mklist('l', 'NNS QP NN $ ADVP JJ VBN VBG ADJP JJR NP JJS DT FW RBR RBS SBAR RB')),
    (['ADVP'],[(['RBR','RB','RBS'],'l'),
               ('FW','l'),
               ('ADVP','l'),('TO','l'),('CD','l'),
               (['JJR','JJ'],'r'),('IN','r'),('NP','r'),
               ('JJS','r'),('NN','r'),(None,'r')]),
    (['CONJP'],[('CC','l'),(['RB','IN'],'r')]),
    (['FRAG'],[(None,'r')]),
    (['INTJ'],[(None,'l')]),
    (['LST'],[(['LS',':'],'r')]),
    (['NP'],[(['NN','NNP','NNPS','NX','JJR'],'r'),(['NP','PRP'],'l'),
             (['$','ADJP','PRN','FW'],'r'),('CD','r'),
             (['JJ','JJS','RB','QP','DT','WDT','RBR','ADVP'],'r'),
             (['POS','l'])]),
    (['PRN'],[(None,'l')]),
    (['PRT'],[('RP','r'),(None,'r')]),
    (['QP'],mklist('l', '$ IN NNS NN JJ RB DT CD NCD QP JJR JJS')),
    (['RRC'],mklist('r','VP NP ADVP ADJP PP')),
    (['S'],mklist('l','TO IN VP S SBAR ADJP UCP NP')),
    (['SBAR'],mklist('l', 'S SBAR WHNP WHPP WHADVP WHADJP IN DT S SQ SINV SBAR FRAG')),
    (['SBARQ'],mklist('l', 'SQ S SINV SBARQ FRAG')),
    (['SINV'],mklist('l', 'VBZ VBD VBP VB MD VP S SINV ADJP NP')),
    (['SQ'],mklist('l', 'VBZ VBD VBP VB MD VP SQ')),
    (['UCP'],[(None,'r')]),
    (['VP'],mklist('l', 'VP VBD VBN VBZ VB VBG VBP MD ADJP NN NNS NP')),
    (['WHADJP'],[(['ADJP','JJ','JJR'],'l'),(['RB','r']),(None,'r')]),
    (['WHADVP'],[('CC','r'),('WRB','r'),(None,'r')]),
    (['WHNP'],[('NP','l'),
               (['NN','NNP','NNPS','NX','POS','JJR','WP'],'r'),
               (['$','ADJP','PRN'],'r'),('CD','r'),
               (['JJ','JJS','RB','QP'],'r'),
               (['WHNP','WHPP','WHADJP','WP$','WP','WDT'],'l')]),
    (['WHPP'],[(['IN','TO'],'r'),('FW','r')]),
    ([None],[(None,'r')])]

nominalCats=['NN','NNS','NNP','NNPS']
verbCats=['VBZ','VB','VBD','VBN','AUX','AUXG']
moreVerbs=['TO','MD']
# incomplete -- use JCC wrapper around Stanford JavaNLP
sd_label_rules=[
    # PosC, CatC, Dir, PosH, CatH, CatP, label
    (None,'NP','l',None,'VP','S','nsubj'),
    (None,'NP','r',None,None,'VP','dobj'),
    ('DT',None,'l',nominalCats,None,'NP','det'),
    (verbCats,None,'l','VBD',None,'VP','aux')]
