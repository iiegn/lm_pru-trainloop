#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import sys
import string
import re
import sfst
import pcfg_site_config
import med
import codecs
import pytree.tree as tree
import os.path
from pytree.sfst_util import *


utf8_enc=codecs.getencoder('UTF-8')
latin1_enc=codecs.getencoder('ISO-8859-15')
latin1_dec=codecs.getdecoder('ISO-8859-15')

def latin2utf(w):
    return utf8_enc(latin1_dec(w)[0])[0]

# returns true if the first letter of the word is a capital letter
def latin_iscapital(w):
    return latin1_dec(w[0])[0].isupper()

def unicode_lower(s):
    return latin1_enc(latin1_dec(s)[0].lower())[0]
def unicode_upper(s):
    return latin1_enc(latin1_dec(s)[0].upper())[0]


cdg_dir=pcfg_site_config.cdg_dir
smor_ca=sfst.load_compact(pcfg_site_config.smor_dir+'smor.ca')
smor_gen_ca=sfst.load_compact(pcfg_site_config.smor_dir+'smor_gen.ca')
names_ca=sfst.load_compact(pcfg_site_config.names_dir+'names.ca')

gend_map={'NoGend':'*',
    'Masc':'m',
    'Neut':'n',
    'Fem':'f'}
case_map={'Nom':'n', 'Acc':'a',
    'Dat':'d', 'Gen':'g'}
num_map={'Sg':'s', 'Pl':'p'}
temp_map={'Pres':'s','Past':'t'}
mood_map={'Ind':'i','Subj':'k'}

frame_map={}
info_map={}

def keep_only(frame0,what):
    if '+' in frame0:
        frame0=frame0[frame0.rindex('+')+1:]
    f=[a for a in frame0 if a in what]
    return ''.join(f)

def make_cng(m,n0):
    num=num_map[m.group(n0+2)]
    cas=case_map[m.group(n0+1)]
    gend=gend_map[m.group(n0)]
    return '%s%s%s'%(cas,num,gend)
def make_cngp(m,n0):
    num=num_map[m.group(n0+1)]
    gend=gend_map[m.group(n0+2)]
    cas=case_map[m.group(n0+3)]
    return '%s%s%s%s'%(cas,num,gend,m.group(n0))

verbframe_re=re.compile('(?:(?:semi)?regular)? *verb +([^ ]+) +([^ ]+)(?: +(.+))?')
for l in file(cdg_dir+'grammar/deutsch/Verben.txt'):
    l=l.strip()
    m=verbframe_re.match(l)
    if m:
        vform=m.group(1)
        vform=vform.replace('>','')
        vform=vform.replace('<','#')
        frame=keep_only(m.group(2),'ad')
        #print "'%s' => '%s'"%(vform,frame)
        frame_map[vform]=frame
        info=m.group(3)
        if info is None: info=''
        info_map[vform]=info

def lookup_frame(ana):
    a=verb_lemma(ana)
    #print "lemma: %s"%(a,)
    try:
        fr=frame_map[a]
        #print "%s => %s"%(a,fr)
        return fr
    except KeyError:
        #print "%s => %s"%(a,'unk')
        return 'unk'

imp_verb_re=re.compile(r"<\+V><Imp><(Sg|Pl)>")
finite_verb_re=re.compile(r"<\+V><(1|2|3)><(Sg|Pl)><(Pres|Past)><(Ind|Subj)>(?:/'s)?$")
def verb_type(a):
    assert '<+V>' in a
    if '<+V><Inf><zu>' in a:
        return 'IZU'
    elif '<+V><Inf>' in a:
        return 'INF'
    elif '<+V><PPast>' in a:
        return 'PP'
    elif '<+V><Imp>' in a:
        return 'IMP'
    elif '<+V><PPres>' in a:
        return 'ADJD'
    elif finite_verb_re.search(a):
        return 'FIN'
    assert False,a

def verb_analyses(word,pos):
    kind=pos[2:]
    a=[x for x in smor_ca.analyze_string(word)
        if '<+V>' in x and verb_type(x)==kind]
    if not a: return []
    a=disamb_bool(a, lambda x: '<V>' not in x and '<NN>' not in x)
    a=disamb_bool(a, lambda x: '<VPREF>' not in x)
    return a

# TODO: look for strange stuff and revert
#       to generation if everything else fails
def verb_lemma(ana):
    a=ana[:ana.index('<+V>')]
    a=a.replace('<VPREF>','')
    a=a.replace('<PREF>','')
    a=a.replace('<VPART>','#')
    a=a.replace('<OLDORTH>','')
    a=a.replace('<NEWORTH>','')
    a=a.replace('ier<SUFF>','ieren')
    a=a.replace('<V>','')
    a=a.replace('<NN>','')
    a=a.replace('<ADJ>','')
    a=a.replace('<SUFF>','')
    return a

def finverb_morphtag(ana):
    m=finite_verb_re.search(ana)
    assert m,ana
    return '%s%s%s%s'%(m.group(1),
        num_map[m.group(2)],
        mood_map[m.group(4)],
        temp_map[m.group(3)])

def impverb_morphtag(ana):
    m=imp_verb_re.search(ana)
    assert m,ana
    return num_map[m.group(1)]


nn_re=re.compile(r'<\+NN><(Neut|NoGend|Masc|Fem)>(?:<Old>|<Simp>)?<(Nom|Acc|Dat|Gen)><(Sg|Pl)>(<St>|<Wk>|)')
adja_morph_re=re.compile(r'<\+ADJ><(Pos|Comp|Sup)><(Neut|NoGend|Masc|Fem)>(?:<Old>|<Simp>)?<(Nom|Acc|Dat|Gen)><(Sg|Pl)>(<St>|<Wk>|)')
ord_morph_re=re.compile(r'<\+ORD><(Neut|NoGend|Masc|Fem)>(?:<Old>|<Simp>)?<(Nom|Acc|Dat|Gen)><(Sg|Pl)>(<St>|<Wk>|)')
art_re=re.compile(r'<\+(REL|ART|DEM|INDEF|POSS|WPRO)><(Pro|Subst|Def|Indef)><(Neut|NoGend|Masc|Fem)><(Nom|Acc|Dat|Gen)><(Sg|Pl)>(?:<St>|<Wk>|)')
pis_re=re.compile(r'<\+INDEF><(Pro|Subst|Attr)><(Neut|NoGend|Masc|Fem)><(Nom|Acc|Dat|Gen)><(Sg|Pl)>(?:<St>|<Wk>|)')
#pis_inv_re=re.compile(r'<\+INDEF><(Pro|Subst|Attr)><Invar>')
appr_re=re.compile(r'<\+(?:PREP|POSTP)><(Nom|Acc|Dat|Gen)>')
apprart_re=re.compile(r'<\+PREPART><(Neut|NoGend|Masc|Fem)><(Nom|Acc|Dat|Gen)><(Sg|Pl)>')
pron_re=re.compile(r'<\+PPRO><(Pers|Refl|Prfl)><(1|2|3)><(Sg|Pl)><(Neut|NoGend|Masc|Fem)><(Nom|Acc|Dat|Gen)>(?:<St>|<Wk>|)')
adj_re=re.compile('e[rmns]?$')

def normalize_nn_morph(mor):
    if mor.group(1)=='NoGend' and mor.group(3)=='Pl':
        return '<+NN><Masc><Nom><Sg><St>'
        ## Probleme:
        ##Cannot lemmatize rechts<ADJ><SUFF><+NN><NoGend><Dat><Sg><Wk>
        ##Cannot lemmatize rechts<ADJ><SUFF><+NN><Fem><Gen><Sg><Wk>
    else:
        return '<+NN><%s><Nom><Sg>%s'%(mor.group(1),
                                     mor.group(4))

def normalize_nn_morph_pl(mor):
    if mor.group(1)=='NoGend' and mor.group(3)=='Pl':
        return '<+NN><NoGend><Nom><Pl>'
    elif (mor.group(1)=='NoGend' and mor.group(2)=='Dat'
          and mor.group(4)=='<Wk>'):
        # Rechten / dpm
        return '<+NN><Masc><Nom><Sg><St>'
    else:
        return '<+NN><%s><Nom><Sg>%s'%(mor.group(1),
                                     mor.group(4))

def noun_lemma(a,word,unk_char=''):
    a0=nn_re.sub(normalize_nn_morph,a)
    words=smor_gen_ca.analyze_string(a0)
    if '<+NN><NoGend>' in a:
        a0=nn_re.sub(normalize_nn_morph_pl,a)
        words+=smor_gen_ca.analyze_string(a0)
    elif ('<+NN><Fem><Gen><Sg>' in a or
          '<+NN><Fem><Dat><Sg>' in a):
        a0=nn_re.sub('<+NN><Fem><Nom><Sg>',a)
        # Liebsten/gsf
        words+=smor_gen_ca.analyze_string(a0)
    words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
    if words:
        return words[0]
    if "�<+NN>" in a:
        words0=words
        words=smor_gen_ca.analyze_string(a0.replace('�<+NN>','�<OLDORTH><+NN>'))
        if not words:
            words=words0
        words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
        if words:
            return words[0]
    if "<V><SUFF><+NN>" in a:
        words=smor_gen_ca.analyze_string(a0.replace('<V><SUFF><+NN>',
            '<V><SUFF><OLDORTH><+NN>'))
        words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
        if words:
            return words[0]
    if (("<ADJ><SUFF>" in a0 or "<ORD><SUFF>" in a0 or "er<+NN>" in a0) and
        not ("<St>" in a0 or "<Wk>" in a0)):
        words=smor_gen_ca.analyze_string(a0+"<St>")
        words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
        if words:
            return words[0]
    #print "Cannot lemmatize %s"%(a,)
    return unk_char+word

pat_adj={'e':['ap*','asf','asn','np*','nsm','nsn'],
         'er':['gp*','dsf','gsf','nsm'],
         'em':['dsm','dsn'],
         'es':['asn','nsn'],
         'en':['np*','ap*','dp*','gp*','ds*','gsm','gsn','asm']}

def adja_lemma(a,word):
    a0=adja_morph_re.sub(lambda g:"<+ADJ><%s><Pred>"%(g.group(1),),a)
    words=smor_gen_ca.analyze_string(a0)
    words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
    if words:
        return words[0]
    else:
        return adj_re.sub('',word)

abbr_tags=set(['NN_p*','NN_sm','NN_sn','NN_sf'])
def possible_pos(word):
    tags=set()
    for a in smor_ca.analyze_string(word):
        if '<+NN>' in a:
            m=nn_re.search(a)
            if not m:
                assert ('<^ABBR><+NN>' in a),repr(a)
                tags.update(abbr_tags)
            else:
                num=num_map[m.group(3)]
                gend=gend_map[m.group(1)]
                if num=='p':
                    gend='*'
                tags.add('NN_%s%s'%(num,gend))
        elif '<+V>' in a:
            frame=lookup_frame(a)
            kind=verb_type(a)
            if kind=='ADJD':
                tags.add('ADJD')
            else:
                tags.add('VV%s_%s'%(kind,frame))
        elif '<+ADJ>' in a or '<+ORD>' in a:
            if '<Adv>' in a or '<Pred>' in a:
                tags.add('ADJD')
            else:
                m=adj_re.search(word)
                if m:
                    tags.add('ADJA_%s'%(m.group(0),))
                else:
                    tags.add('ADJA_*')
        elif '<+NPROP>' in a:
            tags.add('NE')
        elif '<+PREP>' in a:
            m=appr_re.search(a)
            if m:
                case=case_map[m.group(1)]
                tags.add('APPR_%s'%(case,))
            else:
                assert ('<^ABBR>' in a),a
                for case in case_map.values():
                    tags.add('APPR_%s'%(case,))
        elif '<+POSTP>' in a:
            m=appr_re.search(a)
            assert m,a
            case=case_map[m.group(1)]
            tags.add('APPO_%s'%(case,))
        elif '<+VPRE>' in a:
            tags.add('PTKVZ')
        elif '<+ADV>' in a:
            tags.add('ADV')
        elif '<+INTJ>' in a:
            tags.add('ITJ')
        elif '<+CARD>' in a:
            tags.add('CARD')
        elif '<+TRUNC>' in a:
            tags.add('TRUNC')
        elif '<+PROADV>' in a:
            tags.add('PROP')
        elif '<+INDEF>' in a:
            # <+INDEF><Attr><Invar> --> PIDAT_***
            # <+INDEF><Pro>... --> PIS_{a,n,g,d}
            if '<+INDEF><Attr><Invar>' in a:
                tags.add('PIDAT_***')
            elif '<+INDEF><Pro><Invar>' in a:
                tags.add('PIDAT_***')
                tags.add('PIS_*')
            elif '<+INDEF><Subst><Invar>' in a:
                tags.add('PIS_*')
            else:
                m=pis_re.search(a)
                assert m,a
                case=case_map[m.group(3)]
                num=num_map[m.group(4)]
                gend=gend_map[m.group(2)]
                if num=='p':
                    gend='*'
                if m.group(1) in ('Pro','Attr'):
                    tags.add('PIDAT_%s%s%s'%(case,num,gend))
                if m.group(1) in ('Pro','Subst'):
                    tags.add('PIS_%s'%(case,))
        elif '<+SYMBOL>' in a:
            tags.add('XY')
        else:
            # TODO:
            # <+ORD> --> ADJA_unk
            tags.add('???:%s'%(a,))
        # TODO: teilweise Disambiguierung von
        # Kino<NN>karten<V><SUFF><+NN> vs. Kino<NN>Karte<+NN>
    if word[0]==word[0].upper():
        for a in names_ca.analyze_string(word):
            if '<Unk>' not in a:
                tags.add('NE')
    return tags

apprart_irregular={'am':'an','im':'in','vom':'von'}
def apprart_map(lemma):
    if lemma in apprart_irregular:
        return apprart_irregular[lemma]
    elif lemma[-1] in ['m','r','s']:
        return lemma[:-1]
    else:
        return lemma

year_re=re.compile('([0-9][0-9])[0-9][0-9]$')
number_trans=string.maketrans('0123456789','0199999999')
number2_trans=string.maketrans('0123456789','0999999999')
def normalize_card(word):
    m=year_re.match(word)
    if m:
        return m.group(1)+'99'
    else:
        return word[0].translate(number_trans)+word[1:].translate(number2_trans)

def normalize_nn(word):
    if '-' in word:
        end=len(word)-1
        while word[end]=='-' and end>0:
            end-=1
        try:
            word=word[word.rindex('-',0,end)+1:]
        except ValueError:
            word=word[:end]
        if word=='':
            word='-'
        elif word[0]==')':
            word=word[1:]
    return normalize_card(word)
    
art_pos={'ART':'<+ART>','PIDAT':'<+INDEF><Pro>'}
infverb_pos={'IZU':'<+V><Inf><zu>','PP':'<+V><PPast>'}
nomorph_pos='''$. $( $, KON KOUS ADV PTKNEG PWAV CARD PTKA PTKZU
    PTKVZ KOUI TRUNC KOKOM PTKANT DM ITJ
    PROP ADJD XY'''.split()
lemmaonly_pos=['V'+x+y for x in 'VMA' for y in ['INF','IZU','PP']]
for kind1,part1 in [('D','+DEM'),('REL','+REL'),
                   ('I','+INDEF'),('POS','+POSS'),
                    ('W','+WPRO')]:
    for kind2,part2 in [('S','Subst'),('AT','Pro')]:
        art_pos['P'+kind1+kind2]='<%s><%s>'%(part1,part2)

def get_lemma(word,pos,morph=None):
    if '_' in pos:
        pos=pos[:pos.index('_')]
    if pos=='NN':
        if '-' in word:
            end=len(word)-1
            while word[end]=='-' and end>0:
                end-=1
            try:
                word=word[word.rindex('-',0,end)+1:]
            except ValueError:
                word=word[:end]
            if word=='':
                word='-'
            elif word[0]==')':
                word=word[1:]
        analyses=[a for a in smor_ca.analyze_string(word)
                  if '<+NN>' in a]
        if analyses:
            analyses=disamb_bool(analyses,lambda x: '<V><SUFF>' not in x)
            lemmas=[noun_lemma(a,word) for a in analyses]
            lemmas.sort()
            return normalize_card(lemmas[0])
        else:
            return word
    elif pos[0]=='V':
        analyses=verb_analyses(word.lower(),pos)
        if analyses:
            lemmas=[verb_lemma(a) for a in analyses]
            lemmas.sort()
            return lemmas[0]
        else:
            if pos=='VVPP' and word[-1]=='t':
                return word[:-1]+'en'
            else:
                return word
    elif pos=='ADJA':
        return adj_re.sub('',word)
    elif pos=='APPRART':
        return apprart_map(word)
    elif pos=='CARD':
        return normalize_card(word)
    else:
        if pos in ['NE']:
            return word
        else:
            return word.lower()

class AVZLemmatizer(object):
    def __init__(self,real_lemmatizer):
        self.lemmatizer=real_lemmatizer
    def set_pos(self,postags,words):
        avzs={}
        last_vvfin=None
        for i,tag in enumerate(postags):
            if tag.startswith('VVFIN'):
                last_vvfin=i
            if tag=='PTKVZ' and last_vvfin is not None:
                avzs[last_vvfin]=words[i]
                last_vvfin=None
        self.avzs=avzs
    def __call__(self,word,cat,i=None):
        if i in self.avzs and cat.startswith('VVFIN'):
            return "%s#%s"%(self.avzs[i],self.lemmatizer(word,cat))
        else:
            return self.lemmatizer(word,cat)
get_lemma_2=AVZLemmatizer(get_lemma)

quote_re=re.compile("(?:'+|`+|\")$")
def guess_tag(word):
    if quote_re.match(word):
        return "$("
    elif word in "()[]{}" or word in ['-LRB-','-RRB-']:
        return "$("
    elif word==",":
        return "$,"
    elif word in ".!?":
        return "$."
    all_pos=[]
    for cat in possible_pos(word):
        if cat.startswith('???'):
            continue
        if '_' in cat:
            cat=cat[:cat.index('_')]
        all_pos.append(cat)
    if all_pos:
        all_pos.sort()
        return all_pos[0]
    else:
        return 'FM'

def tag_words(words):
    return [tree.TerminalNode(guess_tag(word),word) for word in words]

apprart_irregular={'am':'an','im':'in','vom':'von'}
def apprart_map(lemma):
    if lemma in apprart_irregular:
        return apprart_irregular[lemma]
    elif lemma[-1] in ['m','r','s']:
        return lemma[:-1]
    else:
        return lemma
unmarked_plural={'Mark':'f','Grad':'n','Prozent':'n',
                 'Dollar':'m'}

known_nes={}
known_nns={}
first_names={}
for l in file(os.path.join(pcfg_site_config.pynlp_dir,
                           'data','tueba_ne.txt')):
    line=l.strip().split('\t')
    if line[0]=='NON-AMB':
        known_nes[tuple(line[1].split(' '))]= \
            (line[2].split(' '),line[3].split(' '))
    elif line[0]=='UNK-NN' and line[1][0] not in '123456789':
        known_nns[line[1]]=line[2]
    elif line[0]=='FNAME':
        first_names[line[1]]=line[2]

other_morph={("'s",'PPER'):['%ssn3'%(c,) for c in 'na'],
    ("-s",'PPER'):['%ssn3'%(c,) for c in 'na'],
    ("einander",'PRF'):['ap*3','dp*3'],
    ("disch",'PPER'):['as*3'],
    ("ick",'PPER'):['ns*3'],
    ("se",'PPER'):['%s%s'%(c,n) for c in 'na'
        for n in ['sf3','p*3']]
    }

irregular_vvpp_map={'fund':'find',
                    'schied':'scheid',
                    'lor':'lier',
                    'schloss':'schlie�',
                    'schrieb':'schreib',
                    'blieb':'bleib',
                    'tan':'tun',
                    'nomm':'nehm',
                    'gang':'geh'}

common_ne_suffixes={'str.':'sf','stra�e':'sf','weg':'sm'}
common_nn_suffixes={'str.':'sf','stra�e':'sf','weg':'sm',
    'burg':'sf','stadt':'sf'}


deriv_pat=re.compile('(?:<NN>in<SUFF>|<[A-Za-z]+>)+')
def select_simplest(morphs):
    anas={}
    ana_cplx={}
    for morph,lemma,a in morphs:
        cplx2=len(deriv_pat.findall(a))
        if morph not in ana_cplx:
            anas[morph]=[(lemma,a)]
            ana_cplx[morph]=cplx2
        else:
            cplx1=ana_cplx[morph]
            if cplx1>cplx2:
                ana_cplx[morph]=cplx2
                anas[morph]=[(lemma,a)]
            elif cplx1==cplx2:
                anas[morph].append((lemma,a))
    result=[]
    for k in sorted(anas.iterkeys()):
        result.extend([(k,lm,a) for (lm,a) in anas[k]])
    return result

def get_morphs(word,pos,unk_mark=''):
    if pos in nomorph_pos:
        return []
    morphs=[]
    w=word
    #TODO: use lowercasing that respects umlauts
    if unk_mark=='':
        qmark=''
    else:
        qmark='?'
    if pos not in ['NN','NE','ADJA']:
        w=unicode_lower(w)
    anas=smor_ca.analyze_string(w)
    if pos=='ADJA':
        w_lower=unicode_lower(w)
        if w_lower!=w:
            anas.extend(smor_ca.analyze_string(w_lower))
    if pos.startswith('V'):
        if pos.endswith('FIN'):
            for a in anas:
                if finite_verb_re.search(a):
                    morphs.append((finverb_morphtag(a),
                                   verb_lemma(a),a))
            if morphs:
                morphs=select_simplest(morphs)
            else:
                # try again with some surface form twiddling
                suffix=''
                w=w.replace('kk','ck')
                if w[-1]=="'":
                    if w=="is'":
                        w='ist'
                    else:
                        w=w[:-1]+'e'
                elif w.endswith('hn'):
                    w=w[:-2]+'hen'
                elif w.endswith('ste'):
                    w=w[:-3]+'st'
                    suffix='_Du'
                elif w.endswith('nse'):
                    w=w[:-2]
                    suffix='_Sie'
                    if w[-1]=='n' and w[-2] in 'h':
                        w=w[:-1]+'en'
                elif w[-1]=='s' and w[-2] in 'dtb':
                    w=w[:-1]
                    suffix='_es'
                anas=smor_ca.analyze_string(w)
                for a in anas:
                    if finite_verb_re.search(a):
                        morphs.append((finverb_morphtag(a),
                                       verb_lemma(a)+suffix,a))
                if morphs:
                    morphs=select_simplest(morphs)
                else:
                    # -te => (e)n    [123]sit
                    # -ten => (e)n   [123]pit
                    # -st => (e)n    2sis
                    # -t => (e)n     3sis, 2pis
                    # -n => (e)n     [13]pis
                    tags=None
                    if w.endswith('te'):
                        stem=w[:-2]
                        tags=['1sit','2sit','3sit']
                    elif w.endswith('ten'):
                        stem=w[:-3]
                        tags=['1pit','2pit','3pit']
                    elif w.endswith('st'):
                        stem=w[:-2]
                        tags=['2sis']
                    elif w.endswith('t'):
                        stem=w[:-1]
                        tags=['3sis','2pis']
                    elif w.endswith('n'):
                        stem=w[:-1]
                        tags=['1pis','3pis']
                    if tags is not None:
                        if len(stem)>=3 and stem[-1] in 'lr' and stem[-2]=='e':
                            inf=stem+'n'
                        else:
                            inf=stem+'en'
                        for k in tags:
                            morphs.append((k,inf+unk_mark,'guess:'+stem))
        elif pos[2:] in infverb_pos:
            wanted=infverb_pos[pos[2:]]
            for a in anas:
                if wanted in a:
                    morphs.append(('--',verb_lemma(a),a))
            if pos=='VVPP' and not morphs:
                stem=None
                prefix=None
                maybe_lemma=None
                if w.endswith('iert'):
                    stem=w[:-1]
                    prefix=None
                    maybe_lemma=stem+'en'
                elif w.endswith('t'):
                    if w.startswith('be') or w.startswith('ver'):
                        stem=w[:-1]
                        prefix=None
                    elif 'ge' in w:
                        posn=w.index('ge')
                        if w[posn:]!='geben':
                            prefix=w[:posn]
                            if posn>0:
                                prefix+='#'
                            stem=w[posn+2:-1]
                        else:
                            prefix=''
                            stem=w[:-1]
                    else:
                        prefix=None
                        stem=w[:-1]
                    if prefix is None:
                        prefix=''
                    if stem[-2:]=='er' or stem[-2:]=='el' or stem[-1]=='e':
                        suffix='n'
                    else:
                        suffix='en'
                    maybe_lemma=prefix+stem+suffix
                elif w.endswith('en'):
                    if w.startswith('be'):
                        stem=w[2:-2]
                        prefix='be'
                    elif w.startswith('ent') or w.startswith('ver'):
                        stem=w[3:-2]
                        prefix=w[:3]
                    elif 'ge' in w:
                        posn=w.index('ge')
                        prefix=w[:posn]
                        if posn>0:
                            prefix+='#'
                        stem=w[posn+2:-1]
                    elif w.startswith('um'):
                        stem=w[2:-2]
                        prefix='be'
                    else:
                        prefix=''
                        stem=w[:-2]
                    stem=irregular_vvpp_map.get(stem,stem)
                    maybe_lemma=prefix+stem+'en'
                if maybe_lemma is not None:
                    morphs.append(('--',maybe_lemma+unk_mark,'guess:'+maybe_lemma))
        elif pos.endswith('INF'):
            wanted='<+V><Inf>'
            for a in anas:
                if wanted in a:
                    if '<VPART>' in a:
                        morphs.append(('--',verb_lemma(a),a))
                    else:
                        morphs.append(('--',w,'--'))
            if not morphs:
                  morphs.append(('--',w,'--'))
        elif pos.endswith('IMP'):
            for a in anas:
                if imp_verb_re.search(a):
                    morphs.append((impverb_morphtag(a),verb_lemma(a),a))
            if not morphs:
                # try again with some surface form twiddling
                if w.endswith("'"):
                    w=w[:-1]
                if w.endswith('et') and w[-3] not in 'dt':
                    w=w[:-2]+'t'
                elif w.endswith('e') and w[-2] not in 'dt':
                    w=w[:-1]
                elif w[-1] in 'dt' and w[-2] in 'rl' and w[-3] in 'aeiou':
                    w=w+'e'
                anas=smor_ca.analyze_string(w)
                for a in anas:
                    if imp_verb_re.search(a):
                        morphs.append((impverb_morphtag(a),verb_lemma(a),a))
    elif pos=='NN':
        for a in anas:
            if '<+NN>' in a:
                m=nn_re.search(a)
                if m:
                    # TBD: Rechten/ds* => Rechte|Rechter|Rechtes
                    morphs.append((make_cng(m,1),
                                   noun_lemma(a,w,qmark),a))
                elif '<^ABBR><+NN>' in a:
                    morphs.append(('*s*',w,a))
                    morphs.append(('*p*',w,a))
        if w in unmarked_plural:
            gend=unmarked_plural[w]
            morphs.extend([('%sp%s'%(cas,gend),w,'(unm_pl)') for cas in 'nadg'])
        if morphs:
            morphs=select_simplest(morphs)
            # correct SMOR tags: xxxIn/xxxInnen is s*/p*, not sf/pf
            if w.endswith('Innen'):
                for i in xrange(len(morphs)):
                    if morphs[i][0].endswith('pf'):
                        m,l,a=morphs[i]
                        morphs[i]=(m[0]+'p*',l,a)
            elif w.endswith('In'):
                for i in xrange(len(morphs)):
                    if morphs[i][0].endswith('sf'):
                        m,l,a=morphs[i]
                        morphs[i]=(m[0]+'s*',l,a)
        else:
            if w in known_nns:
                for cas in 'nadg':
                    morphs.append((cas+known_nns[w],w,'(known_nns)'))
                #print "known_nns: %s"%(morphs,)
            elif w.endswith('s') and w[:-1] in known_nns:
                m=known_nns[w[:-1]][1]
                for cas in 'nadg':
                    morphs.append(('%sp%s'%(cas,m),w[:-1],'(known_nns_pl)'))
                #print "known_nns_gen: %s"%(morphs,)
                morphs.append(('gs'+m,w[:-1],'(known_nns_gen)'))
            else:
                for suffix in common_nn_suffixes:
                    if w.endswith(suffix):
                        for cas in 'nadg':
                            morphs.append((cas+common_nn_suffixes[suffix],
                                w,'(common_suffixes)'))
    elif pos=='NE':
        if word[-1]=='s':
            if word[-2:]=="'s":
                morphs.append(('***',w[:-2],'(gen-S)'))
            else:
                morphs.append(('gs*',w[:-1],'(gen-S)'))
                morphs.append(('*s*',w,'(non-gen-S'))
                morphs.append(('*p*',w[:-1],'(plur-S'))
        else:
            morphs.append(('***',w,'(NE-default)'))
    elif pos in ['APPR', 'APPO']:
        for a in anas:
            if '<+PREP>' in a or '<+POSTP>' in a:
                m=appr_re.search(a)
                if m:
                    morphs.append((case_map[m.group(1)],w,a))
        if not morphs:
            # guess case for unknown prepositions
            for cas in 'ad':
                morphs.append((cas,w,'(appr_guess)'))
    elif pos=='APPRART':
        for a in anas:
            m=apprart_re.search(a)
            if m:
                morphs.append((make_cng(m,1),
                    apprart_map(w),a))
    elif pos in art_pos:
        wanted=art_pos[pos]
        for a in anas:
            if wanted not in a:
                continue
            m=art_re.search(a)
            if m:
                morphs.append((make_cng(m,3),'--',a))
            elif '<Invar>' in a:
                morphs.append(('***','--',a))
        if pos.endswith('S') and not morphs:
            wanted=art_pos[pos[:-1]+'AT']
            for a in anas:
                if wanted not in a:
                    continue
                m=art_re.search(a)
                if m:
                    morphs.append((make_cng(m,3),'--',a))
    elif pos=='ADJA':
        for a in anas:
            m=adja_morph_re.search(a)
            if m:
                morphs.append((make_cng(m,2),
                    adja_lemma(a,w),a))
            elif '<+ADJ><Invar>' in a:
                morphs.append(('***',w,a))
            else:
                m=ord_morph_re.search(a)
                if m:
                    morphs.append((make_cng(m,1),
                        a[:m.start()],a))
                elif '<+ORD>' in a:
                    morphs.append(('*s*',w,a))
                    morphs.append(('*p*',w,a))
        if morphs:
            morphs=select_simplest(morphs)
        else:
            if latin_iscapital(w) and w.endswith('er'):
                morphs=[('***',w,'(invar_adj)')]
            else:
                morphs=[]
            w_lower=unicode_lower(w)
            m=adj_re.search(w_lower)
            if m and not morphs:
                morphs.extend([(mor,w_lower[:m.start()]+unk_mark,'(pat_adj)')
                               for mor in pat_adj[m.group(0)]])
    elif pos in ['PRF','PPER']:
        if pos=='PRF':
            default_lemma='%refl'
        else:
            default_lemma=w
        for a in anas:
            m=pron_re.search(a)
            if m:
                morphs.append((make_cngp(m,2),default_lemma,a))
        if not morphs:
            if (w,pos) in other_morph:
                morphs.extend([(m,default_lemma,'other_morph') for m in other_morph[(w,pos)]])
            else:
                print >>sys.stderr, "No morph info for %s_%s"%(w,pos)
                morphs.append(('****',default_lemma,'<NoInfo>'))
    if not morphs:
        if pos in ['NN','ADJA'] or pos in art_pos:
            morphs.append(('***',word+unk_mark,'--'))
        else:
            morphs.append(('--',word+unk_mark,'--'))
    return morphs
