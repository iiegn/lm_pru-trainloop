import re

nominalCats=frozenset(['NP','NP-REL','PN'])
nominalPOS=frozenset(['NN','NE','FM'])
vx_pos_cats1="VAM"
vx_pos_cats2={'INF':'I','IZU':'Z','FIN':'F','PP':'P',
                'IMP':'M'}
VxInf_POS=frozenset(['V'+a+'INF' for a in vx_pos_cats1])
VxIzu_POS=frozenset(['V'+a+'IZU' for a in vx_pos_cats1])
fullverb_pos=frozenset(['VV'+a for a in vx_pos_cats2])
auxverb_pos=frozenset(['VA'+a for a in vx_pos_cats2])
modalverb_pos=frozenset(['VM'+a for a in vx_pos_cats2])
hr_table=[
    (['PP','PP-REL'],[(['APPR','APPRART','PROAV'],'l'),('APPO','r'),
            (None,'CJ','l'),(['@PP','@PP-REL'],'M-REST','l')]),
    (nominalCats.union(['@NP','@PN']),
     [(None,'M-HD','r'),
      (None,'NK','r'),(None,'PNC','l'),
      (nominalCats.union(nominalPOS).union(set(['CH','FM'])),'l'),
      ('NumP','l'),('AP','l')]),
    ('FX',[(None,'HD','r'),(None,'l')]),
    (['AP','AVP','DP','CAP','CAVP'],
     [(None,['HD','M-HD'],'l'),
      (None,'CJ','l'),(None,'M-REST','l'),
      ('AP','l')]),
    (['@CNP','@CAP','@CAVP','@CPP','@CVP','@CS',
      'CNP','CAP','CAVP','CPP','CVP','CS'],
     [(None,'CJ','l'),(None,'M-REST','l')]),
    (['VP','S'],[(None,['HD','M-HD'],'r'),
                 (fullverb_pos,'r')]),
    (['@VP','@S'],[(None,['HD','M-HD'],'r'),
                 (fullverb_pos,'r')]),
    (['CVP','CS'],[(None,'CJ','l'),(None,'M-REST','l')])]

hr_semhead=[
    (['PP','PP-REL'],[(['APPR','APPRART','PROAV'],'l'),('APPO','r'),
            (None,'CJ','l'),(['@PP','@PP-REL'],'M-REST','l')]),
    (nominalCats,[(None,'M-HD','r'),
                  (None,'NK','r'),(None,'PNC','l'),
                  (nominalCats.union(nominalPOS).union(set(['CH','FM'])),'l'),
                    ('NumP','l'),('AP','l')]),
    ('FX',[(None,'HD','r'),(None,'l')]),
    (['AP','AVP','DP','CAP','CAVP'],
     [(None,['HD','M-HD'],'l'),
      (None,'CJ','l'),(None,'M-REST','l'),
      ('AP','l')]),
    (['CNP','CAP','CAVP','CPP','CVP','CS'],
     [(None,'CJ','l'),(None,'M-REST','l')]),
    (['@CNP','@CAP','@CAVP','@CPP','@CVP','@CS'],
     [(None,'CJ','l'),(None,'M-REST','l')]),
    (['@VP','@S'],[(fullverb_pos.union(['VZ']),'HD','r'),
                 ('VP','OC','r'),
                 (None,['HD','M-HD'],'r'),
                 (fullverb_pos,'r')]),
    (['VP','S'],[(fullverb_pos.union(['VZ']),'HD','r'),
                 ('VP','OC','r'),
                 (None,['HD','M-HD'],'r'),
                 (fullverb_pos,'r')])]

