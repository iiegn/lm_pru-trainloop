import re

nominalCats=frozenset(['NX','NX-REL','NX-MOD','NX-W','NCX','EN-ADD'])
nominalPOS=frozenset(['NN','NE','FM'])
vx_pos_cats1="VAM"
vx_pos_cats2={'INF':'I','IZU':'Z','FIN':'F','PP':'P',
                'IMP':'M'}
VxInf_POS=frozenset(['V'+a+'INF' for a in vx_pos_cats1])
VxIzu_POS=frozenset(['V'+a+'IZU' for a in vx_pos_cats1])
fullverb_pos=frozenset(['VV'+a for a in vx_pos_cats2])
auxverb_pos=frozenset(['VA'+a for a in vx_pos_cats2])
modalverb_pos=frozenset(['VM'+a for a in vx_pos_cats2])
hr_table=[
    (['PX','PX-REL'],[(['APPR','APPRART'],'l'),('APPO','r'),
            (None,'APP','l'),(None,'KONJ','l'),(None,'M-REST','l'),
            (['PROP','PX','PWAV'],'HD','l')]),
    (nominalCats,[(None,'HD','r'),(None,'APP','l'),
                    (['NX','NE'],'l'),
                    (nominalCats.union(nominalPOS).union(set(['FX','FM'])),'l'),
                    ('NumP','l'),('ADJX','l'),(None,'l')]),
    ('FX',[(None,'HD','r'),(None,'l')]),
    (['ADJX','ADVX','DP'],[(None,'HD','l'),(None,'APP','l'),
                           (None,'KONJ','l'),(None,'M-REST','l'),
                           ('ADJX','l')]),
    (['@NX','@ADJX','@ADVX','@PX'],
     [(None,'KONJ','l'),(None,'M-REST','l')]),
    (['VXVP','VXVF','VXVI'],[(fullverb_pos.union(set(['VXVP','VXVI'])),'r')]),
    (['VXMP','VXMF','VXMI'],[(modalverb_pos.union(set(['VXMP','VXMI'])),'r')]),
    (['VXAP','VXAF','VXAI'],[(auxverb_pos.union(set(['VXAP','VXAI'])),'r')]),
    (['SIMPX','R-SIMPX'],[(['VXVP','VXVF','VXVI'],'l'),
                          (['VXMP','VXMF','VXMI','VXAP','VXAF','VXAP'],'r')])]

open_cat=['ADJA','NN','NE','VVPP']
