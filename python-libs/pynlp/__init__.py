import re
import os.path
import pcfg_site_config
import sys

def get_defaults(mods):
    """returns a hash with filenames or objects
    for the following keys:
    * grammar: default grammar
    * coarsify: coarsify function for head finding
    * heads: head finder
    * regex_in: default regex.in file
    * possible_pos: POS predictor
    * lemmatize: lemmatizer
    """
    result={}
    for key in mods:
        if key=='de':
            if os.path.exists(os.path.join(pcfg_site_config.smor_dir,
                                           'smor.ca')):
                import pynlp.de.smor_pos as smor_pos
                result['possible_pos']=smor_pos.possible_pos
                result['lemmatize']=smor_pos.get_lemma_2
                result['tag_words']=smor_pos.tag_words
            else:
                # TBD: fallback to Morphy
                raise ValueError, 'SMOR not found'
            result['clusters']=os.path.join(pcfg_site_config.pynlp_dir,
                                            'data/de_clusters_250.txt')
        elif key=='tueba':
            import pynlp.de.tueba_heads as heads
            result['heads']=heads.hr_table
            result['coarsify']=default_coarsify
        elif key=='tiger':
            import pynlp.de.tiger_heads as heads
            result['heads']=heads.hr_table
            result['coarsify']=default_coarsify
        elif key=='althead':
            import pynlp.de.tiger_heads as heads
            result['heads']=heads.hr_semhead
        elif key=='it':
            import pynlp.it.morphit_pos as morphit_pos
            import pynlp.it.lmtut_heads as heads
            result['possible_pos']=morphit_pos.possible_pos
            result['lemmatize']=morphit_pos.get_lemma
            result['heads']=heads.hr_table
            result['coarsify']=heads.coarsify_func
            result['clusters']=os.path.join(pcfg_site_config.pynlp_dir,
                                            'data/it_clusters_250.txt')
        else:
            print >>sys.stderr,"Unknown PyNLP option: %s"%(key,)
    return result

def load_clusters(fname):
    clustering={}
    for l in file(fname):
        line=l.strip().split()
        clustering[line[0]]=int(line[1])
    return clustering

def null_lemmatizer(word,pos):
    return word.lower()

markov_re=re.compile('<.*')
deco_re=re.compile('_.*')
def default_coarsify(sym):
    add_at=False
    if markov_re.search(sym):
        add_at=True
        sym=markov_re.sub('',sym)
    sym=deco_re.sub('',sym)
    if add_at:
        return '@'+sym
    else:
        return sym
