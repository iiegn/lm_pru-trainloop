#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import sys
import pytree.tree as tree
import morphit_pos

##
## transformations on the constituent level
##

## TODO:
## prune unary DP, PUNCT-P
## remodel conjunctions
## merge CONTIN+DENOM into single NP
## remodel P-N-P and P-N type locutions
## mark number in ARTP, ADJP
## distinguish pre-/postmodifying ADJP
## markovize NP, S
## DONE:
## mark relative pronouns (che)
## make verb chunks (VX..)


def correct_const(t):
    remodel_conjunctions(t.roots,None)
    remodel_names(t.roots,None)
    for n in t.roots:
        make_verb_chunks(n)
        rename_artp(n)
        rename_conjp(n)
    kill_unaries(t.roots,None)
    kill_emptynodes(t.roots)
    mark_phrases(t)

def mark_rel(node):
    if (len(node.children)>1 and
        node.children[0].cat in ['CCHE','CCHI','QNS','QNP'] and
        node.children[1].edge_label=='RELCL'):
        print >>sys.stderr,"Free relative: %s"%(node.to_penn(),)
        chi=node.children[0]
        relcl1=node.children[1]
        relcl1.children[0:0]=[chi]
        chi.parent=relcl1
        del node.children[0]
        if '-REL' not in relcl1.cat:
            relcl1.cat+='-REL'
        else:
            print >>sys.stderr,"Confusing relative clause: %s"%(relcl1.to_penn(),)
        return
    if node.cat=='NP':
        node.cat='NP-REL'
    n_old=node
    n=node.parent
    while n:
        if (len(n.children)>1 and
            n.children[0] is n_old and
            n.children[1].edge_label=='RELCL'):
            print >>sys.stderr,"Free relative: %s"%(n.to_penn,)
            relcl1=n.children[1]
            relcl1.children[0:0]=[node]
            if '-REL' not in relcl1.cat:
                relcl1.cat+='-REL'
            else:
                print >>sys.stderr,"Confusing relative clause: %s"%(relcl1.to_penn(),)
            node.parent=relcl1
            del n.children[0]
            break
        elif n.cat in ['S','S-REL']:
            n.cat='S-REL'
            break
        elif n.cat=='PX':
            n.cat='PX-REL'
        elif n.cat in ['NP','NP-REL']:
            n.cat='NP-REL'
        elif n.cat=='VP':
            print >>sys.stderr,"chi/che in %s"%(n.to_penn())
        else:
            print >>sys.stderr,"chi/che in %s"%(n.to_penn())
            break
            #assert False,n.to_penn()
        n_old=n
        n=n.parent

def mark_phrases(t):
    """Transformations that are useful for parsing
       rather than strictly necessary"""
    for n in t.terminals:
        if (n.word in ['che','chi'] or
            '-Q' in n.cat or
            n.cat[:2]=='QN') and (hasattr(n.parent,'cat') and n.parent.cat=='NP'):
            mark_rel(n.parent)
    unlink_end(t.roots[-1],t.roots)
    # Im Prinzip müsste man den zweiten Teil von mark_phrases (classify_{np,adjp,vp}
    # und markovize_{np,conjunctions}) für die Zwecke der LM-Pipeline weglassen
    # können, weil die zusätzliche Information fürs PCFG-Parsing gedacht war und
    # hinterher wieder weggeworfen wird.
    #classify_verbs(t)
    #classify_adjp(t)
    #classify_np(t)
    #for n in t.roots:
    #    markovize_np(n)
    #for n in t.roots:
    #    markovize_conjunctions(n)

def unlink_end(node,roots):
    if node.edge_label=='END':
        del node.parent.children[-1]
        node.parent=None
        roots.append(node)
    else:
        if node.children:
            unlink_end(node.children[-1],roots)

def markovize_conjunctions(node):
    is_conjunction=False
    for n in node.children:
        if n.edge_label=='KONJ':
            is_conjunction=True
            break
    if is_conjunction:
        nodes=node.children
        for n1 in nodes:
            markovize_conjunctions(n1)
        n=node
        while len(nodes)>2:
            cat='%s<%s'%(node.cat,nodes[1].cat)
            n1=tree.NontermNode(cat,'M-REST')
            chld1=nodes[0]
            chld1.parent=n
            n1.parent=n
            del nodes[0]
            n.children=[chld1,n1]
            n=n1
        for n1 in nodes:
            n1.parent=n
        n.children=nodes
    else:
        for n1 in node.children:
            markovize_conjunctions(n1)

coord_labels=['COORD+BASE', 'COORD+RANGE','COORD+ADVERS','COORD']
coord_labels2=['COORD2ND+BASE', 'COORD2ND+RANGE','COORD2ND+ADVERS','COORD2ND']
def remodel_conjunctions(nodes,parent_node):
    pos_coord=None
    for (i,n) in enumerate(nodes):
        if n.edge_label in coord_labels:
            pos_coord=i
    if pos_coord!=None:
        n1=tree.NontermNode(parent_node.cat,'KONJ')
        n1.parent=parent_node
        n1.children=nodes[:pos_coord]
        for nn in n1.children:
            nn.parent=n1
        ns=[n1]
        find_conjuncts(nodes[pos_coord],ns)
        for nn in ns:
            if nn.edge_label in coord_labels:
                nn.edge_label='-'
            elif nn.edge_label in coord_labels2:
                nn.edge_label='KONJ'
            nn.parent=parent_node
        nodes[:pos_coord+1]=ns
    for n in nodes:
        remodel_conjunctions(n.children,n)

def find_conjuncts(node,ns):
    pos_2nd=None
    for (i,n) in enumerate(node.children):
        if (n.edge_label in coord_labels or
            n.edge_label in coord_labels2):
            pos_2nd=i
            break
    if pos_2nd==None:
        ns.append(node)
    else:
        old_chlds=node.children
        if pos_2nd>0:
            node.children=old_chlds[:pos_2nd]
            ns.append(node)
        find_conjuncts(old_chlds[pos_2nd],ns)
        ns.extend(old_chlds[pos_2nd+1:])
        
def remodel_names(nodes,parent=None):
    pos_contin=None
    for (i,n) in enumerate(nodes):
        if n.edge_label in ['CONTIN+DENOM']:
            pos_contin=i
    if pos_contin!=None:
        ns=[]
        gather_names(nodes[pos_contin],ns)
        for nn in ns:
            nn.parent=parent
        nodes[pos_contin:pos_contin+1]=ns
    for n in nodes:
        remodel_names(n.children,n)

def make_verb_chunks(node):
    if node.cat=='VP':
        first_v=None
        last_v=None
        for i,n in enumerate(node.children):
            if n.edge_label in ['HD','AUX+TENSE',
                                'AUX+PASSIVE','AUX+PROGRESSIVE']:
                if first_v is None:
                    first_v=i
                last_v=i
        if first_v is not None:
            ns=[]
            for n in node.children[first_v:last_v+1]:
                if n.cat=='VP':
                    collect_verbs(n,ns)
                else:
                    ns.append(n)
            if not ns:
                # got a verb chunk full of traces - nothing to see
                node.children[first_v:last_v+1]=[]
                # make a guess whether it's an S or a VP
                if node.parent.cat=='S':
                    node.cat='S'
            else:
                while ns[0].cat[0]!='V':
                    # put adverbs that should not be here outside
                    print >>sys.stderr, "stray adverb in VX: %s"%(ns[0].to_penn())
                    ns[0].parent=node
                    node.children[first_v:first_v]=[ns[0]]
                    del ns[0]
                    first_v+=1
                    last_v+=1
                #VF+E => VF
                nodecat='VX'+ns[0].cat[1]
                if nodecat.endswith('Y'): nodecat=nodecat[:-1]
                # ignore number info on VX
                if nodecat.endswith('P'): nodecat='VXP'
                assert ns[-1].cat[0]=='V'
                if ns[-1].cat[-1]=='Y':
                    nodecat=nodecat+'Y'
                newnode=tree.NontermNode(nodecat,'HD')
                newnode.parent=node
                for n in ns:
                    n.parent=newnode
                newnode.children=ns
                node.children[first_v:last_v+1]=[newnode]
                if nodecat in ['VXI','VXIY']:
                    node.cat='S'
                    if node.edge_label=='KONJ' and node.parent.cat=='VP':
                        node.parent.cat='S'
    for n in node.children:
        make_verb_chunks(n)

def collect_verbs(node,ns):
    for n in node.children:
        if n.edge_label=='HD':
            n.edge_label=node.edge_label
            ns.append(n)
        elif n.edge_label in ['AUX+TENSE',
                              'AUX+PASSIVE','AUX+PROGRESSIVE']:
            collect_verbs(n,ns)
        else:
            #assert False,node.to_penn()
            # adverbs can come *in* the verb chunk...
            ns.append(n)

def gather_names(node,ns):
    pos_contin=None
    for (i,n) in enumerate(node.children):
        if n.edge_label in ['CONTIN+DENOM']:
            pos_contin=i
    ns.extend(node.children[:pos_contin])
    if pos_contin!=None:
        gather_names(node.children[pos_contin],ns)
        ns.extend(node.children[pos_contin+1:])
        del node.children[pos_contin:]    

def rename_artp(node):
    """relabel as NP those elliptic phrases that are clearly NPs"""
    if node.cat=='ARTP' and node.edge_label not in ['DET','DET+DEF']:
        node.cat='NP'
    if node.cat in ['ADJP','NUM-P'] and node.edge_label in ['SUBJ','DOBJ']:
        node.cat='NP'
    for n in node.children:
        rename_artp(n)

def rename_conjp(node):
    """relabel as SBAR the likes of CONJ-P -> C_sub S"""
    if node.cat=='CONJ-P':
        csub_seen=False
        s_seen=False
        for n in node.children:
            if n.cat=='S' and n.edge_label=='CONJ-ARG':
                s_seen=True
            elif n.cat in ['C_sub','CADV','CCHE'] and n.edge_label=='HD':
                csub_seen=True
        if s_seen and csub_seen:
            node.cat='SBAR'
    for n in node.children:
        rename_conjp(n)

def kill_unaries(nodes,parent):
    for i,n in enumerate(nodes):
        if n.cat in ['ARTP','PUNCT-P','CONJ-P'] and len(n.children)==1:
            chld=n.children[0]
            chld.edge_label=n.edge_label
            chld.parent=parent
            nodes[i]=chld
        else:
            kill_unaries(n.children,n)

def kill_emptynodes(nodes):
    newnodes=[]
    for n in nodes:
        if n.isTerminal():
            newnodes.append(n)
        elif n.children==[]:
            pass
        elif kill_emptynodes(n.children):
            print >>sys.stderr,"pruned empty node %s"%(n,)
            pass
        else:
            newnodes.append(n)
    nodes[:]=newnodes
    return not(nodes)

def classify_verbs(t):
    for n in t.terminals:
        if n.cat[0]=='V':
            n.cat+='_'+morphit_pos.classify_verb(n.lemma,n.cat[-1]=='Y')
    for n in t.terminals:
        if n.cat[0]=='V' and 'Y' not in n.cat:
            classify_vx(n.parent)

def classify_adjp(t):
    for n in t.terminals:
        if n.cat[0] in ['A','D']:
            if n.parent.cat=='ADJP':
                n1=n.parent
                while n1.cat=='ADJP':
                    n1.cat+='_'+n.cat[-1]
                    if n1.edge_label in ['HD','KONJ']:
                        n1=n1.parent
                    else:
                        break

def classify_np(t):
    for n in t.terminals:
        if n.cat[0]=='P' and n.parent and n.parent.cat=='NP':
            n.parent.cat+='_pro'

def classify_vx(node):
    chlds=node.children
    auxverbs=[n for n in chlds if n.cat[0]=='V' and 'Y' in n.cat]
    fullverbs=[n for n in chlds if n.cat[0]=='V' and 'Y' not in n.cat]
    assert len(fullverbs)==1,node.to_penn()
    catN=fullverbs[-1].cat
    assert ('_' in catN), (node.cat,node.children)
    cls=catN[catN.index('_')+1:]
    if cls=='trans' and '+E' in catN:
        # e.g. chiamarsi
        cls='intr'
    elif cls=='trans' and catN in ['VSP','VPP'] and auxverbs:
        catN1=auxverbs[-1].cat
        cls1=catN1[catN1.index('_')+1:]
        # detect passivization
        if cls1=='pred':
          cls='intr'
    node.cat+='_'+cls
    if node.parent.cat in ['S','VP','S-REL']:
        markovize_s(node.parent)
    else:
        print >>sys.stderr,"don't markovize %s"%(node.parent.cat)

def deepen_vxrest(node,inner_cat,inner_lbl):
    def cut_phrase(n,num,icat=inner_cat,ilbl=inner_lbl):
        if len(n.children)<=num:
            return None
        node2=tree.NontermNode(inner_cat,inner_lbl)
        node2.children=chlds[:-num]
        node2.parent=node1
        for n in node2.children:
            n.parent=node2
        node1.children=[node2]+chlds[-num:]
        return node2
    node1=node
    while node1:
        chlds=node1.children
        node2=node1
        if len(chlds)<2:
            break
        if ((chlds[-1].edge_label in ['RELCL','RMOD','APPOSITION']) and
            chlds[-2].edge_label=='SEPARATOR'):
            #print >>sys.stderr,"cut_PX:%s"%(node1.to_penn(),)
            node1=cut_phrase(node1,2)
        elif (chlds[-1].edge_label=='RELCL' or
            chlds[-1].edge_label=='RMOD' and
              chlds[-1].cat in ['PX','SBAR','VP']):
            #print >>sys.stderr,"cut_PX:%s"%(node1.to_penn(),)
            node1=cut_phrase(node1,1)
        elif (chlds[-1].edge_label=='CLOSE+PARENTHESIS' and
               chlds[-3].edge_label=='OPEN+PARENTHESIS'):
             node1=cut_phrase(node1,3)
        else:
            break
    return node2


def markovize_s(node):
    head_idx=None
    head_seen=False
    prehead_args=[]
    posthead_args=[]
    for i,n in enumerate(node.children):
        if n.edge_label=='HD':
            head_idx=i
            head_seen=True
        elif n.edge_label in arg_letter:
            if head_seen:
                posthead_args.append(arg_letter[n.edge_label])
            else:
                prehead_args.append(arg_letter[n.edge_label])
    if head_seen and head_idx!=len(node.children)-1:
        ns=node.children[head_idx+1:]
        nrest=tree.NontermNode('vxrest_'+''.join(posthead_args),
                               'M-REST')
        nrest.parent=node
        for n in ns:
            n.parent=nrest
        nrest.children=ns
        node.children[head_idx+1:]=[nrest]
        #nrest=deepen_vxrest(nrest,nrest.cat,'M-REST')
    if node.cat=='S' and head_seen and head_idx!=0:
        ns=node.children[:head_idx]
        nrest=tree.NontermNode('vxrestL_'+''.join(prehead_args),
                               'M-REST')
        nrest.parent=node
        for n in ns:
            n.parent=nrest
        nrest.children=ns
        node.children[:head_idx]=[nrest]

def deepen_np(node,inner_cat,inner_lbl):
    def cut_np(n,num,icat=inner_cat,ilbl=inner_lbl):
        node2=tree.NontermNode(inner_cat,inner_lbl)
        node2.children=chlds[:-num]
        node2.parent=node1
        for n in node2.children:
            n.parent=node2
        node1.children=[node2]+chlds[-num:]
        return node2
    node1=node
    while True:
        chlds=node1.children
        if len(chlds)<2:
            break
        if ((chlds[-1].edge_label in ['RELCL','RMOD','APPOSITION']) and
            chlds[-2].edge_label=='SEPARATOR'):
            #print >>sys.stderr,"cut_PX:%s"%(node1.to_penn(),)
            node1=cut_np(node1,2)
        elif (chlds[-1].edge_label=='RELCL' or
            chlds[-1].edge_label=='RMOD' and
              chlds[-1].cat in ['PX','SBAR','VP']):
            #print >>sys.stderr,"cut_PX:%s"%(node1.to_penn(),)
            node1=cut_np(node1,1)
        elif (chlds[-1].edge_label=='CLOSE+PARENTHESIS' and
               chlds[-3].edge_label=='OPEN+PARENTHESIS'):
             node1=cut_np(node1,3)
        else:
            break
    return node1

def markovize_np(node):
    """splits off pre- and postmodifiers of common noun NPs,
       and marks prenominal ADJPs"""
    if node.cat=='NP':
        head_idx=None
        head_seen=False
        for i,n in enumerate(node.children):
            if n.cat in ['SS','SN','SP'] and n.edge_label=='HD':
                head_idx=i
                head_seen=True
            elif n.edge_label in ['HD','KONJ']:
                head_seen=True
            #elif n.cat.startswith('ADJP') and not head_seen:
            #    n.cat+='_pre'
        if head_seen:
            node1=node
            #node1=deepen_np(node,'NP','M-REST')
        else:
            node1=node
        if head_idx is not None:
            hcat=node1.children[head_idx].cat
            ns=node1.children[head_idx+1:]
            if ns:
                nrest=tree.NontermNode('nprestR_'+hcat,'M-REST')
                nrest.parent=node1
                for n in ns:
                    n.parent=nrest
                nrest.children=ns
                node1.children[head_idx+1:]=[nrest]
            ns=node1.children[:head_idx]
            if ns:
                nrest=tree.NontermNode('nprestL_'+hcat,'M-REST')
                nrest.parent=node1
                for n in ns:
                    n.parent=nrest
                nrest.children=ns
                node1.children[:head_idx]=[nrest]
    for n in node.children:
        markovize_np(n)
            
arg_letter={'VERB-OBJ/VERB-SUBJ':'s',
            'VERB+MODAL-INDCOMPL':'F',
            'VERB-SUBJ/VERB-SUBJ+IMPERS':'S',
            'EMPTYCOMPL':'d',
            'SUBJ':'s',
            'DOBJ':'d',
            'PRED':'p'}

