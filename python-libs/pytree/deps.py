import tree
import sys
import re

messages={
    'nolabel':"Could not determine label (%s:%s.%s %s:%s.%s)\n",
    'nohead':"No head for %s\n",
    'no_syn_parent':"syn_parent attribute not set: %s\n",
}


def default_warning_handler(w,args):
    sys.stderr.write(messages[w]%args)

warning_handler=default_warning_handler


class DepsError(StandardError):
    def __init__(self,node,msg):
        self.node=node
        self.msg=msg
    def __str__(self):
        return '%s (%s)'%(self.msg,self.node)

class LabelFinder:
    def __init__(self,labelRules):
        self.labelRules=labelRules
    def __call__(self,adjunct,headNode,parentNode):
        assert adjunct.head
        assert headNode.head
        pos1=adjunct.head.cat
        cat1=adjunct.cat
        lbl1=adjunct.edge_label
        pos2=headNode.head.cat
        cat2=parentNode.cat
        lbl2=headNode.edge_label
        for pat1,pat2,label in self.labelRules:
            if pat1:
                if ((pat1[0] is None or pos1 in get_items(pat1[0])) and
                    (pat1[1] is None or cat1 in get_items(pat1[1])) and
                    (pat1[2] is None or lbl1 in get_items(pat1[2]))):
                    pass
                else:
                    continue
            if pat2:
                if ((pat2[0] is None or pos2 in get_items(pat2[0])) and
                    (pat2[1] is None or cat2 in get_items(pat2[1])) and
                    (pat2[2] is None or lbl2 in get_items(pat2[2]))):
                    pass
                else:
                    continue
            return label
        warning_handler('nolabel',(pos1,cat1,lbl1,pos2,cat2,lbl2))
        return '-UNKNOWN-'

def make_labeling_func(rules):
    return LabelFinder(rules)

def get_items(setDescr):
    if setDescr is None or isinstance(setDescr,basestring):
        return [setDescr]
    else:
        return setDescr

try:
    # compiling the rules to bytecode makes the label
    # finder 10-30x faster
    import byteplay
    def compile_labeling_func(rules):
        """compiles a list of rules into python bytecodes"""
        def compiled_func(adjunct,headNode,parentNode):
            assert adjunct.head
            assert headNode.head
            pos1=adjunct.head.cat
            cat1=adjunct.cat
            lbl1=adjunct.edge_label
            pos2=headNode.head.cat
            cat2=parentNode.cat
            lbl2=headNode.edge_label
        next_label=byteplay.Label()
        my_code=[]
        names1=['pos','cat','lbl']
        for ruleno,(pat1,pat2,label) in enumerate(rules):
            my_code.append((byteplay.SetLineno,ruleno+1000))
            for (pat,suffix) in [(pat1,'1'),(pat2,'2')]:
                if pat is None: continue
                for i,val in enumerate(pat):
                    if val is None:
                        continue
                    my_code.append((byteplay.LOAD_FAST,names1[i]+suffix))
                    my_code.append((byteplay.LOAD_CONST,val))
                    if isinstance(val,basestring):
                        my_code.append((byteplay.COMPARE_OP,'=='))
                    else:
                        my_code.append((byteplay.COMPARE_OP,'in'))
                    my_code.append((byteplay.JUMP_IF_FALSE,next_label))
                    my_code.append((byteplay.POP_TOP,None))
            my_code.append((byteplay.LOAD_CONST,label))
            my_code.append((byteplay.RETURN_VALUE,None))
            my_code.append((next_label,None))
            my_code.append((byteplay.POP_TOP,None))
            next_label=byteplay.Label()
        # put something onto the stack to make the
        # bytecode verifier happy
        my_code.append((byteplay.LOAD_CONST,None))
        my_code.append((next_label,None))
        my_code.append((byteplay.SetLineno,2000))
        my_code.append((byteplay.POP_TOP,None))
        my_code.append((byteplay.LOAD_GLOBAL,'warning_handler'))
        my_code.append((byteplay.LOAD_CONST,'nolabel'))
        for suffix in '12':
            for name in names1:
                my_code.append((byteplay.LOAD_FAST,name+suffix))
        my_code.append((byteplay.BUILD_TUPLE,6))
        my_code.append((byteplay.CALL_FUNCTION,2))
        my_code.append((byteplay.POP_TOP,None))
        my_code.append((byteplay.LOAD_CONST,'-UNKNOWN-'))
        my_code.append((byteplay.RETURN_VALUE,None))
        c=byteplay.Code.from_code(compiled_func.func_code)
        c.code=c.code[:-2]+my_code
        compiled_func.func_code=c.to_code()
        try:
            import psyco
            return psyco.proxy(compiled_func,rec=1)
        except ImportError:
            pass
        return compiled_func
    # override make_labeling_func with the rule compiler
    make_labeling_func=compile_labeling_func
except ImportError:
    pass

class HeadRule:
    def __init__(self,ruleDescr):
        self.table={}
        self.table1={}
        self.table2={}
        self.default_pos=1001
        self.dirs=[]
        for pos,descr in enumerate(ruleDescr):
            if len(descr)==3:
                for n in get_items(descr[0]):
                    for e in get_items(descr[1]):
                        if n is not None:
                            if e is not None:
                                self.table[(n,e)]=pos
                            else:
                                self.table1[n]=pos
                        else:
                            if e is not None:
                                self.table2[e]=pos
                            else:
                                self.default_pos=pos
            else:
                if descr[0] is not None:
                    for n in get_items(descr[0]):
                        self.table1[n]=pos
                else:
                    self.default_pos=pos
            if descr[-1]=='r':
                self.dirs.append(True)
            elif descr[-1]=='l':
                self.dirs.append(False)
            else:
                assert False
    def findHead(self,nodes):
        tpos=1000
        headPos=None
        table=self.table
        table1=self.table1
        table2=self.table2
        default_pos=self.default_pos
        for i,node in enumerate(nodes):
            cat=node.cat
            lbl=node.edge_label
            newpos=min(table.get((cat,lbl),1001),
                        table1.get(cat,1001),
                        table2.get(lbl,1001),
                        default_pos)
            if newpos<tpos or (newpos==tpos and self.dirs[newpos]):
                tpos=newpos
                headPos=i
        return headPos

def make_headrules(hr_table):
    headRules={}
    for cats,ruleDescr in hr_table:
        rule=HeadRule(ruleDescr)
        for cat in get_items(cats):
            headRules[cat]=rule
    return headRules

class SimpleDepExtractor:
    def __init__(self,hr_table,punctCats=[],determine_label=None):
        self.headRules=make_headrules(hr_table)
        self.punctCats=punctCats
        if determine_label==None:
            self.determine_label=lambda adj,head,parent: ''
        else:
            self.determine_label=determine_label
    def __call__(self,t):
        for n in t.roots:
            head=self.treedep(n)
            head.syn_parent=None
            if head.cat in self.punctCats:
                head.syn_label=''
            else:
                head.syn_label='S'
        self.modify_deps(t)
    def modify_deps(self,t):
        pass
    def treedep(self,node):
        if node.isTerminal():
            node.head=node
            return node
        # try head projection
        if node.cat in self.headRules:
            pos=self.headRules[node.cat].findHead(node.children)
        else:
            pos=self.headRules[None].findHead(node.children)
        #assert pos>=0 and pos<len(node.children)
        if pos < 0 or pos>=len(node.children):
            warning_handler('nohead',(node,))
            pos=len(node.children)-1
        headNode=node.children[pos]
        head=self.treedep(headNode)
        node.head=head
        self.attach(node.children[0:pos],headNode,node)
        self.attach(node.children[pos+1:],headNode,node)
        return head
    def attach(self,nodes,headNode,parent):
        for n in nodes:
            self.attach1(n,headNode,parent)
    def attach1(self,node,headNode,parent):
        #print "attach1(%s,%s,%s)"%(node,headNode,parent)
        node.head=self.treedep(node)
        label=self.determine_label(node,headNode,parent)
        node.head.syn_parent=headNode.head
        node.head.syn_label=label
