import sys
import getopt
import tree
import hocgrammar
import hoclexer
import re

def unescape(str):
    str=re.sub(r"\\(.)",r"\1",str)
    return str

sym_type=hoclexer.types["sym"]
idx_type=hoclexer.types["index"]
node_type=hocgrammar.types["NODE"]
info_type=hocgrammar.types["INFO"]
list_type=hocgrammar.types["LIST"]

def maketoken(type,obj,loc=None):
    if type==sym_type:
        return unescape(obj)
    elif type==idx_type:
        return int(obj)
    else:
        return None

def makesymbol(type,args):
    if type in [info_type,list_type]:
        return list(args)
    elif type==node_type:
        cat,info,contents=args
        if isinstance(contents,basestring):
            n=tree.TerminalNode(cat,contents)
        else:
            n=tree.NontermNode(cat)
            chlds=n.children
            for n1 in contents:
                chlds.append(n1)
                n1.parent=n
        if info:
            for i in xrange(0,len(info),2):
                setattr(n,info[i],info[i+1])
        return n
            
def number_ids(t,node,start=1):
    if node.isTerminal():
        node.start=start
        node.end=start+1
        t.terminals.append(node)
        return start+1
    else:
        pos=start
        for n in node.children:
            pos=number_ids(t,n,pos)
        return pos

def line2parse(line):
    """Parses a line into a node structure"""
    hoclexer.onstring(maketoken,line)
    try:
        parse=hocgrammar.parse(makesymbol,hoclexer.readtoken)
    finally:
        hoclexer.close()
    return parse
