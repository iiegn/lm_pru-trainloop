import sys
import re

atom_pl=re.compile(r"[a-z][a-zA-Z_0-9]*$")
unwanted_pl=re.compile(r"([\\'])")
def escape_prolog(string):
    if not string: return "''"
    if atom_pl.match(string):
        return string
    res=unwanted_pl.sub(r"\\\1",string)
    return "'%s'"%(res,)

unwanted_mrg=re.compile(r"([^A-Za-z0-9\x80-\xff\-_])")
def escape_mrg(string):
    if not string: return ''
    return unwanted_mrg.sub(r"\\\1",string)

def bottomup_enumeration(nodes):
    #print "bottomup_enumeration: %s"%(nodes)
    for n in nodes:
        for n1 in bottomup_enumeration(n.children):
            yield n1
        yield n

def descendants(node):
    for n in node.children:
        yield n
        for n1 in descendants(n):
            yield n1
    return


def determine_tokenspan(node):
    if not node.isTerminal():
        assert node.children,(node.cat,node.id)
        node.start=min(map(lambda x:x.start, node.children))
        node.end=max(map(lambda x:x.end, node.children))

class Tree(object):
    __slots__=['node_table','roots','terminals','__dict__']
    def __getstate__(self):
        return (self.node_table,
                self.roots,
                self.terminals,
                self.__dict__)
    def __setstate__(self,state):
        self.node_table, self.roots, self.terminals, self.__dict__=state
    """represents a syntax tree"""
    def __init__(self):
        self.node_table={}
        self.roots=[]
        self.terminals=[]
    def __iter__(self):
        return iter(self.roots)
    def bottomup_enumeration(self):
        return bottomup_enumeration(self.roots)
    def topdown_enumeration(self):
        for n in self.roots:
            yield n
            for n1 in descendants(n):
                yield n1
        return
    def determine_tokenspan_all(self):
        "determines the tokenspan for all nodes and sorts children accordingly"
        def by_pos(a,b):
            return cmp(a.start,b.start)
        for node in self.bottomup_enumeration():
            determine_tokenspan(node)
            node.children.sort(by_pos)
        self.roots.sort(by_pos)
    def check_roots(self):
        for n in self.roots:
            assert n.parent==None
            self.check_nodes(n,[])
    def check_nodes(self,node,parents):
        if node.parent==None:
            assert parents==[]
        else:
            assert node.parent==parents[-1]
        parents.append(node)
        for n in node.children:
            assert not n in parents
            self.check_nodes(n,parents)
        del parents[-1]
    def renumber_ids(self,nodes=None,start=500):
        """gives ids to all nonterminal nodes."""
        pos=start
        if nodes==None: nodes=self.roots
        for n in nodes:
            if not n.isTerminal():
                #print "Renumber %r: entering %s, pos=%d"%(n.id,pos)
                pos=1+self.renumber_ids(n.children,pos)
                #sys.stderr.write("Renumber %r: %s => %d\n"%(n,n.id,pos))
                n.id="%s"%pos
                self.node_table[n.id]=n
        return pos
    def check_nodetable(self):
        for key in self.node_table:
            if self.node_table[key].id != key:
                raise "Nodetable: node %s(%r) has id %s"%(key,
                    self.node_table[key],self.node_table[key].id)
            assert self.node_table[key].id==key
            if self.node_table[key].parent==None:
                assert self.node_table[key] in self.roots
            else:
                parent=self.node_table[key].parent
                assert self.node_table[parent.id]==parent
    def discontinuity(self,nodes,index,sent_node):
        """returns True iff there is a discontinuity between
        the Nth and the N+1th member of nodes, ignoring
        punctuation and parentheses."""
        if nodes[index].end==nodes[index+1].start: return False
        sys.stderr.write('Looking for a discontinuity between %r and %r'%(
            self.terminals[nodes[index].end],
            self.terminals[nodes[index+1].start]))
        for n in self.terminals[nodes[index].end:nodes[index+1].start]:
            n1=n
            while n1!=None:
                if n1==sent_node: return True
                n1=n1.parent
        return False

    
# abstract base class for all nodes
class Node(object):
    def __init__(self,cat):
        self.id=None
        self.start=-1
        self.end=-1
        self.cat=cat
        self.children=[]
        self.parent=None
    def add_at(self,node,pos):
        self.children[pos:pos]=[node]
        node.set_parent(self)
    def append(self,node):
        self.children.append(node)
        node.set_parent(self)
    def insert(self,node):
        "inserts a node at the appropriate position"
        node.set_parent(self)
        for (i,n) in enumerate(self.children):
            if (n.start>=node.start):
                self.children[i:i]=[node]
                return
        self.append(node)
    def set_parent(self,parent):
        self.parent=parent

class NontermNode(Node):
    "Node class for nonterminal node"
    def __init__(self,cat,edge_label=None):
        Node.__init__(self,cat)
        self.edge_label=edge_label
        self.attr='--'
    def __repr__(self):
        return '<%s.%s at %s>'%(self.cat,self.edge_label,hex(id(self)))
    def isTerminal(self):
        return False
    def __str__(self):
        return '<NonTerm %s #%s>'%(self.cat,self.id)
    def to_penn(self):
        if self.edge_label:
            a="(%s.%s "%(self.cat,self.edge_label)
        else:
            a="(%s "%(self.cat)
        a+=' '.join(map(lambda x:x.to_penn(),self.children))
        a+=")"
        return a
    def to_full(self,wanted_attrs):
        pairs=[]
        for key in wanted_attrs:
            pairs.append('%s=%s'%(key,escape_mrg(str(getattr(self,key,'--')))))
        a="(%s"%(escape_mrg(self.cat))
        if pairs:
            a=a+"=#i[%s]"%(' '.join(pairs))
        a+=" %s)"%(' '.join(map(lambda x:x.to_full(wanted_attrs),self.children)),)
        return a


class TerminalNode(Node):
    "Node class for a preterminal node"
    def __init__(self,cat,word,edge_label=None,morph=None):
        Node.__init__(self,cat)
        self.word=word
        self.edge_label=edge_label
        self.morph=morph
    def __repr__(self):
        return '<%s/%s(%d) at %s>'%(self.word,self.cat,self.start,hex(id(self)))
    def isTerminal(self):
        return True
    def to_penn(self):
        if self.edge_label:
            return "(%s.%s %s)"%(self.cat,self.edge_label,self.word)
        else:
            return "(%s %s)"%(self.cat,self.word)
    def to_full(self,wanted_attrs):
        pairs=[]
        for key in wanted_attrs:
            pairs.append('%s=%s'%(key,escape_mrg(str(getattr(self,key,'--')))))
        a="(%s"%(escape_mrg(self.cat),)
        if pairs:
            a=a+"=#i[%s]"%(' '.join(pairs))
        a+=" %s)"%(escape_mrg(self.word),)
        return a

