
# This file was generated automatically

# action 0 for: [(23) Attrs.list2 -> , AttrVal]
def action0(kids) :
	return kids

# action 1 for: [(0) START -> NodePat]
def action1(kids) :
	return kids[0]
	

# action 2 for: [(1) Rule -> $$rewrite id NodePat $$as NodePat]
def action2(kids) :
	return ('rewrite',kids[1],kids[2],kids[4])
	

# action 3 for: [(2) NodePat -> # id Attrs]
def action3(kids) :
	return [('#',kids[1])]+kids[2]
	

# action 4 for: [(3) NodePat -> # id = NodeDescr]
def action4(kids) :
	return [('#',kids[1])]+kids[3]
	

# action 5 for: [(4) NodePat -> NodeDescr]
def action5(kids) :
	return kids[0]
	

# action 6 for: [(5) NodeDescr -> ( CatDescr Attrs NodeContents )]
def action6(kids) :
	return kids[1]+kids[2]+kids[3]
	

# action 7 for: [(6) CatDescr -> id]
def action7(kids) :
	return [('cat',kids[0])]
	

# action 8 for: [(7) CatDescr -> id * id]
def action8(kids) :
	return [('cat*',kids[0]),('*cat',kids[2])]
	

# action 9 for: [(8) CatDescr -> id *]
def action9(kids) :
	return [('cat*',kids[0])]
	

# action 10 for: [(9) CatDescr -> * id]
def action10(kids) :
	return [('*cat',kids[0])]
	

# action 11 for: [(10) CatDescr -> $ id]
def action11(kids) :
	return [('cat$',kids[1])]
	

# action 12 for: [(11) CatDescr -> *]
def action12(kids) :
	return []
	

# action 13 for: [(12) NodeContents -> id]
def action13(kids) :
	return [('word',kids[0])]
	

# action 14 for: [(13) NodeContents -> $ id]
def action14(kids) :
	return [('word$',kids[1])]
	

# action 15 for: [(14) NodeContents -> *]
def action15(kids) :
	return []
	

# action 16 for: [(15) NodeContents.posclos1 -> SeqPat]
def action16(kids) :
	return [kids[0]]

# action 17 for: [(16) NodeContents.posclos1 -> NodeContents.posclos1 SeqPat]
def action17(kids) :
	kids[0].append(kids[1])
	return kids[0]

# action 18 for: [(17) NodeContents -> NodeContents.posclos1]
def action18(kids) :
	return [('children',kids[0])]
	

# action 19 for: [(18) SeqPat -> ... id]
def action19(kids) :
	return ('dots',kids[1])
	

# action 20 for: [(19) SeqPat -> NodePat]
def action20(kids) :
	return ('node',kids[0])
	

# action 21 for: [(20) Attrs ->]
def action21(kids) :
	return []
	

# action 22 for: [(21) Attrs -> . id Attrs]
def action22(kids) :
	return [('edge_label',kids[1])]+kids[2]
	

# action 23 for: [(22) Attrs -> . $ id Attrs]
def action23(kids) :
	return [('edge_label$',kids[2])]+kids[3]
	

# action 24 for: [(24) Attrs.clos3 ->]
def action24(kids) :
	return []

# action 25 for: [(25) Attrs.clos3 -> Attrs.clos3 Attrs.list2]
def action25(kids) :
	kids[0].append(kids[1])
	return kids[0]

# action 26 for: [(26) Attrs -> [ AttrVal Attrs.clos3 ]]
def action26(kids) :
	return [kids[1]]+map(lambda x:x[1], kids[2])
	

# action 27 for: [(27) AttrVal -> id = id]
def action27(kids) :
	return (kids[0],kids[2])
	

# action 28 for: [(28) AttrVal -> id = $ id]
def action28(kids) :
	return (kids[0]+'$',kids[3])
	
goto = {(31, 19): 40, (20, 20): 34, (18, 4): 29, (18, 19): 32, (1, 'id'): 6, (27, 'id'): 38, (31, 5): 5, (17, 'id'): 24, (0, 4): 4, (33, 20): 41, (23, 25): 36, (2, 6): 10, (33, '.'): 11, (31, '#'): 1, (22, '='): 35, (6, 26): 14, (18, '*'): 26, (18, '...'): 27, (2, 11): 10, (18, 'id'): 28, (31, 2): 29, (44, 'id'): 22, (2, '$'): 7, (18, 15): 31, (18, '('): 2, (6, '['): 13, (6, '.'): 11, (20, 26): 34, (36, ']'): 45, (31, 18): 40, (20, 21): 34, (18, 5): 5, (18, 16): 31, (8, 'id'): 16, (12, '('): 2, (25, 'id'): 37, (31, 4): 29, (36, 23): 46, (0, 5): 5, (23, 24): 36, (2, 7): 10, (33, '['): 13, (6, '='): 12, (11, 'id'): 20, (6, 20): 14, (10, 20): 18, (0, 2): 4, (33, 22): 41, (2, 8): 10, (18, '$'): 25, (10, 26): 18, (42, 'id'): 47, (7, 'id'): 15, (18, 12): 30, (18, '#'): 1, (13, 'id'): 22, (20, 22): 34, (18, 2): 29, (18, 17): 30, (13, 28): 23, (10, '['): 13, (44, 28): 48, (31, '...'): 27, (18, 18): 32, (2, 'id'): 9, (6, 21): 14, (12, 5): 21, (10, 21): 18, (35, 'id'): 43, (0, 3): 4, (20, '.'): 11, (35, '$'): 42, (33, 21): 41, (2, 9): 10, (19, 'id'): 33, (33, 26): 41, (10, '.'): 11, (6, 22): 14, (10, 22): 18, (18, 13): 30, (0, 0): 3, (11, '$'): 19, (2, '*'): 8, (2, 10): 10, (9, '*'): 17, (0, '('): 2, (31, 3): 29, (0, '#'): 1, (20, '['): 13, (18, 3): 29, (44, 27): 48, (31, '('): 2, (18, 14): 30, (30, ')'): 39, (13, 27): 23, (36, ','): 44}
action = {(33, '$$as'): [('reduce', ('Attrs', 0, 20))], (29, ')'): [('reduce', ('SeqPat', 1, 19))], (9, '#'): [('reduce', ('CatDescr', 1, 6))], (17, 'id'): [('shift', 24), ('reduce', ('CatDescr', 2, 8))], (41, ')'): [('reduce', ('Attrs', 4, 22))], (15, '('): [('reduce', ('CatDescr', 2, 10))], (20, '...'): [('reduce', ('Attrs', 0, 20))], (9, '('): [('reduce', ('CatDescr', 1, 6))], (20, '*'): [('reduce', ('Attrs', 0, 20))], (33, '$EOF$'): [('reduce', ('Attrs', 0, 20))], (40, '('): [('reduce', ('NodeContents.posclos1', 2, 16))], (33, '('): [('reduce', ('Attrs', 0, 20))], (1, 'id'): [('shift', 6)], (33, '...'): [('reduce', ('Attrs', 0, 20))], (27, 'id'): [('shift', 38)], (10, '*'): [('reduce', ('Attrs', 0, 20))], (33, 'id'): [('reduce', ('Attrs', 0, 20))], (41, 'id'): [('reduce', ('Attrs', 4, 22))], (6, '*'): [('reduce', ('Attrs', 0, 20))], (18, '*'): [('shift', 26)], (16, 'id'): [('reduce', ('CatDescr', 2, 9))], (24, '.'): [('reduce', ('CatDescr', 3, 7))], (34, '*'): [('reduce', ('Attrs', 3, 21))], (8, '('): [('reduce', ('CatDescr', 1, 11))], (33, ')'): [('reduce', ('Attrs', 0, 20))], (41, '*'): [('reduce', ('Attrs', 4, 22))], (18, '...'): [('shift', 27)], (20, '#'): [('reduce', ('Attrs', 0, 20))], (32, '('): [('reduce', ('NodeContents.posclos1', 1, 15))], (29, '#'): [('reduce', ('SeqPat', 1, 19))], (14, '#'): [('reduce', ('NodePat', 3, 2))], (21, '$$as'): [('reduce', ('NodePat', 4, 3))], (15, 'id'): [('reduce', ('CatDescr', 2, 10))], (45, '*'): [('reduce', ('Attrs', 4, 26))], (10, ')'): [('reduce', ('Attrs', 0, 20))], (17, '*'): [('reduce', ('CatDescr', 2, 8))], (6, ')'): [('reduce', ('Attrs', 0, 20))], (9, '.'): [('reduce', ('CatDescr', 1, 6))], (32, ')'): [('reduce', ('NodeContents.posclos1', 1, 15))], (24, '$'): [('reduce', ('CatDescr', 3, 7))], (46, ','): [('reduce', ('Attrs.clos3', 2, 25))], (33, '.'): [('shift', 11)], (31, '#'): [('shift', 1)], (39, '$$as'): [('reduce', ('NodeDescr', 5, 5))], (22, '='): [('shift', 35)], (6, '$EOF$'): [('reduce', ('Attrs', 0, 20))], (10, '('): [('reduce', ('Attrs', 0, 20))], (20, 'id'): [('reduce', ('Attrs', 0, 20))], (6, '('): [('reduce', ('Attrs', 0, 20))], (8, '#'): [('reduce', ('CatDescr', 1, 11))], (34, '('): [('reduce', ('Attrs', 3, 21))], (41, '$EOF$'): [('reduce', ('Attrs', 4, 22))], (8, '...'): [('reduce', ('CatDescr', 1, 11))], (21, '...'): [('reduce', ('NodePat', 4, 3))], (8, '*'): [('reduce', ('CatDescr', 1, 11))], (16, '#'): [('reduce', ('CatDescr', 2, 9))], (48, ']'): [('reduce', ('Attrs.list2', 2, 23))], (16, '('): [('reduce', ('CatDescr', 2, 9))], (18, 'id'): [('shift', 28)], (44, 'id'): [('shift', 22)], (45, '('): [('reduce', ('Attrs', 4, 26))], (2, '$'): [('shift', 7)], (17, '.'): [('reduce', ('CatDescr', 2, 8))], (32, '...'): [('reduce', ('NodeContents.posclos1', 1, 15))], (34, '$$as'): [('reduce', ('Attrs', 3, 21))], (16, '['): [('reduce', ('CatDescr', 2, 9))], (10, '...'): [('reduce', ('Attrs', 0, 20))], (6, '['): [('shift', 13)], (6, '.'): [('shift', 11)], (8, '['): [('reduce', ('CatDescr', 1, 11))], (28, ')'): [('reduce', ('NodeContents', 1, 12))], (45, '$EOF$'): [('reduce', ('Attrs', 4, 26))], (3, '$EOF$'): [('accept', None)], (16, '*'): [('reduce', ('CatDescr', 2, 9))], (36, ']'): [('shift', 45)], (38, ')'): [('reduce', ('SeqPat', 2, 18))], (39, '...'): [('reduce', ('NodeDescr', 5, 5))], (8, 'id'): [('shift', 16), ('reduce', ('CatDescr', 1, 11))], (20, ')'): [('reduce', ('Attrs', 0, 20))], (17, '['): [('reduce', ('CatDescr', 2, 8))], (12, '('): [('shift', 2)], (43, ','): [('reduce', ('AttrVal', 3, 27))], (9, '['): [('reduce', ('CatDescr', 1, 6))], (25, 'id'): [('shift', 37)], (38, '('): [('reduce', ('SeqPat', 2, 18))], (9, '...'): [('reduce', ('CatDescr', 1, 6))], (15, '#'): [('reduce', ('CatDescr', 2, 10))], (41, '#'): [('reduce', ('Attrs', 4, 22))], (21, ')'): [('reduce', ('NodePat', 4, 3))], (20, '$'): [('reduce', ('Attrs', 0, 20))], (5, '$$as'): [('reduce', ('NodePat', 1, 4))], (10, '$$as'): [('reduce', ('Attrs', 0, 20))], (8, '.'): [('reduce', ('CatDescr', 1, 11))], (39, '$EOF$'): [('reduce', ('NodeDescr', 5, 5))], (33, '#'): [('reduce', ('Attrs', 0, 20))], (24, 'id'): [('reduce', ('CatDescr', 3, 7))], (33, '['): [('shift', 13)], (6, '='): [('shift', 12)], (43, ']'): [('reduce', ('AttrVal', 3, 27))], (45, '$'): [('reduce', ('Attrs', 4, 26))], (34, '...'): [('reduce', ('Attrs', 3, 21))], (11, 'id'): [('shift', 20)], (46, ']'): [('reduce', ('Attrs.clos3', 2, 25))], (14, '...'): [('reduce', ('NodePat', 3, 2))], (6, '#'): [('reduce', ('Attrs', 0, 20))], (34, '#'): [('reduce', ('Attrs', 3, 21))], (10, 'id'): [('reduce', ('Attrs', 0, 20))], (5, ')'): [('reduce', ('NodePat', 1, 4))], (10, '$'): [('reduce', ('Attrs', 0, 20))], (31, ')'): [('reduce', ('NodeContents', 1, 17))], (6, '$$as'): [('reduce', ('Attrs', 0, 20))], (18, '$'): [('shift', 25)], (15, '['): [('reduce', ('CatDescr', 2, 10))], (17, '('): [('reduce', ('CatDescr', 2, 8))], (17, '#'): [('reduce', ('CatDescr', 2, 8))], (16, '.'): [('reduce', ('CatDescr', 2, 9))], (42, 'id'): [('shift', 47)], (7, 'id'): [('shift', 15)], (15, '.'): [('reduce', ('CatDescr', 2, 10))], (10, '#'): [('reduce', ('Attrs', 0, 20))], (39, '#'): [('reduce', ('NodeDescr', 5, 5))], (16, '...'): [('reduce', ('CatDescr', 2, 9))], (6, 'id'): [('reduce', ('Attrs', 0, 20))], (18, '#'): [('shift', 1)], (23, ']'): [('reduce', ('Attrs.clos3', 0, 24))], (29, '...'): [('reduce', ('SeqPat', 1, 19))], (13, 'id'): [('shift', 22)], (48, ','): [('reduce', ('Attrs.list2', 2, 23))], (21, '$EOF$'): [('reduce', ('NodePat', 4, 3))], (45, '#'): [('reduce', ('Attrs', 4, 26))], (40, '#'): [('reduce', ('NodeContents.posclos1', 2, 16))], (20, '['): [('shift', 13)], (38, '...'): [('reduce', ('SeqPat', 2, 18))], (24, '['): [('reduce', ('CatDescr', 3, 7))], (15, '...'): [('reduce', ('CatDescr', 2, 10))], (47, ']'): [('reduce', ('AttrVal', 4, 28))], (38, '#'): [('reduce', ('SeqPat', 2, 18))], (45, 'id'): [('reduce', ('Attrs', 4, 26))], (10, '['): [('shift', 13)], (20, '$EOF$'): [('reduce', ('Attrs', 0, 20))], (39, ')'): [('reduce', ('NodeDescr', 5, 5))], (14, ')'): [('reduce', ('NodePat', 3, 2))], (45, ')'): [('reduce', ('Attrs', 4, 26))], (31, '...'): [('shift', 27)], (15, '*'): [('reduce', ('CatDescr', 2, 10))], (32, '#'): [('reduce', ('NodeContents.posclos1', 1, 15))], (10, '$EOF$'): [('reduce', ('Attrs', 0, 20))], (33, '$'): [('reduce', ('Attrs', 0, 20))], (45, '$$as'): [('reduce', ('Attrs', 4, 26))], (5, '$EOF$'): [('reduce', ('NodePat', 1, 4))], (14, '$EOF$'): [('reduce', ('NodePat', 3, 2))], (24, '...'): [('reduce', ('CatDescr', 3, 7))], (41, '$'): [('reduce', ('Attrs', 4, 22))], (14, '('): [('reduce', ('NodePat', 3, 2))], (24, '('): [('reduce', ('CatDescr', 3, 7))], (2, 'id'): [('shift', 9)], (18, '('): [('shift', 2)], (45, '...'): [('reduce', ('Attrs', 4, 26))], (40, '...'): [('reduce', ('NodeContents.posclos1', 2, 16))], (4, '$EOF$'): [('reduce', ('START', 1, 0))], (17, '...'): [('reduce', ('CatDescr', 2, 8))], (41, '('): [('reduce', ('Attrs', 4, 22))], (9, 'id'): [('reduce', ('CatDescr', 1, 6))], (47, ','): [('reduce', ('AttrVal', 4, 28))], (35, 'id'): [('shift', 43)], (34, '$'): [('reduce', ('Attrs', 3, 21))], (9, '$'): [('reduce', ('CatDescr', 1, 6))], (20, '.'): [('shift', 11)], (23, ','): [('reduce', ('Attrs.clos3', 0, 24))], (26, ')'): [('reduce', ('NodeContents', 1, 14))], (35, '$'): [('shift', 42)], (17, '$'): [('reduce', ('CatDescr', 2, 8))], (19, 'id'): [('shift', 33)], (6, '...'): [('reduce', ('Attrs', 0, 20))], (37, ')'): [('reduce', ('NodeContents', 2, 13))], (21, '#'): [('reduce', ('NodePat', 4, 3))], (10, '.'): [('shift', 11)], (21, '('): [('reduce', ('NodePat', 4, 3))], (40, ')'): [('reduce', ('NodeContents.posclos1', 2, 16))], (24, '#'): [('reduce', ('CatDescr', 3, 7))], (34, ')'): [('reduce', ('Attrs', 3, 21))], (24, '*'): [('reduce', ('CatDescr', 3, 7))], (39, '('): [('reduce', ('NodeDescr', 5, 5))], (8, '$'): [('reduce', ('CatDescr', 1, 11))], (6, '$'): [('reduce', ('Attrs', 0, 20))], (14, '$$as'): [('reduce', ('NodePat', 3, 2))], (16, '$'): [('reduce', ('CatDescr', 2, 9))], (11, '$'): [('shift', 19)], (2, '*'): [('shift', 8)], (34, 'id'): [('reduce', ('Attrs', 3, 21))], (41, '$$as'): [('reduce', ('Attrs', 4, 22))], (9, '*'): [('shift', 17), ('reduce', ('CatDescr', 1, 6))], (20, '('): [('reduce', ('Attrs', 0, 20))], (0, '('): [('shift', 2)], (5, '...'): [('reduce', ('NodePat', 1, 4))], (33, '*'): [('reduce', ('Attrs', 0, 20))], (5, '#'): [('reduce', ('NodePat', 1, 4))], (0, '#'): [('shift', 1)], (5, '('): [('reduce', ('NodePat', 1, 4))], (41, '...'): [('reduce', ('Attrs', 4, 22))], (31, '('): [('shift', 2)], (29, '('): [('reduce', ('SeqPat', 1, 19))], (30, ')'): [('shift', 39)], (36, ','): [('shift', 44)], (20, '$$as'): [('reduce', ('Attrs', 0, 20))], (34, '$EOF$'): [('reduce', ('Attrs', 3, 21))], (15, '$'): [('reduce', ('CatDescr', 2, 10))]}
semactions = [action1, action2, action3, action4, action5, action6, action7, action8, action9, action10, action11, action12, action13, action14, action15, action16, action17, action18, action19, action20, action21, action22, action23, action0, action24, action25, action26, action27, action28]
gramspec = (goto, action, semactions)


