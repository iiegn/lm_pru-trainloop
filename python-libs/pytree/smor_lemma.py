import sys
import sfst
import re
import med
from pytree.sfst_util import *

smor_a=sfst.load_automaton(sys.prefix+'/sfst/smor/smor.a')

def propercase(word):
    return word[0].upper()+word[1:]

gend_map={'NoGend':'*',
    'Masc':'m',
    'Neut':'n',
    'Fem':'f'}
case_map={'Nom':'n', 'Acc':'a',
    'Dat':'d', 'Gen':'g'}
num_map={'Sg':'s', 'Pl':'p'}
nn_re=re.compile(r'<\+NN><(Neut|NoGend|Masc|Fem)>(?:<Old>)?<(Nom|Acc|Dat|Gen)><(Sg|Pl)>(<St>|<Wk>|)')
appr_re=re.compile(r'<\+PREP><(Nom|Acc|Dat|Gen)>')
adj_re=re.compile('e[rmns]?$')
nprop_re=re.compile(r'<\+NPROP>')

def normalize_nn_morph(mor):
    return '<+NN><%s><Nom><Sg>%s'%(mor.group(1),
                                   mor.group(4))
def normalize_nn_morph2(mor):
    return '<+NN><%s><Nom><Pl>%s'%(mor.group(1),
                                   mor.group(4))

def lemmatise(word):
    # TODO: put the analyses in pl and sg form together
    lemmas_a_sg=set()
    lemmas_a_pl=set()
    lemmas_a_v=set()
    lemmas_a_ne=set()
    for a in analyses(word,smor_a):
        if '<+NN>' in a:
            if ('<ADJ><SUFF><+NN>' in a or
                '<ADJ><SUFF><SUFF><+NN>' in a):
                a0=nn_re.sub('<+NN><Masc><Nom><Sg><St>',a)
                lemmas_a_sg.add(a0)
                a0=nn_re.sub('<+NN><NoGend><Nom><Pl><St>',a)
                lemmas_a_pl.add(a0)
            else:
                a0=nn_re.sub(normalize_nn_morph,a)
                lemmas_a_sg.add(a0)
                a0=nn_re.sub(normalize_nn_morph2,a)
                lemmas_a_pl.add(a0)
        elif '<+V>' in a:
            a0=a[:a.index('<+V>')]+'<+V><Inf>'
            lemmas_a_v.add(a0)
        elif ('<+NPROP>' in a and
              not '<PREF>' in a and
              not '<TRUNC>' in a):
            if '<^ABBR><+NPROP>' in a:
                a0=a[:a.index('<^ABBR><+NPROP>')]
            else:
                a0=a[:a.index('<+NPROP>')]
            lemmas_a_ne.add(a0)
    lemmas_a_sg=set(disamb_bool(lemmas_a_sg, lambda a: '<V><SUFF><+NN>' not in a))
    lemmas_sg=set()
    lemmas_pl=set()
    lemmas_v=set()
    for a in lemmas_a_sg:
        words=generate(a,smor_a)
        words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
        lemmas_sg.update(words)
    for a in lemmas_a_pl:
        words=generate(a,smor_a)
        words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
        lemmas_pl.update(words)
    for a in lemmas_a_v:
        words=generate(a,smor_a)
        words=disamb_rank(words, lambda w: med.edit_dist(w,word,0))
        lemmas_v.update(words)        
    return {'N_sg':list(lemmas_sg),
            'N_pl':list(lemmas_pl),
            'V':list(lemmas_v),
            'NE':list(lemmas_a_ne)}

if __name__=='__main__':
    for l in sys.stdin:
        word=l.strip('\r\n')
        lem=lemmatise(word)
        all_lem=[]
        for l in lem['N_sg']:
            all_lem.append('%s:N_sg'%(l,))
        for l in lem['N_pl']:
            all_lem.append('%s:N_pl'%(l,))
        for l in lem['V']:
            all_lem.append('%s:V'%(l,))
        if all_lem:
            print "%s\t%s"%(word,' '.join(all_lem))

