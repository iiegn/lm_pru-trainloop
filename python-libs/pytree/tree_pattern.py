from pytree.tree import NontermNode, TerminalNode, determine_tokenspan
import pyggy
import pyggy.lexer
from pyggy.srgram import SRGram
from pyggy.glr import GLR
import pytree.pytree_gramtab
from pytree.pytree_gramtab import gramspec,semactions
from pytree.pytree_lextab import lexspec
from pytree.export import *
import compiler
import cStringIO

l=pyggy.lexer.lexer(lexspec)
p=GLR(SRGram(gramspec))
ptab=semactions

def compile_nodepattern(str):
    l.setinputstr(str)
    p.setlexer(l)
    parsed=p.parse()
    pat=pyggy.proctree(parsed, pytree.pytree_gramtab)
    return pat

def gen_offset(seq_id,off):
    off1,off2=off
    if off1=='from_start':
        return str(off2)
    elif off1=='from_end':
        return 'len(seq_%s)%+d'%(seq_id,off2)
    else:
        return 'off_%s+%d'%(off1,off2)

class Opcodes:
    def __init__(self):
        self.nodedict={}
        self.dotsdict={}
        self.lastnode=0
        self.lastlist=-1
        self.lastoff=-1
        self.lastseq=-1
        self.curoff=None
        self.preds=[]
    def compile_nodepattern(self,pat,curnode=0):
        for x in pat:
            if x[0]=='#':
                self.nodedict[x[1]]=curnode
            elif x[0]=='children':
                self.lastseq+=1
                self.preds.append(('set_children',curnode,self.lastseq))
                self.compile_seqpattern(x[1],self.lastseq)
            else:
                self.preds.append(('check_field',curnode,x[0],x[1]))
    def compile_seqpattern(self,pat,curseq):
        dots_pos=[i for i,n in enumerate(pat)
                  if n[0]=='dots']
        min_len=0
        for n in pat:
            if n[0]=='node': min_len+=1
        print dots_pos,min_len
        if dots_pos:
            last_dots=dots_pos[-1]
            pat1=pat[:last_dots]
            dots_mid=pat[last_dots]
            pat2=pat[last_dots+1:]
        else:
            pat1=pat
            dots_mid=None
            pat2=[]
        curpos_a='from_start'
        curpos_b=0
        if dots_mid:
            self.preds.append(('check_mid',curseq,('from_start',min_len)))
        else:
            self.preds.append(('check_end',curseq,('from_start',min_len)))
        for x in pat1:
            if x[0]=='node':
                self.lastnode+=1
                self.preds.append(('set_node',curseq,(curpos_a,curpos_b),
                                   self.lastnode))
                self.compile_nodepattern(x[1],self.lastnode)
                curpos_b+=1
                min_len-=1
            elif x[0]=='dots':
                self.lastoff+=1
                self.preds.append(('dots',curseq,(curpos_a,curpos_b),
                                   self.lastoff,min_len))
                nextpos=(self.lastoff,0)
                self.dotsdict[x[1]]=(curseq,(curpos_a,curpos_b),nextpos)
                curpos_a,curpos_b=nextpos
        if dots_mid:
            # using min_len should make sure that
            # 
            ## self.preds.append(('check_mid',curseq,(curpos_a,curpos_b),
            ##                   len(pat2)))
            self.dotsdict[dots_mid[1]]=(curseq,(curpos_a,curpos_b),
                                        ('from_end',-min_len))
            for i,x in enumerate(pat2):
                curpos=('from_end',i-len(pat2))
                assert x[0]=='node'
                self.lastnode+=1
                self.preds.append(('set_node',curseq,curpos,self.lastnode))
                self.compile_nodepattern(x[1],self.lastnode)
        #else:
        #    self.preds.append(('check_end',curseq,(curpos_a,curpos_b)))
    def make_matcher(self):
        s=cStringIO.StringIO()
        s.write('def matcher(node_0):\n')
        indent=1
        redo_action='return'
        for pred in self.preds:
            s.write('\t'*indent)
            if pred[0]=='set_children':
                s.write('seq_%d=node_%d.children'%(pred[1],pred[2]))
            elif pred[0]=='check_field':
                s.write('if node_%d.%s != "%s": %s'%(pred[1],pred[2],pred[3],
                                                     redo_action))
            elif pred[0]=='dots':
                s.write('for off_%d in xrange(%s,len(seq_%s)-%d):'%(
                    pred[3],gen_offset(pred[1],pred[2]),
                    pred[1],pred[4]))
                indent+=1
                redo_action='continue'
            elif pred[0]=='set_node':
                s.write('node_%d=seq_%d[%s]'%(pred[3],pred[1],
                                              gen_offset(pred[1],pred[2])))
            elif pred[0]=='check_end':
                s.write('if len(seq_%d)!=%s: %s'%(pred[1],
                                                  gen_offset(pred[1],pred[2]),
                                                  redo_action))
            elif pred[0]=='check_mid':
                s.write('if len(seq_%d)<%s: %s'%(pred[1],
                                                  gen_offset(pred[1],pred[2]),
                                                  redo_action))
            else:
                s.write('# unknown instruction %s'%(pred,))
            s.write('\n')
        s_indent='\t'*indent
        s.write(s_indent); s.write("node_dict={}\n")
        for k,v in self.nodedict.iteritems():
            s.write(s_indent); s.write("node_dict['%s']=node_%d\n"%(k,v))
        s.write(s_indent); s.write("dots_dict={}\n")
        for k,v in self.dotsdict.iteritems():
            s.write(s_indent)
            s.write("dots_dict['%s']=(seq_%d,%s,%s)\n"%(
                k,v[0],gen_offset(v[0],v[1]),gen_offset(v[0],v[2])))
        s.write(s_indent); s.write('yield (node_dict,dots_dict)\n')
        return s.getvalue()

def compile_matcher(str):
    """parses a nodepattern, compiles the opcode to python code
    and returns the corresponding generator function."""
    pat=compile_nodepattern(str)
    op=Opcode()
    op.compile_nodepattern(pat)
    code_str=op.make_matcher()
    d={}
    eval(compiler.compile(code_str,'compiled_matcher','exec'),globals(),d)
    return d['matcher']

def match_nodepattern(pat,node,nodedict,dotsdict,symdict):
    for x in pat:
        if x[0]=='#':
            #print "Assigning #%s"%x[1]
            nodedict[x[1]]=node
        elif x[0]=='children':
            #print "Testing children: %s"%(x[1],)
            # BUG: match_seqpattern should be used
            #      nondet and not cc_semidet
            m_iter=match_seqpattern(x[1],node.children,0,len(node.children),
                    nodedict,dotsdict,symdict)
            m_iter.next()
        elif x[0].endswith('$'):
            field=x[0][:-1]
            if symdict.has_key(x[1]):
                if not symdict[x[1]]==getattr(node,field):
                    return
            else:
                symdict[x[1]]=getattr(node,field)
        elif x[0].endswith('*'):
            field=x[0][:-1]
            if not getattr(node,field).startswith(x[1]):
                return
        elif x[0].startswith('*'):
            field=x[0][1:]
            if not getattr(node,field).endswith(x[1]):
                return
        else:
            #print "Testing %s=%s"%(x[0],x[1])
            if not x[1]==getattr(node,x[0]):
                return
    yield True

def match_seqpattern(pat,nodes,start,end,nodedict,dotsdict,symdict):
    #print "match_seqpattern: %s"%(pat,)
    if len(pat)==0:
        if start==end:
            #print "=> Yes."
            yield True
        else:
            #print "=> No (start=%s, end=%s)"%(start,end)
            return
    elif pat[0][0]=='node':
        if start==end: return
        for x in match_nodepattern(pat[0][1],nodes[start],nodedict,dotsdict,symdict):
            for y in match_seqpattern(pat[1:],nodes,start+1,end,
                nodedict,dotsdict,symdict):
                yield y
    elif pat[0][0]=='dots':
        for mid in xrange(start,end+1):
            #print "Trying ...%s -> %d--%d"%(pat[0][1],start,mid)
            dotsdict[pat[0][1]]=(nodes,start,mid)
            for y in match_seqpattern(pat[1:],nodes,mid,end,
                nodedict,dotsdict,symdict):
                yield y
    return

def make_node_from_pattern(pat,nodedict,dotsdict,symdict):
    # determine node type
    #sys.stderr.write('mnfp: %s\n'%(pat,))
    node_type=None
    cat=None
    for x in pat:
        if x[0]=='children' and not node_type:
                node_type='nonterm'
        elif x[0]=='word':
                node_type='term'
                word=x[1]
        elif x[0]=='word$':
                node_type='term'
                word=symdict[x[1]]
        elif x[0]=='cat':
                cat=x[1]
        elif x[0]=='cat$':
                cat=symdict[x[1]]
        elif x[0]=='#':
                node=nodedict[x[1]]
                # prevent nodes from being used twice
                del nodedict[x[1]]
                node_type='indexed'
    if node_type=='nonterm':
        node=NontermNode(cat)
    elif node_type=='term':
        node=TerminalNode(cat,word)
    elif node_type=='indexed':
        pass
    else:
        raise 'Node type is unknown!'
    for x in pat:
        if x[0]=='children':
            node.children=make_seq_from_pattern(x[1],nodedict,dotsdict,symdict)
            for n in node.children:
                n.parent=node
        elif x[0] in ['word','word$','cat','cat$','#']:
            pass # ignore
        elif x[0].endswith('$'):
            field=x[0][:-1]
            setattr(node,field,symdict[x[1]])
        else:
            setattr(node,x[0],x[1])
    determine_tokenspan(node)
    return node

def make_seq_from_pattern(pat,nodedict,dotsdict,symdict):
    seq=[]
    for x in pat:
        if x[0]=='node':
            seq.append(make_node_from_pattern(x[1],nodedict,dotsdict,symdict))
        elif x[0]=='dots':
            (nodes,start,end)=dotsdict[x[1]]
            #sys.stderr.write('Dots %s: %r[%d:%d]\n'%(x[1],nodes,start,end))
            seq.extend(nodes[start:end])
            # prevent dots from being used twice
            del dotsdict[x[1]]
        else:
            raise 'Unknown item in Sequence patten'
    return seq

def replace_node_bu(patIn,patOut,nodes,proc=None):
    """replaces all occurrences of patIn with patOut.
    the procedure passed as proc can either modify
    the node/dots/sym dictionaries or raise a StopIteration
    to prevent the modification"""
    if len(nodes)==0: return
    parent=nodes[0].parent
    for i in xrange(0,len(nodes)):
        node1=replace_node_bu1(patIn,patOut,nodes[i],proc)
        if node1:
            node1.parent=parent
            nodes[i]=node1

def replace_node_bu1(patIn,patOut,node,proc):
    replace_node_bu(patIn,patOut,node.children)
    nodedict={}
    dotsdict={}
    symdict={}
    m_iter=match_nodepattern(patIn,node,nodedict,dotsdict,symdict)
    try:
        m_iter.next()
        if proc: proc(nodedict,dotsdict,symdict)
        node1=make_node_from_pattern(patOut,nodedict,dotsdict,symdict)
        return node1
    except StopIteration:
        return None

def test():
    t=tree.Tree()
    pat=compile_nodepattern("""
    (NX.S (ART.DET der) (ADJA.ATTR rote) (NN.HD Hahn)
        (NX.KON (ART.DET die) (ADJA.ATTR gruene) (NN.HD Henne)
            (KON-P.KON (KON und)
                (NX.CJ (ART.DET das) (NN.HD Kueken)))))""")
    t.roots=[make_node_from_pattern(pat,{},{},{})]
    pat2=compile_nodepattern("""
    #0=(* (A FOO) (B BAR) (C BAZ))""")
    n=make_node_from_pattern(pat2,{"0":t.roots[0]},{},{})
    print n.to_penn()
    print t.roots[0].to_penn()

def do_grepping(fname,pattern):
    """reads in an export file and searches for matches of pattern"""
    pat=compile_nodepattern(pattern)
    print "Pattern: %s"%pat
    f=open(fname,'r')
    l=f.readline()
    while (l!=''):
        m=bos_pattern.match(l)
        if m:
            sent_no=m.group(1)
            t=read_sentence(f)
            nodedict={}
            dotsdict={}
            symdict={}
            for n in t.topdown_enumeration():
                m_iter=tree.match_nodepattern(pat,n,
                        nodedict,dotsdict,symdict)
                try:
                    match=m_iter.next()
                    print "%s->%s %s %s"%(sent_no,
                        nodedict,dotsdict,symdict)
                    print n.to_penn()
                except StopIteration:
                    pass
        l=f.readline()
    f.close()


move_app_1_I=compile_nodepattern("""
#0=(NX #1=(NE.HD *) (NX.APP #2=(NE.HD *) ...1))""")
move_app_1_O=compile_nodepattern("""
#0=(NX (NX.HD #1.- #2.-) ...1)""")

raise_kon_1_I=compile_nodepattern("""
#0=($cat ...1 (KON-P.KON ...2 #1.HD #2.CJ))""")
raise_kon_1_O=compile_nodepattern("""
#0=($cat ($cat.CJ ...1) ...2 #1.- #2)""")

raise_kon_2_I=compile_nodepattern("""
#0=($cat ...1 ($cat2.KON #1.CJ ...2))""")
raise_kon_2_O=compile_nodepattern("""
#0=($cat ($cat.CJ ...1) #1 ...2)""")

introduce_koord_I=compile_nodepattern("""
#0=(KON-P #KON.HD #1=($Sent ...1))""")
introduce_koord_O=compile_nodepattern("""
#0=(SIMPX (KOORD.- #KON) #1)""")
def introduce_koord_proc(nodes,dots,syms):
    if not syms['Sent'] in sent_cats:
        raise StopIteration

kill_vx_in_koord_I=compile_nodepattern("""
#0=(SIMPX.$L #1=(VX* ...1 #2.CJ ...2))""")
kill_vx_in_koord_O=compile_nodepattern("""
#1=(*.$L ...1 #2 ...2)""")

def optional_field(name,dots):
    (nodes0,start,end)=dots[name]
    if (start==end):
        pass
    else:
        #sys.stderr.write('OPTF: %s %r\n'%(name,nodes0[start:end]))
        node=tree.NontermNode(name,'-')
        node.children=nodes0[start:end]
        for n in node.children:
            n.parent=node
        dots[name]=([node],0,1)
# (C dass/damit) (VF er) fahren kann
introduce_fields_0_I=compile_nodepattern("""
#0=(SIMPX #K.KONJ ...MF #VC.AUX #F.HD ...NF)""")
introduce_fields_0_O=compile_nodepattern("""
#0=(SIMPX (C.- #K) ...MF #VC #F.- ...NF)""")
def introduce_fields_0_proc(nodes,dots,syms):
    optional_field('MF',dots)
    optional_field('NF',dots)
# (C den) (VF er) fahren kann
introduce_fields_0R_I=compile_nodepattern("""
#0=(R-SIMPX #R ...MF #VC.AUX #F.HD ...NF)""")
introduce_fields_0R_O=compile_nodepattern("""
#0=(R-SIMPX (C.- #R) ...MF #VC #F.- ...NF)""")
def introduce_fields_0R_proc(nodes,dots,syms):
    optional_field('MF',dots)
    optional_field('NF',dots)
# (C dass/damit) (VF er) faehrt
introduce_fields_1_I=compile_nodepattern("""
#0=(SIMPX #K.KONJ ...MF #F.HD ...NF)""")
introduce_fields_1_O=compile_nodepattern("""
#0=(SIMPX (C.- #K) ...MF #F.- ...NF)""")
def introduce_fields_1_proc(nodes,dots,syms):
    optional_field('MF',dots)
    optional_field('NF',dots)
# er ist in die Stadt gefahren, um Butter zu kaufen
# hat (MF Beckmeyer BLG-Interessen) verteidigt?
introduce_fields_2_I=compile_nodepattern("""
#0=(SIMPX ...VF #F.HD ...MF #VC.AUX ...NF)""")
introduce_fields_2_O=compile_nodepattern("""
#0=(SIMPX ...VF #F.- ...MF #VC ...NF)""")
def introduce_fields_2_proc(nodes,dots,syms):
    optional_field('VF',dots)
    optional_field('MF',dots)
    optional_field('NF',dots)
# er macht die Dose auf
introduce_fields_2a_I=compile_nodepattern("""
#0=(SIMPX ...VF #F.HD ...MF #VC.AVZ ...NF)""")
introduce_fields_2a_O=compile_nodepattern("""
#0=(SIMPX ...VF #F.- ...MF #VC ...NF)""")
def introduce_fields_2a_proc(nodes,dots,syms):
    optional_field('VF',dots)
    optional_field('MF',dots)
    optional_field('NF',dots)
# er faehrt in die Stadt,um Butter zu kaufen
# Veruntreute (MF die AWO Spendengeld?)
#TODO: NF abspalten
introduce_fields_3_I=compile_nodepattern("""
#0=(SIMPX ...VF #F.HD ...MF)""")
introduce_fields_3_O=compile_nodepattern("""
#0=(SIMPX ...VF #F.- ...MF)""")
def introduce_fields_3_proc(nodes,dots,syms):
    optional_field('VF',dots)
    optional_field('MF',dots)

def raise_par(t,node):
    """attaches parentheses to the root node"""
    nodes=node.children
    pars=[]
    for i in xrange(0,len(nodes)):
        if nodes[i].edge_label=='PAR':
            nodes[i].set_parent(None)
            t.roots.append(nodes[i])
            pars.append(i)
        raise_par(t,nodes[i])
    pars.reverse()
    for i in pars:
        del nodes[i]

sent_labels=frozenset(['S','REL','NEB','OBJC','SUBJC'])
obj_labels=frozenset(['SUBJ','OBJA','OBJA2','OBJD','OBJG',
                'PRED','PP','OBJP','EXPL','KOM','ZEIT','ETH',
                'OBJC','OBJI','S','SUBJC','KONJ','NEB','REL'
                ])

def insert_simpx(nodes):
    """inserts SIMPX nodes into the tree"""
    for n in nodes:
        insert_simpx(n.children)
        if n.cat.startswith('VX'):
            if n.edge_label in sent_labels:
                lowernode=tree.NontermNode(n.cat,'HD')
                lowernode.parent=n
                lowernode.children=n.children
                lowernode.start=n.start
                lowernode.end=n.end
                n.children=[lowernode]
                if n.edge_label=='REL':
                    n.cat='R-SIMPX'
                else:
                    n.cat='SIMPX'
                for child in lowernode.children:
                    child.parent=lowernode
            elif n.edge_label=='KON' or n.edge_label=='CJ':
                has_subject=False
                has_object=False
                for n1 in n.children:
                    if n1.edge_label=='SUBJ':
                        has_subject=True
                        break
                    elif n1.edge_label in obj_labels:
                        has_object=True
                if has_object or has_subject:
                    lowernode=tree.NontermNode(n.cat,'HD')
                    lowernode.parent=n
                    lowernode.children=n.children
                    lowernode.start=n.start
                    lowernode.end=n.end
                    n.children=[lowernode]
                    if has_subject:
                        n.cat='SIMPX'
                    else:
                        n.cat='FKONJ'
                    for child in lowernode.children:
                        child.parent=lowernode

sent_cats=frozenset(['SIMPX','R-SIMPX','FKONJ'])
nonsent_cats=frozenset(['NX','ADJX','PX','ADVX'])
def raise_objects(nodes,sent_node=None):
    """raise objects (and nonfinite verbs) to the nearest
    sentence node"""
    # DANGER: a node can be both an argument and a sentence node,
    #         so we have to be careful.
    if len(nodes)==0: return
    objs=[]
    for (i,n) in enumerate(nodes):
        if n.cat in sent_cats:
            raise_objects(n.children,n)
        elif n.cat in nonsent_cats:
            raise_objects(n.children,None)
        else:
            raise_objects(n.children,sent_node)
        if (n.edge_label in obj_labels or
            # TODO: "nicht behandelt, sondern mitbehandelt"
            n.edge_label=='ADV' or
            n.edge_label=='AUX' and n.parent.cat.endswith('F') or
            n.edge_label=='KON' and n.cat in sent_cats or
            n.cat=='KON-P' and n.children[1] in sent_cats or
            n.edge_label=='AVZ' and n.parent.cat=='VXVF'):
                objs.append(i)
    if sent_node and nodes[0].parent!=sent_node:
        for i in objs[::-1]:
            nodes[i].parent=sent_node
            sent_node.children.append(nodes[i])
            del nodes[i]

def raise_nonprojective(t,node,sent_node=None):
    """raise nonprojective adjuncts to the nearest
    sentence node"""
    new_sent_node=sent_node
    if node.cat in sent_cats:
        new_sent_node=node
    for (i,n) in enumerate(node.children):
        raise_nonprojective(t,n,new_sent_node)
    if sent_node and (node.cat=='NX'):
        for (i,n) in list(enumerate(node.children))[::-1]:
            if n.edge_label=='HD': break
            if t.discontinuity(node.children,i-1,sent_node):
                n.realparent=n.parent
                sent_node.children.append(n)
                n.parent=sent_node
                del node.children[i]


def doNothing(): pass

def introduceFields(t):
    tree.replace_node_bu(introduce_fields_0_I,
            introduce_fields_0_O, t.roots,
            introduce_fields_0_proc)
    tree.replace_node_bu(introduce_fields_0R_I,
            introduce_fields_0R_O, t.roots,
            introduce_fields_0R_proc)
    tree.replace_node_bu(introduce_fields_1_I,
            introduce_fields_1_O, t.roots,
            introduce_fields_1_proc)
    tree.replace_node_bu(introduce_fields_2_I,
            introduce_fields_2_O, t.roots,
            introduce_fields_2_proc)
    tree.replace_node_bu(introduce_fields_2a_I,
            introduce_fields_2a_O, t.roots,
            introduce_fields_2a_proc)
    tree.replace_node_bu(introduce_fields_3_I,
            introduce_fields_3_O, t.roots,
            introduce_fields_3_proc)


def doStuff(t):
    for n in t.roots:
        assert n.parent is None
        raise_par(t,n)
    t.check_nodetable()
    insert_simpx(t.roots)
    tree.replace_node_bu(move_app_1_I, move_app_1_O, t.roots)
    tree.replace_node_bu(raise_kon_1_I, raise_kon_1_O, t.roots)
    tree.replace_node_bu(raise_kon_2_I, raise_kon_2_O, t.roots)
    tree.replace_node_bu(introduce_koord_I, introduce_koord_O, t.roots,
        introduce_koord_proc)
    raise_objects(t.roots)
    t.determine_tokenspan_all()
    for n in t.roots:
        raise_nonprojective(t,n,None)
    t.determine_tokenspan_all()
    for n in t.roots:
        sys.stderr.write(n.to_penn())
    sys.stderr.write('\n')
    tree.replace_node_bu(kill_vx_in_koord_I,kill_vx_in_koord_O,
        t.roots)
    introduceFields(t)
    t.check_roots()
    t.node_table={}
    t.renumber_ids(t.roots)
    t.check_roots()
    t.check_nodetable()

if __name__=='__main__':
    export_file=sys.argv[1]
    transform_filter(export_file,doStuff)
    #do_grepping(export_file,"(KON-P ...1)")
