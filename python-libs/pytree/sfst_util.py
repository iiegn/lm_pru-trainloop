import sfst

def analyses(word,automaton):
    a1=sfst.Automaton(word,automaton.alphabet)
    a2=automaton.compose(a1).lower_level().minimise()
    return a2.enumerate_strings()

def generate(word,automaton):
    a1=sfst.Automaton(word,automaton.alphabet)
    a2=a1.compose(automaton).upper_level().minimise()
    return a2.enumerate_strings()

def disamb_bool(xs, pred):
    filtered=[x for x in xs if pred(x)]
    if filtered:
        return filtered
    else:
        return xs

def disamb_rank(xs, func):
    best=[]
    best_score=None
    for x in xs:
        score=func(x)
        if best_score is None or score<best_score:
            best=[x]
            best_score=score
        elif best_score==score:
            best.append(x)
    return best
