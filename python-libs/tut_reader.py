#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
import sys
import re
import pytree.tree as tree

token_re=re.compile('([0-9]+(?:\.[0-9]+)?) *([a-z]+ \[.*\](?: *[A-Za-z]+)?|[^ \(\)]+|[\(\)]) *\((.*)\) *\[([0-9]+(?:\.[0-9]+)?);\]?([A-Z0-9\-\+\*/]+)\]')
token_re_2=re.compile('([0-9]+(?:\.[0-9]+)?) *(t \[.*\]|[^ \(\)]+|[\(\)]) *\((.*)\) *\[\]')

class Token:
    def __init__(self,**kw):
        for k,v in kw.iteritems():
            setattr(self,k,v)
    def __str__(self):
        return 'Token(%s)'%(','.join(
            ['%s=%s'%(x[0],repr(x[1])) for x in self.__dict__.iteritems()]))
    def __repr__(self):
        return self.__str__()

tokens=[]
tokens_by_id={}

def check_cycles():
    for tok in tokens:
        id0=tok.id
        id1=tok.parent_id
        seen=[tok.id]
        while id1!='0':
            if id1 in seen:
                tok=tokens_by_id[id0]
                print >>sys.stderr, "Break cycle at %s:%s-%s:%s"%(id0,
                                                                  tokens_by_id[id0],
                                                                  id1,
                                                                  tokens_by_id[id1])
                tok.parent_id='0'
                tok.edge_label="BREAK-CYCLE"
                break
            tok=tokens_by_id[id1]
            id0=id1
            id1=tok.parent_id
            seen.append(id0)
            
                
def fix_dp():
    need_correction={}
    for tok in tokens:
        if tok.coarse_pos=='NOUN' and tok.edge_label.startswith('DET'):
            tok2=tokens_by_id[tok.parent_id]
            assert tok2.coarse_pos in ['ART','ADJ','NUM'],(tok2.word,tok2.coarse_pos)
            tok.parent_id=tok2.parent_id
            tok.edge_label=tok2.edge_label
            tok2.parent_id=tok.id
            tok2.edge_label='DET'
            need_correction[tok2.id]=tok.id
    for tok in tokens:
        if tok.parent_id in need_correction:
            tok.parent_id=need_correction[tok.parent_id]


def convert_prepart():
    for i in xrange(len(tokens)-1):
        tok=tokens[i]
        tok2=tokens[i+1]
        if (tok2.id==tok.id+'.1' and
            tok2.coarse_pos=='ART' and
            tok.coarse_pos=='PREP'):
            n=make_ng(tok2.attrs)
            tok.cat='E'+num2textpro[n[0]]
            tok.attrs += tok2.attrs

def convert_clitic():
    for i in xrange(len(tokens)-1):
        tok=tokens[i]
        tok2=tokens[i+1]
        if (tok2.id==tok.id+'.1' and
            tok2.coarse_pos=='PRON' and
            tok2.attrs[-1]=='CLITIC' and
            tok.coarse_pos=='VERB'):
            n=make_ng(tok2.attrs)
            tok.cat += '+E'
            tok.attrs += tok2.attrs


correct_accents={
    "a'":"�",
    "a`":"�",
    "e'":"�",
    "e`":"�",
    "u'":"�",
    "u`":"�",
    "i'":"�",
    "i`":"�",
    "o`":"�"
    }

# problem: distant past, ex. contribui'
def make_terminal(tok):
    real_word=tok.word
    n=tree.TerminalNode(tok.cat,real_word,'HD')
    n.lemma=tok.attrs[0].lower()
    if tok.coarse_pos in ['DET','ADJ']:
        n.morph=make_ng(tok.attrs)
    else:
        n.morph='--'
    return n

def mk_prep(p):
    return [p,'PREP','MONO']
def mk_prepart(p,gen='M',num='SING'):
    return [p,'PREPART','MONO','ART','DEF',gen,num]
def mk_noun(p,gen='M',num='SING'):
    return [p,'NOUN','COMMON',gen,num]
def mk_vf(p,kind='TRANS'):
    return [p,'VERB','MAIN','INFINITE','PRES',kind]

pnp_locut={
    'A_CAUSA_DI':[mk_prep('A'),
                  mk_noun('CAUSA','F'),
                  mk_prep('DI')],
    'A_COMINCIARE_DA':[mk_prep('A'),
                       'COMINCIARE VERB MAIN INFINITE PRES INTRANS'.split(),
                       mk_prep('DA')],
    'A_DIFFERENZA_DI': [mk_prep('A'),
                        mk_noun('DIFFERENZA','F'),
                        mk_prep('DI')],
    'A_MENO_DI': [mk_prep('A'),
                  'MENO PRON INDEF ALLVAL ALLVAL LSUBJ+LOBJ+OBL'.split(),
                  mk_prep('DI')],
    'A_MO_DI': [mk_prep('A'),
                mk_noun("MO'",'M'),
                mk_prep('DI')],
    'A_SECONDA_DI': [mk_prep('A'),
                     mk_noun('SECONDA','F'),
                     mk_prep('DI')],
    'A_SEGUITO_DI': [mk_prep('A'),
                     mk_noun('SEGUITO','M'),
                     mk_prep('DI')],
    'AL_PARI_DI': [mk_prepart('A'),
                   mk_noun('PARI','ALLVAL'),
                   mk_prep('DI')],
    'AL_POSTO_DI': [mk_prepart('A'),
                    mk_noun('POSTO','M'),
                    mk_prep('DI')],
    "ALL'_INTERNO_DI": [mk_prepart('A'),
                        mk_noun('INTERNO','M'),
                        mk_prep('DI')],
    'ALLA_LUCE_DI': [mk_prepart('A','F'),
                     mk_noun('LUCE','F'),
                     mk_prep('DI')],
    'ALLA_PARI_DI': [mk_prepart('A','F'),
                     mk_noun('PARI','ALLVAL'),
                     mk_prep('DI')],
    'ALLO_SCOPO_DI': [mk_prepart('A'),
                      mk_noun('SCOPO','M'),
                      mk_prep('DI')],
    'DA_PARTE_DI': [mk_prep('DA'),
                    mk_noun('PARTE','F'),
                    mk_prep('DI')],
    'DI_FRONTE_A': [mk_prep('DI'),
                    mk_noun('FRONTE','M'),
                    mk_prep('A')],
    'IN_BASE_A': [mk_prep('IN'),
                  mk_noun('BASE','F'),
                  mk_prep('A')],
    'IN_CERCA_DI': [mk_prep('IN'),
                    mk_noun('CERCA','F'),
                    mk_prep('DI')],
    'IN_CIMA_A': [mk_prep('IN'),
                  mk_noun('CIMA','F'),
                  mk_prep('A')],
    'IN_DIREZIONE_DI': [mk_prep('IN'),
                        mk_noun('DIREZIONE','F'),
                        mk_prep('DI')],
    'IN_LINEA_CON': [mk_prep('IN'),
                     mk_noun('LINEA','F'),
                     mk_prep('CON')],
    'IN_MANO_A': [mk_prep('IN'),
                  mk_noun('MANO','F'),
                  mk_prep('A')],
    'IN_MEZZO_A': [mk_prep('IN'),
                   mk_noun('MEZZO','M'),
                   mk_prep('A')],
    'IN_MODO_A': [mk_prep('IN'),
                  mk_noun('MODO','M'),
                  mk_prep('A')],
    'IN_NOME_DI': [mk_prep('IN'),
                   mk_noun('NOME','M'),
                   mk_prep('DI')],
    'IN_SEDE_DI': [mk_prep('IN'),
                   mk_noun('SEDE','F'),
                   mk_prep('DI')],
    'IN_VISTA_DI': [mk_prep('IN'),
                    mk_noun('VISTA','F'),
                    mk_prep('DI')],
    'NEI_CONFRONTI_DI': [mk_prepart('IN','M','PL'),
                         mk_noun('CONFRONTO','M','PL'),
                         mk_prep('DI')],
    'NEI_PRESSI_DI': [mk_prepart('IN','M','PL'),
                      mk_noun('PRESSO','M','PL'),
                      mk_prep('DI')],
    'NEL_CORSO_DI': [mk_prepart('IN','M'),
                     mk_noun('CORSO','M'),
                     mk_prep('DI')]
    }

def remove_superfluous_traces():
    global tokens
    needed=set([tok.parent_id for tok in tokens])
    tokens=[tok for tok in tokens
            if '.' not in tok.id and tok.attrs[1]!='MARKER'
               or tok.id in needed]

def convert_locut():
    global tokens
    proxy_arg={}
    for i in xrange(0,len(tokens)-2):
        if ('LOCUTION' in tokens[i].attrs
            and tokens[i+1].edge_label=='CONTIN+LOCUT'
            and tokens[i+2].edge_label=='CONTIN+LOCUT'):
            if tokens[i].attrs[0] in pnp_locut:
                print >>sys.stderr,"Correct locution: %s"%(tokens[i].attrs[0],)
                replacement=pnp_locut[tokens[i].attrs[0]]
                for j in xrange(3):
                    tok=tokens[i+j]
                    tok.attrs=replacement[j]
                    try:
                        tok.coarse_pos=replacement[j][1]
                        if tok.coarse_pos=='PREPART':
                            n=make_ng2(tok.attrs,5)[0]
                            tok.cat='E'+num2textpro[n]
                    except IndexError:
                        assert False, replacement[j]
                    tokens[i+j].cat=determine_pos(replacement[j])
                proxy_arg[tokens[i].id]=tokens[i+2].id
                tokens[i+1].edge_label='PREP-ARG'
                tokens[i+2].edge_label='RMOD'
        elif (tokens[i].parent_id in proxy_arg and
              tokens[i].parent_id!=tokens[i-1].id):
            print >>sys.stderr,"Correcting head of %s_%s"%(tokens[i].id,tokens[i].word)
            tokens[i].parent_id=proxy_arg[tokens[i].parent_id]
        if tokens[i].attrs[0].startswith('A_CHE_'):
            # a che vedere/fare
            # right now: [PX a [VP che vedere]]
            print >>sys.stderr,"a_che: %s"%(tokens[i].attrs[0],)
            assert tokens[i].word.lower()=='a'
            tokens[i].attrs=mk_prep('A')
            tokens[i+1].attrs='CHE PRON RELAT ALLVAL ALLVAL LSUBJ+LOBJ'.split()
            tokens[i+2].attrs=mk_vf(tokens[i+2].word.upper())
            tokens[i+1].parent_id=tokens[i+2].id
            tokens[i+1].edge_label='DOBJ'
            tokens[i+2].parent_id=tokens[i].id
            tokens[i+2].edge_label='PREP-ARG'
            for j in xrange(3):
                tokens[i+j].coarse_pos=tokens[i+j].attrs[1]
                tokens[i+j].cat=determine_pos(tokens[i+j].attrs)

def make_ng2(attrs,off=2):
    if len(attrs)<=off:
        return '**'
    if attrs[off]=='ALLVAL':
        g='*'
    else:
        g=attrs[off][0].lower()
    if len(attrs)<=off+1:
        return '*'+g
    if attrs[3]=='ALLVAL':
        n='*'
    else:
        n=attrs[off+1][0].lower()
    if g in 'sp' or n in 'fm':
        print >>sys.stderr,"make_ng: bogus attributes %s"%(attrs,)
        g,n=n,g
    return n+g

def correct_deps():
    convert_locut()
    fix_dp()
    convert_prepart()
    convert_clitic()
    remove_superfluous_traces()

num2textpro={'p':'P', 's':'S', '*':'N'}
def determine_pos(attrs):
    if attrs[1]=='NOUN':
        if attrs[2]=='PROPER':
            return 'SPN'
        elif attrs[2]=='COMMON':
            n=make_ng(attrs)[0]
            return 'S'+num2textpro[n]
    elif attrs[1]=='ART':
        if attrs[2] in ['DEF','INDEF']:
            n=make_ng(attrs)[0]
            return 'R'+num2textpro[n]
        else:
            return 'ART'
    elif attrs[1]=='ADJ':
        if attrs[2] in ['QUALIF','ORDIN']:
            n=make_ng(attrs)[0]
            return 'A'+num2textpro[n]
        elif attrs[2] in ['POSS','INDEF','DEMONS','DEITT']:
            n=make_ng(attrs)[0]
            return 'D'+num2textpro[n]
        elif attrs[2]=='INTERR':
            if attrs[0]=='CHE':
                return 'CCHE'
            elif attrs[0].startswith('QUANT'):
                n=make_ng(attrs)[0]
                return 'QN'+num2textpro[n]
            else:
                n=make_ng(attrs)[0]
                return 'D'+num2textpro[n]+'-Q'
    elif attrs[1]=='PREDET':
        n=make_ng2(attrs)[0]
        return 'D'+num2textpro[n]
    elif attrs[1]=='CONJ':
        if attrs[0]=='CHE':
            return 'CCHE'
        if attrs[2]=='COORD':
            return 'C_coo'
        elif attrs[2]=='SUBORD':
            if attrs[0][0]=='Q' or attrs[0] in ['COME','DOVE']:
                return 'CADV'
            else:
                return 'C_sub'
    elif attrs[1]=='ADV':
        return 'B'
    elif attrs[1]=='PREP':
        return 'E'
    elif attrs[1]=='PUNCT':
        if attrs[0][-1] in '.:;?!':
            return 'XPS'
        elif attrs[0][-1] in ',':
            return 'XPW'
        elif attrs[0][-1] in '()[]':
            return 'XPB'
        elif attrs[0][-1] in '\'"-��':
            return 'XPO'
    elif attrs[1]=='PRON':
        if attrs[0].lower()=='che':
            return 'CCHE'
        elif attrs[0].lower()=='chi':
            return 'CCHI'
        elif attrs[2] in ['RELAT','INTERR']:
            if attrs[0].startswith('QUANT'):
                n=make_ng(attrs)[0]
                return 'QN'+num2textpro[n]
            else:
                n=make_ng(attrs)[0]
                return 'P'+num2textpro[n]+'-Q'
        elif attrs[2]=='LOC':
            return 'B'
        elif attrs[2] in ['PERS','DEMONS','REFL-IMPERS','INDEF','POSS']:
            n=make_ng(attrs)[0]
            return 'P'+num2textpro[n]
        elif attrs[2]=='INTERR':
            #TBD: are interrogative pronouns also Px? Or QNx?
            n=make_ng(attrs)[0]
            return 'P'+num2textpro[n]+'-Q'
    elif attrs[1]=='NUM':
        return 'N'
    elif attrs[1]=='VERB':
        if attrs[2]=='AUX':
            y='Y'
        else:
            y=''
        if attrs[3] in ['IND','CONDIZ','CONG']:
            return 'VI'+y
        elif attrs[3]=='IMPER':
            return 'VM'+y
        elif attrs[3]=='GERUND':
            return 'VG'+y
        elif attrs[3]=='PARTICIPLE':
            if len(attrs)>6:
                p=attrs[6][0]
                if p not in 'SP':
                    p='S'
            else:
                p='S'
            return 'V%sP%s'%(p,y)
        elif attrs[3]=='INFINITE':
            return 'VF'+y
    elif attrs[1]=='INTERJ':
        return 'I'
    return attrs[1]

def enter_token(tok):
    real_word=tok.word
    if real_word[-2:] in correct_accents:
        accent=correct_accents[real_word[-2:]]
        if real_word.endswith("che'"):
            accent='�'
        elif real_word in ["cioe'","se'"]:
            accent='�'
        real_word=real_word[:-2]+accent
        tok.word=real_word
    tok.lemma=tok.attrs[0]
    tok.coarse_pos=tok.attrs[1]
    tok.cat=determine_pos(tok.attrs)
    tokens_by_id[tok.id]=tok
    tokens.append(tok)

def make_ng(attrs):
    if len(attrs)<=3:
        return '**'
    if attrs[3] in ['ALLVAL','ALLVL']:
        g='*'
    else:
        g=attrs[3][0].lower()
    if len(attrs)<=4:
        return '*'+g
    if attrs[4] in ['ALLVAL','ALLVL']:
        n='*'
    else:
        n=attrs[4][0].lower()
    complain=False
    if g in 'sp' or n in 'fm':
        complain=True
        g,n=n,g
    if g not in 'fm*':
        complain=True
        g='*'
    if n not in 'sp*':
        n='*'
    if complain:
        print >>sys.stderr,"make_ng: bogus attributes %s"%(attrs,)
    return n+g


def read_trees(f):
    global tokens
    global tokens_by_id
    sent_no=1
    for line in f:
        if line.startswith('***'):
            if tokens:
                check_cycles()
                correct_deps()
                yield (sent_no,tokens)
                sent_no+=1
                tokens=[]
                tokens_by_id={}
        elif line.strip()=='':
            pass
        else:
            if '"Does not exist"' in line:
                #HACK: make some innocent-looking entry for unknown words
                # (to process the output of TULE)
                w=line.strip().split()[1]
                line=line.replace('"Does not exist"','(%s NOUN COMMON ALLVAL ALLVAL)'%(w,))
            m=token_re.match(line.strip())
            if m:
                tok=Token(id=m.group(1),word=m.group(2))
                tok.attrs=m.group(3).split()
                tok.parent_id=m.group(4)
                tok.edge_label=m.group(5)
                enter_token(tok)
            else:
                m=token_re_2.match(line.strip())
                if m:
                    tok=Token(id=m.group(1),word=m.group(2))
                    tok.id=m.group(1)
                    tok.word=m.group(2)
                    tok.attrs=m.group(3).split()
                    tok.parent_id='0'
                    tok.edge_label='--'
                    enter_token(tok)
                else:
                    print "Cannot parse:"
                    print line

